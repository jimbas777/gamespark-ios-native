//
//  FloatingIcon.m
//  Gamespark
//
//  Created by Jimmy Basselo on 3/5/15.
//  Copyright (c) 2015 mainpark. All rights reserved.
//

#import "FloatingIcon.h"

@implementation FloatingIcon
@synthesize MoveEnable;
@synthesize MoveEnabled;

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	MoveEnabled = NO;
	[super touchesBegan:touches withEvent:event];
	if (!MoveEnable) {
		return;
	}
	UITouch *touch = [touches anyObject];
	beginpoint = [touch locationInView:self];
}

- (CGRect)backgroundRectForBounds:(CGRect)bounds
{
	bounds.origin.x += 5;
	bounds.origin.y += 5;
	bounds.size.width -= 10;
	bounds.size.height -= 10;
	return bounds;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	
	MoveEnabled = YES;
	
	if (!MoveEnable) {
		return;
	}
	UITouch *touch = [touches anyObject];
	CGPoint currentPosition = [touch locationInView:self];
	float offsetX = currentPosition.x - beginpoint.x;
	float offsetY = currentPosition.y - beginpoint.y;
	self.center = CGPointMake(self.center.x + offsetX, self.center.y + offsetY);
	
	if (self.center.x > (self.superview.bounds.size.width-self.frame.size.width/2)) {
		CGFloat x = self.superview.bounds.size.width-self.frame.size.width/2;
		self.center = CGPointMake(x, self.center.y + offsetY);
	}else if (self.center.x < self.frame.size.width/2){
		CGFloat x = self.frame.size.width/2;
		self.center = CGPointMake(x, self.center.y + offsetY);
	}
	
	if (self.center.y > (self.superview.bounds.size.height-self.frame.size.height/2)) {
		CGFloat x = self.center.x;
		CGFloat y = self.superview.bounds.size.height-self.frame.size.height/2;
		self.center = CGPointMake(x, y);
	}else if (self.center.y <= self.frame.size.height/2){
		CGFloat x = self.center.x;
		CGFloat y = self.frame.size.height/2;
		self.center = CGPointMake(x, y);
	}
}

- (void) moveButton:(BOOL)origin
{
	if(origin) {
		//self.center = CGPointMake(self.superview.bounds.size.width/2, self.superview.bounds.size.height/2);
		//self.center = [self getLastPos];
		CGPoint pt = [self getLastPos];
		float fa = self.superview.bounds.size.width/2;
		float fb = pt.x;
		if((fa-fb) < 0.0f) {
			fa += 10.0f;
		} else {
			fa -= 10.0f;
		}
		float fh = self.superview.bounds.size.height - 20.0f;
		float fy = pt.y > fh ? fh : pt.y;
		// jika preference abis reset atau aplikasi baru install, float icon ditengah screen
		if(fy < 20.f) fy = self.superview.bounds.size.height/2;
		NSLog(@"origin:fy:%.2f,fh:%.2f", fy, fh);
		//self.center = CGPointMake(self.superview.bounds.size.width/2, pt.y);
		self.center = CGPointMake(fa, fy);
	}
	if (self.center.x >= self.superview.bounds.size.width/2) {
		[UIView beginAnimations:@"move" context:nil];
		[UIView setAnimationDuration:1];
		[UIView setAnimationDelegate:self];
		//self.frame=CGRectMake(280.f,self.center.y-20, 40.f,40.f);
		int top = self.center.y-20-2;
		if(top < 0) top = 2;
		self.frame=CGRectMake(self.superview.bounds.size.width-self.frame.size.width-2.0f,top, 40.f,40.f);
		[UIView commitAnimations];
		// NSLog(@"touchesEndedIf");
	}else{
		[UIView beginAnimations:@"move" context:nil];
		[UIView setAnimationDuration:1];
		[UIView setAnimationDelegate:self];
		self.frame=CGRectMake(2.f,self.center.y-20+2, 40.f,40.f);
		[UIView commitAnimations];
		// NSLog(@"touchesEndedElse");
	}
}

- (id) getUserPreference:(NSString*)forKey
{
	NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
	return [defaults valueForKey:forKey];
}

- (CGPoint) getLastPos
{
	NSString *sleft = [self getUserPreference:@"gamesparkPosX"];
	NSString *stop = [self getUserPreference:@"gamesparkPosY"];
	int left = [sleft intValue];
	int top = [stop intValue];
	return CGPointMake(left,top);
}

- (void) setUserPreference:(id)value forKey:(NSString*)key
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:value forKey:key];
	[defaults synchronize];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (!MoveEnable) {
		return;
	}
	[self moveButton:NO];
	int left,top;
	// [UIView beginAnimations:@"move" context:nil];
	// [UIView setAnimationDuration:1];
	// [UIView setAnimationDelegate:self];
	if (self.center.x >= self.superview.bounds.size.width/2) {
		//self.frame=CGRectMake(280.f,self.center.y-20, 40.f,40.f);
		left = self.superview.bounds.size.width-self.frame.size.width-2.0f;
		top = self.center.y-20-2;
		if(top < 0) top = 2;
	}else{
		left = 2.f;
		top = self.center.y-20+2;
	}

	// self.frame=CGRectMake(left,top, 40.f,40.f);
	// [UIView commitAnimations];
	// [UIView animateWithDuration:1
	// 	delay:0 options:UIViewAnimationOptionBeginFromCurrentState
	// 	animations:^(void) {}
	// 	completion:^(BOOL completed) {
	// 	if (completed) {
	// 		NSString *sx = [NSString stringWithFormat:@"%d",left];
	// 		NSString *sy = [NSString stringWithFormat:@"%d",top];
	// 		[self setUserPreference:sx forKey:@"gamesparkPosX"];
	// 		[self setUserPreference:sy forKey:@"gamesparkPosY"];
	// 		NSLog(@"[2]left:%d,top:%d", left, top);
	// 	}
	// }];
	NSString *sx = [NSString stringWithFormat:@"%d",(int)self.center.x];
	NSString *sy = [NSString stringWithFormat:@"%d",(int)self.center.y];
	[self setUserPreference:sx forKey:@"gamesparkPosX"];
	[self setUserPreference:sy forKey:@"gamesparkPosY"];

	[super touchesEnded: touches withEvent: event];	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
@end
