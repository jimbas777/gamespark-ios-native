//
//  Gamespark.m
//  Gamespark
//
//  Created by Jimmy Basselo on 3/5/15.
//  Copyright (c) 2015 mainpark. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h> // Need to import for CC_MD5 access
#import <CommonCrypto/CommonCryptor.h>

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#import "Gamespark.h"
#import "FloatingIcon.h"
#import "KeychainItemWrapper.h"

// ini dari header, utk sementara jgn dulu
//- (void) enableTouchToClose:(BOOL)touchToClose;

@interface GPUIView : UIView {
	id<GPUIViewDelegate> _delegate;
}
@property (nonatomic,strong) id delegate;
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event;
@end

@implementation GPUIView
@synthesize delegate;
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if( point.x > 0 && point.x < self.frame.size.width && point.y > 0 && point.y < self.frame.size.height ) {
        [delegate uiViewTouched:YES];
        return YES;
    }
    [delegate uiViewTouched:NO];
    return NO;
}
@end

enum {
	ALID_NONE,
	ALERTID_CHECKJAR
};

BOOL gbProduction = YES;
NSMutableArray *mAls = nil;

@interface Gamespark () {
	BOOL tabBarViewShown;
	GPUIView *tabBarView;
	FloatingIcon *myButton;
	UIWebView* mWebView;
	UIButton *btnFbBack;
	UIActivityIndicatorView* mActivityIndicView;
	NSString *msSecretKey;
	GamesparkOnReturn mGpOnReturn;
	NSString *msProductId;
	NSString *mProductName;
	NSString *msChannelId;
	BOOL mbJimbasOrg;
	NSString *msCheckJar;
	NSString *msUserId;
	NSString *msIndexUrl;
	NSString *msLoginUrl;
	NSString *msRegisterUrl;
	NSString *msFbUrl;
	NSString *msProfileUrl;
	NSString *msLogoutUrl;
	NSString *msBindEmail;
	NSString *msBindFb;
	NSString *msSupportUrl;
	NSString *msQuickStartUrl;
	NSString *msQuickLoginUrl;
	BOOL mbLoggedIn;
	//NSString *msPage;
	NSUInteger nTag;
	NSString *sConnResp;
	NSDictionary *msDict;
	NSUInteger nAlertId;
	//NSTimer *mConnectionTimer;
	NSTimer *mTimerTouchToClose;
	NSUInteger nCheckJarErrorRet;
	BOOL mbTouchToClose;
}
@property(nonatomic,strong)UIViewController *uiViewCtrl;
@property(nonatomic,strong)GPUIView *tabBarView;
@property(nonatomic,strong)UIWebView *mWebView;
@property(nonatomic,strong)UIActivityIndicatorView *mActivityIndicView;
@end

@implementation Gamespark

@synthesize tabBarView;
@synthesize mWebView;
@synthesize mActivityIndicView;
@synthesize uiViewCtrl;

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Versioning
	//
	// 1.0
	// 	- Initial release
	// 1.1,1.2,1.3
	// 	- Skipped to standardize all versioning number for SDK & the PDF documentation
	// 1.4
	// 	- page support is available now
	// 	- bug minor fixed, to close logout busy indicator
	// 	- add getSdkVersion function
	// 	- add enableGateway function
	// 1.4.1
	// 	- bug fix when register new user.
	// 1.4.2
	// 	- remove classes that refer to iOS7 and later. This will allow running on iOS 6 down to 5
	// 	- bug fix rotate handling in iOS7 and below
	// 1.4.3
	// 	- fix back button on register
	// 1.4.4
	// 	- implement quick start
	//  - implement multiple user account
	// 1.4.5
	// 	- fixed several problems in quick start & its multiple user.
	// 1.4.6
	// 	- bug fixed, return message (respText) null onGamesparkReturn, null'ed by ARC
	// 	- bug fixed, when logout
	// 1.4.6.1
	// 	- bug on V146 still exist. duplicate respText to other var for onGamesparkReturn
	// 1.4.7
	// 	- now we activate custom header, standardize iOS & Android in how to access index page
	// 1.4.8
	// 	- it is now running good in iOS 6
	//	- bug fixed, floating icon animated tends to 'sunk' at lower left corner or at upper right corner
	//	- remember the last floating icon position.
	//	- add small circle button at upper left corner as a back button when it shows facebook login page
	// 1.4.9
	// 	- add channel parameter to distinguish mobile store
	// 	- file logging
	//
	// 1.5.0
	// 	- minor bug fixed on channel_id implementation
	// 1.5.1
	// 	- fixed all binding, facebook and email
	//	- implement new toast message for all success messages.
	// 1.5.2
	// 	- fixed all binding, when using existing account
	// 1.5.2.1
	// 	- add new function destroy, to allow developer to destroy if there is something went wrong
	//
	////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString*) getSdkVersion
{
	return [NSString stringWithFormat:@"%@",@"v1.5.0"];
}

void dp(NSString *format, ...)
{
	if(gbProduction) return;
	va_list args;
	va_start(args, format);
	NSLogv([NSString stringWithFormat:@"Gamespark:%@", format], args);
	va_end(args);
	//#if TARGET_IPHONE_SIMULATOR == 0
	NSString *s = [[NSString alloc] initWithFormat:format arguments:args];
	if(mAls == nil) mAls = [[NSMutableArray alloc] init];
	[mAls addObject:s];
	if([mAls count] < 16) return;
	[mAls removeObjectAtIndex:0]; 
	//#endif
}

- (void) flushLog
{
	if(mAls == nil) return;
	NSString *sals = [mAls componentsJoinedByString:@"\n"];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docdir = [paths objectAtIndex:0];
	NSString *filePath = [NSString stringWithFormat:@"%@/%@", docdir, @"gplog.txt"];
	if([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
		NSError *error;
		[sals writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
		if(error == nil) {
			NSLog(@"written to %@", filePath);
		} else {
			NSLog(@"write file error: %@", error);
		}
	} else {
		NSLog(@"path not writeable");
	}
}

- (void) enableGateway:(BOOL)productionMode
{
	if(productionMode) {
		msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"https://api.gamespark.net/api/v1/check/jar"];
		gbProduction = YES;
		return;
	}
	msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://staging.gamespark.net/api/v1/check/jar"];
	gbProduction = NO;
}

- (void)create:(UIViewController*)uivc gamesparkOnReturn:(GamesparkOnReturn)gamesparkOnReturn
	 productId:(NSString*)productId productName:(NSString*)productName channelId:(NSString*)channelId
{
	gbProduction = NO;
	//msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://www.jimbas.org/myoffice/gamespark/sdk/devel/checkjar/"];
	msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://staging.gamespark.net/api/v1/check/jar"];
	
	msSecretKey = [[NSString alloc] initWithFormat:@"%@",@"XJf8o3OJ4qtaOYaBYO4XOtwMMJiBD6qG"];
	msProductId = [[NSString alloc] initWithFormat:@"%@",productId];
	mProductName = [[NSString alloc] initWithFormat:@"%@",productName];
	msChannelId = [[NSString alloc] initWithFormat:@"%@",channelId];
	self.uiViewCtrl = uivc;
	mGpOnReturn = gamesparkOnReturn;
	[self clearVars];
	// this will ensure facebook js read the cookie
	[[NSHTTPCookieStorage sharedHTTPCookieStorage]setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
}

- (void) clearVars
{
	mbJimbasOrg = NO;
	mbLoggedIn = NO;	
	msUserId = nil;
	msIndexUrl = nil;
	msLoginUrl = nil;
	msRegisterUrl = nil;
	msFbUrl = nil;
	msProfileUrl = nil;
	msLogoutUrl = nil;
	msBindEmail = nil;
	msBindFb = nil;
	msSupportUrl = nil;
	msQuickStartUrl = nil;
	msQuickLoginUrl = nil;
	//msPage = nil;
	nTag = 0;
	sConnResp = nil;
	msDict = nil;
	nAlertId = 0;
	//mConnectionTimer = nil;
	mTimerTouchToClose = nil;
	nCheckJarErrorRet = 999;
	mbTouchToClose = NO;
	btnFbBack = nil;
}

-(void) enableTouchToClose:(BOOL)touchToClose;
{
	mbTouchToClose = touchToClose;
}

-(void) touchToClose:(NSTimer*)tmr
{
	mTimerTouchToClose = nil;
}

- (void) uiViewTouched:(BOOL)wasInside
{
	if(!mbTouchToClose) return;
	if(!wasInside) {
		if(mTimerTouchToClose == nil) {
			mTimerTouchToClose = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(touchToClose:) userInfo:nil repeats:NO];
		} else {
			// close the thing here...
			[mTimerTouchToClose invalidate];
			mTimerTouchToClose = nil;
			if(tabBarView != nil) {
				tabBarView.hidden = YES;
				tabBarViewShown = NO;
				NSString *errmsg = [NSString stringWithFormat:@"User Canceled"];
				[self displayToastWithMessage:errmsg];
			    mGpOnReturn(GPRET_LOGIN_CANCELED, errmsg);
			}
		}
	}
}

- (void)execute:(NSUInteger)tag
{
	NSString *errmsg = nil;
	switch(tag) {
	case GPEXEC_LOGIN:
		if(mbLoggedIn) {
			errmsg = [NSString stringWithFormat:@"Logged in already"];
			[self displayToastWithMessage:errmsg];
			mGpOnReturn(GPRET_LOGIN_ALREADY, errmsg);
			return;
		}
		nCheckJarErrorRet = GPRET_LOGIN_ERROR;
		break;
	case GPEXEC_LOGOUT:
		if(!mbLoggedIn) {
			errmsg = [NSString stringWithFormat:@"Logged out already or never login"];
			[self displayToastWithMessage:errmsg];
			mGpOnReturn(GPRET_NOT_LOGIN_YET, errmsg);
			return;
		}
		nCheckJarErrorRet = GPRET_LOGOUT_ERROR;
		break;
	// case 999: { // test
	// 		NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	// 		for(NSHTTPCookie *cookie in [storage cookies]) {
	// 			dp(@"cookies: %@", cookie);
	// 		}
	// 		return;
	// 	}
	// 	break;
	default:
		errmsg = [NSString stringWithFormat:@"Unknown Tag"];
		[self displayToastWithMessage:errmsg];
		mGpOnReturn(GPRET_UNKNOWN_TAG, errmsg);
		return;
	}

	nTag = tag;
	[self loadProgress];
	[self getCheckJar];
}

- (NSString*) getIndexInJsonArray:(NSString*)jarrstr stringToCheck:(NSString*)str
{
    int i;
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    for(i=0;i<[values count];i++) {
        if([[values objectAtIndex:i] isEqualToString:str]) {
            return [NSString stringWithFormat:@"%d",i];
        }
    }
    return [NSString stringWithFormat:@"%@",@"-1"];
}

- (NSString*) getStringFromJsonArray:(NSString*)jarrstr indexString:(NSString*)indexString
{
    int idx = [indexString intValue];
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSString* str = [[NSString alloc] initWithFormat:@"%@",[values objectAtIndex:idx]];
    return str;
}

- (NSString*) addStringToJsonArray:(NSString*)jarrstr stringToSet:(NSString*)str
{
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    [values addObject:str];
    data = [NSJSONSerialization dataWithJSONObject:values options:0 error:nil];
    NSString *jsonstr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return jsonstr;
}

- (NSString*) setStringInJsonArray:(NSString*)jarrstr indexString:(NSString*)indexString stringToSet:(NSString*)stringToSet
{
    int idx = [indexString intValue];
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
	[values setObject:stringToSet atIndexedSubscript:idx];
    data = [NSJSONSerialization dataWithJSONObject:values options:0 error:nil];
    NSString *jsonstr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return jsonstr;
}

- (void) normalizaUserAcountInPreference
{
	NSString *email = [self getUserPreference:@"gamesparkEmail"];
	if(email == nil || [email isEqualToString:@"(null)"]) [self setUserPreference:@"[]" forKey:@"gamesparkEmail"];
	NSString *password = [self getUserPreference:@"gamesparkPassword"];
	if(password == nil || [password isEqualToString:@"(null)"]) [self setUserPreference:@"[]" forKey:@"gamesparkPassword"];
	NSString *userid = [self getUserPreference:@"gamesparkUserId"];
	if(userid == nil || [userid isEqualToString:@"(null)"]) [self setUserPreference:@"[]" forKey:@"gamesparkUserId"];
	NSString *loggedin = [self getUserPreference:@"gamesparkLoggedIn"];
	if(loggedin == nil || [loggedin isEqualToString:@"(null)"]) [self setUserPreference:@"-1" forKey:@"gamesparkLoggedIn"];
}

- (void) executeResume
{	
	//[self setUserPreference:@"-1" forKey:@"gamesparkLoggedIn"];
	// get user pref, email, password, & userid
	[self normalizaUserAcountInPreference];
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	dp(@"executeResume email:%@ password:%@ userid:%@ loggedin:%@", spref0, spref1, spref2, spref3);
	
	BOOL userdetexist = [spref3 isEqualToString:@"-1"] ? NO : YES;//?

	if(userdetexist) {	
		//msUserId = [self getStringFromJsonArray:userid indexString:loggedin];
		msUserId = [self getDecryptedPassword:[self getStringFromJsonArray:spref2 indexString:spref3]];
	}

	//if([msPage isEqualToString:@"login"]) {
	switch(nTag) {
	case GPEXEC_LOGIN: {
			// init all views here
			[self initTabBarView];
			[self initWebView];
			[self initButtonFbBack];
			[self initFloatingIcon];
			tabBarViewShown = NO;
			dp(@"requesting login");
			BOOL blandingpage = YES;
			if(userdetexist) {
				// if(password != nil && ![password isEqualToString:@""]) {
				dp(@"user details exist. try to logging in...");
				//loadProgress(); ini krn IGamespark
				NSString *semail = [self getStringFromJsonArray:spref0 indexString:spref3];
				NSString *spassword = [self getStringFromJsonArray:spref1 indexString:spref3];
				if([semail isEqualToString:@"Guest"]) {
					[self doQuickStart:msQuickLoginUrl];
				} else if([semail isEqualToString:@"Facebook_Enabled"]) {
					dp(@"Login using facebook account on Facebook Binding instead");
					[self doLoginWithFacebook:[self getDecryptedPassword:spassword] bind:NO];
					//new PostTask().execute(new String[]{ msFbUrl.replace("{product_id}", msProductId), sep, "onReturnFbLogin" });
				} else {
					[self doLogin:semail encryptedPassword:[self getDecryptedPassword:spassword]];
				}
				blandingpage = NO;
				// }
				// jika masih gagal, mungkin karena data yg tersimpan di preference tiba2 kacau,
				// ask to user to clear data atau clear cache
			} else {
				NSString *spref6 = [self getUserPreference:@"gamesparkFbStuff"];
				if(spref6 != nil && ![spref6 isEqualToString:@""]) {
					dp(@"Was login using facebook previously. Do Facebook login");
					[self doLoginWithFacebook:spref6 bind:NO];
					blandingpage = NO;
				}
			}
			/////////////////
			//blandingpage = YES;
			//email = [NSString stringWithFormat:@"%@", @"[\"guest\",\"jimbas777@gmail.com\"]"];
			// NSDictionary *msDict = [[NSDictionary alloc] initWithObjectsAndKeys:
			// 	msProductId,@"product_id",
			// 	spref0,@"select",
			// 	nil
			// ];
			// NSData *jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
			// NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
			// dp(@"jsonstr: %@", jsonstr);
			/////////////////
			if(blandingpage) {
				[tabBarView setHidden:NO];
				tabBarViewShown = YES;
				//loadProgress(); ini krn IGamespark
				//[self initFloatingIcon];

				//msIndexUrl = [NSString stringWithFormat:@"%@", @"http://staging.gamespark.net/api/v1/user/new_index"];
				// NSMutableString* indexUrl = [[NSMutableString alloc] initWithFormat:@"%@", msIndexUrl];
				// indexUrl = [[indexUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
				//dp(@"indexUrl: %@", indexUrl);

				NSString *encspref0 = (__bridge NSString*)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)spref0, NULL,
					(CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 );
				NSString *indexUrl = [NSString stringWithFormat:@"%@?product_id=%@&select=%@",msIndexUrl,msProductId,encspref0];
				//msIndexUrl + "?" + String.format("product_id=%s&select=%s", msProductId, URLEncoder.encode(email))
				[self loadUrlRequest:YES url:indexUrl postParam:nil];
				//[self loadUrlRequest:YES url:msIndexUrl postParam:jsonstr];
				//[self loadUrlRequest:NO url:@"http://www.jimbas.org/myoffice/gamespark/sdk/devel/landingPage/landingPage.html"];
			}
		}
		break;
	case GPEXEC_LOGOUT: {
			[self doLogout];
		}

		break;
	}
}

- (void) destroy
{
	if(tabBarView != nil && myButton != nil) {
		[self->mWebView removeFromSuperview];
		[self->tabBarView removeFromSuperview];
		[self->myButton removeFromSuperview];
		tabBarView = nil;
		mWebView = nil;
		myButton = nil;
		btnFbBack = nil;
		[self clearVars];
	}
}

- (void) loadProgress
{
	if(mActivityIndicView == nil) {
		mActivityIndicView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		mActivityIndicView.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width / 2, self.uiViewCtrl.view.bounds.size.height / 2);
	}
	if(mActivityIndicView.superview == nil) {
		[mActivityIndicView startAnimating];
		[self.uiViewCtrl.view addSubview:self.mActivityIndicView];
		if(tabBarView != nil) {
			[tabBarView setAlpha:0.5];
		}
	}
}

- (void) closeProgress
{
	if(mActivityIndicView != nil) {
		[mActivityIndicView removeFromSuperview];
		mActivityIndicView = nil;
		if(tabBarView != nil) {
			[tabBarView setAlpha:1.0];
		}
	}
}

- (void) connectError:(NSError*)error
{
	dp(@"connectError: %@", error);
	[self closeProgress];
	[self doAlert:@"Gamespark Connection Problem" message:@"Unable to connect to server. Press OK to retry." alertid:ALERTID_CHECKJAR];
	mGpOnReturn(GPRET_CONNECT_ERROR, @"Unable to connect to server");
}

- (NSString*) decodeFromPercentEscapeString:(NSString*)string
{
	return (__bridge NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
		(__bridge CFStringRef) string, CFSTR(""), kCFStringEncodingUTF8);
} 

- (void) getCheckJar
{
	if([msCheckJar rangeOfString:@"jimbas.org"].location == NSNotFound) {
	} else {
		mbJimbasOrg = true;
		dp(@"using jimbas.org detected");
	}

	NSString *realchkjar = [self decodeFromPercentEscapeString:msCheckJar];
	//[msCheckJar stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

	[self connectServer:realchkjar postParam:nil
	  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		  //[mConnectionTimer invalidate];
		  NSString * respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		  dp(@"getCheckJar:respText: %@",respText);
		  if(error == nil) {
			  // parse check jar json
			  NSData* respData = [respText dataUsingEncoding:NSUTF8StringEncoding];
			  NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:respData options:NSJSONReadingMutableContainers error:&error];
			  if(!dict) {
				  dp(@"getCheckJar:jsonParseError: %@", error);
				  mGpOnReturn(nCheckJarErrorRet, respText);
				  return;
			  }
			  msIndexUrl = [[NSString alloc] initWithFormat:@"%@",[self decodeFromPercentEscapeString:dict[@"indexURL"]]];
			  msLoginUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"loginURL"]]];
			  msRegisterUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"registerURL"]]];
			  msFbUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"fbURL"]]];
			  msProfileUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"profileURL"]]];
			  msLogoutUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"logoutURL"]]];
			  msBindEmail = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"bindEmail"]]];
			  msBindFb = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"bindFB"]]];
			  msSupportUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"supportURL"]]];
			  msQuickStartUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"quickstartURL"]]];
			  msQuickLoginUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"quickLoginURL"]]];

			  dp(@"\nmsIndexUrl: %@\nmsLoginUrl: %@\nmsRegisterUrl: %@\nmsFbUrl: %@\nmsProfileUrl: %@\nmsLogoutUrl: %@\nmsBindEmail: %@\nmsBindFb: %@\nmsSupportUrl: %@\nmsQuickStartUrl: %@\nmsQuickLoginUrl: %@",
					msIndexUrl, msLoginUrl, msRegisterUrl, msFbUrl, msProfileUrl, msLogoutUrl, msBindEmail, msBindFb, msSupportUrl, msQuickStartUrl, msQuickLoginUrl);
			  [self executeResume];
		  } else {
			  [self connectError:error];
		  }
	  }
	 ];
}

- (id) getUserPreference:(NSString*)forKey
{
	NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
	return [defaults valueForKey:forKey];
}

- (void) setUserPreference:(id)value forKey:(NSString*)key
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:value forKey:key];
	[defaults synchronize];
}

// - (NSString*) decodeEncryptedPassword:(NSString*) scodedencryptedpassword
// {
// 	return scodedencryptedpassword;
// }

- (void) doLogout
{
	// this is just for testing logout
	//msUserId = [[NSString alloc] initWithFormat:@"%@", @"553f0adf891e40723e000005"];
	//dp(@"msLogoutUrl:%@, msUserId:%@", msLogoutUrl, msUserId);
	NSMutableString* logoutUrl = [[NSMutableString alloc] initWithFormat:@"%@", msLogoutUrl];
	logoutUrl = [[logoutUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
	msDict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		msUserId,@"user_id",
	nil];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doLogout:msLogoutUrl:%@, msUserId:%@ logoutUrl:%@", msLogoutUrl, msUserId, logoutUrl);
	////// kirim ke server
	[self connectServer:logoutUrl postParam:jsonstr
	  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		  //[mConnectionTimer invalidate];
		  if(mbJimbasOrg) {
			  dp(@"logout success test - jimbas.org");
			  NSString *sresult = [NSString stringWithFormat:@"%@",@"{\"success\":true}"];
			  data = [sresult dataUsingEncoding:NSUTF8StringEncoding];
			  error = nil;
		  }		  		  
		  NSString *respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		  //if(!mbJimbasOrg) {
		  dp(@"doLogout:respText: %@",respText);
		  //}
		  if(error == nil) {
			  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
			  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
			  if(!dictb) {
				  dp(@"doLogout:errora: %@", error);
				  // --- kita skip aja, server suka laravel error :(
				  [self clearFacebookCookies];
				  [self clearPreferences];
				  [self setUserPreference:[NSString stringWithFormat:@""] forKey:@"gamesparkFbStuff"];
				  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__logout_success" repeats:NO];
				  mGpOnReturn(GPRET_LOGOUT_SUCCESS, @"Logout success");
				  // --- jadi logout akan selalu sukses :)
				  // respText = [NSString stringWithFormat:@"%@",error];
				  // mGpOnReturn(GPRET_LOGOUT_ERROR, respText);
				  return;
			  }
			  NSNumber *stat = [dictb valueForKey:@"success"];
			  if([stat isEqual: @YES]) {
				  [self clearFacebookCookies];
				  [self clearPreferences];
				  [self setUserPreference:[NSString stringWithFormat:@""] forKey:@"gamesparkFbStuff"];
				  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__logout_success" repeats:NO];
				  mGpOnReturn(GPRET_LOGOUT_SUCCESS, respText);
			  } else {
				  //NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('logoutFailed', '')"];
				  //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
				  NSString *msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"message"]];
				  if(msg == nil || [msg isEqualToString:@"(null)"]) {
					msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"Unable to logout from server"];
					}
				  }
				  [self doAlert:@"Gamespark Logout Failed" message:msg alertid:ALID_NONE];
				  mGpOnReturn(GPRET_LOGOUT_ERROR, respText);
			  }
		  } else {
			  [self connectError:error];
		  }
		  [self closeProgress];
	  }
	 ];
}

- (void) doQuickStart:(NSString*)quickUrl
{
	//NSDictionary* dict = @{ @"email":email, @"password":encryptedPassword };
	msDict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		[self getDeviceId],@"uuid",
		msChannelId,@"channel_id",
	nil];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doQuickStart: quickUrl:%@ jsonstr:%@", quickUrl, jsonstr);
	[self connectServer:quickUrl postParam:jsonstr
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		  //[mConnectionTimer invalidate];
		  NSString * respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		  dp(@"doQuickStart:respText: %@",respText);
		  if(error == nil) {
			  /////////////////
			  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
			  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
			  if(!dictb) {
				  dp(@"doQuickStart:error: %@", error);
				  respText = [NSString stringWithFormat:@"%@",error];
				  [self doAlert:@"Gamespark QuickStart Failed" message:respText alertid:ALID_NONE];
				  mGpOnReturn(GPRET_LOGIN_ERROR, respText);
				  return;
			  }
			  //BOOL stat = (BOOL)[dictb valueForKey:@"success"];
			  NSNumber *stat = [dictb valueForKey:@"success"];//?
			  if([stat isEqual: @YES]) {
				  dp(@"quickstart success");
				  // sampe sini lidx seharusnya tidak sama dengan -1
				  msUserId = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
				  NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
				  NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
				  NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
				  NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
				  NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];
				  spref2 = [self setStringInJsonArray:spref2 indexString:lidx stringToSet:[self getEncryptedPassword:msUserId]];
				  spref3 = lidx;
				  dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
				  [self setUserPreference:spref0 forKey:@"gamesparkEmail"];
				  [self setUserPreference:spref1 forKey:@"gamesparkPassword"];
				  [self setUserPreference:spref2 forKey:@"gamesparkUserId"];
				  [self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
				  [tabBarView setHidden:YES];
				  tabBarViewShown = NO;
				  [myButton setHidden:NO];
				  [myButton moveButton:YES];
				  mbLoggedIn = YES;
				  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__login_success" repeats:NO];
				  mGpOnReturn(GPRET_LOGIN_SUCCESS, respText);
			  } else {
				  dp(@"quick start failed!");
				  NSString *msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"message"]];
				  if(msg == nil || [msg isEqualToString:@"(null)"]) {
					msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"Failed on trying Quickstart"];
					}
				  }
				  [self doAlert:@"Gamespark QuickStart Failed" message:msg alertid:ALID_NONE];
				  //NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('loginFailed', '%@')",msg];
				  //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
				  mGpOnReturn(GPRET_LOGIN_ERROR, respText);
			  }
		  } else {
			  [self connectError:error];
		  }
	  }
	 ];
}

- (void) doLogin:(NSString*)email encryptedPassword:(NSString*)encryptedPassword
{
	//NSDictionary* dict = @{ @"email":email, @"password":encryptedPassword };
	msDict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		email,@"email",
		encryptedPassword,@"password",
		msChannelId,@"channel_id",
		nil
	];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doLogin:jsonString: %@", jsonstr);
	[self connectServer:msLoginUrl postParam:jsonstr
	  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		  //[mConnectionTimer invalidate];
		  NSString * respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		  dp(@"doLogin:respText: %@",respText);
		  if(error == nil) {
			  /////////////////
			  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
			  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
			  if(!dictb) {
				  dp(@"doLogin:error: %@", error);
				  respText = [NSString stringWithFormat:@"%@",error];
				  [self doAlert:@"Gamespark Login Failed" message:respText alertid:ALID_NONE];
				  mGpOnReturn(GPRET_LOGIN_ERROR, respText);
				  return;
			  }
			  //BOOL stat = (BOOL)[dictb valueForKey:@"success"];
			  NSNumber *stat = [dictb valueForKey:@"success"];
			  if([stat isEqual: @YES]) {
				  msUserId = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
				  NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
				  NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
				  NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
				  NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
				  NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:msDict[@"email"]];
				  if([lidx isEqualToString:@"-1"]) {
					  spref0 = [self addStringToJsonArray:spref0 stringToSet:msDict[@"email"]];
					  spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:msDict[@"password"]]];
					  lidx = [self getIndexInJsonArray:spref0 stringToCheck:msDict[@"email"]];
				  }
				  spref2 = [self setStringInJsonArray:spref2 indexString:lidx stringToSet:[self getEncryptedPassword:msUserId]];
				  spref3 = lidx;
				  dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
				  [self setUserPreference:spref0 forKey:@"gamesparkEmail"];
				  [self setUserPreference:spref1 forKey:@"gamesparkPassword"];
				  [self setUserPreference:spref2 forKey:@"gamesparkUserId"];
				  [self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
				  [tabBarView setHidden:YES];
				  tabBarViewShown = NO;
				  [myButton setHidden:NO];
				  [myButton moveButton:YES];
				  mbLoggedIn = YES;
				  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__login_success" repeats:NO];
				  mGpOnReturn(GPRET_LOGIN_SUCCESS, respText);
			  } else {
				  dp(@"login failed!");
				  NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"message"]];
				  if(msg == nil || [msg isEqualToString:@"(null)"]) {
					msg = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"Login Failed. Please make sure correct email & password"];
					}
				  }
				  [self doAlert:@"Gamespark Login" message:msg alertid:ALID_NONE];
				  // NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('loginFailed', '%@')",msg];
				  // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
				  mGpOnReturn(GPRET_LOGIN_ERROR, respText);
			  }
		  } else {
			  [self connectError:error];
		  }
	  }
	 ];
}

- (void) doAlert:(NSString*)title message:(NSString*)message alertid:(NSUInteger)alertid
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
		message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	nAlertId = alertid;
}

- (void)initFloatingIcon
{
	if(myButton == nil) {
		myButton = [FloatingIcon buttonWithType:UIButtonTypeCustom];
		myButton.MoveEnable = YES;
		myButton.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width/2, self.uiViewCtrl.view.bounds.size.height/2);
		myButton.frame = CGRectMake(myButton.center.x, myButton.center.y, 40, 40);
		[myButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"asstvlogo.png"]] forState:UIControlStateNormal];		
		myButton.layer.cornerRadius = 40/2;//5;
		//myButton.layer.backgroundColor = [[UIColor blackColor] CGColor];
		myButton.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
		myButton.layer.borderWidth = 2.0; // set as you want.
		myButton.clipsToBounds = YES;
		
		[myButton setTag:10];
		[myButton addTarget:self action:@selector(floatIconClicked:) forControlEvents:UIControlEventTouchUpInside];
		[self.uiViewCtrl.view addSubview:myButton];
		[myButton setHidden:YES];
	} else {
		myButton.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width/2, self.uiViewCtrl.view.bounds.size.height/2);
		myButton.frame = CGRectMake(myButton.center.x, myButton.center.y, 40, 40);
	}

	//tabBarViewShown = NO;
	//[self initTabBar];
	//[self initWebView];
}

- (void) updateView
{
	[self initTabBarView];
	[self initFloatingIcon];
	[myButton moveButton:YES];
}

-(void)initTabBarView
{
	float fa = self.uiViewCtrl.view.bounds.size.width;
	float fb = self.uiViewCtrl.view.bounds.size.height;
	dp(@"width:%.2f height:%.2f", fa, fb);
	float fc = fa < fb ? fa : fb;
	float fd = fc;
	fc *= 0.80f;
	fd *= 0.95f;
	if(tabBarView == nil) {
		tabBarView = [[GPUIView alloc] initWithFrame:CGRectMake((fa/2)-(fc/2), (fb/2)-(fd/2), fc , fd)] ;
		tabBarView.layer.cornerRadius = 10;
		tabBarView.layer.backgroundColor = [[UIColor whiteColor] CGColor];
		tabBarView.alpha = 0.5f;
		tabBarView.layer.masksToBounds = YES;
		tabBarView.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
		tabBarView.layer.borderWidth = 3.0; // set as you want.
		//tabBarView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		tabBarView.delegate = self;
		[self.uiViewCtrl.view addSubview:tabBarView];
		[tabBarView setHidden:YES];
	} else {
		tabBarView.frame = CGRectMake((fa/2)-(fc/2), (fb/2)-(fd/2), fc , fd);
	}
}

-(void)initWebView
{
	if(mWebView != nil) return;
	mWebView = [[UIWebView alloc]initWithFrame:self.tabBarView.bounds];
	mWebView.delegate = self;
	[self.tabBarView addSubview:mWebView];
	[[NSURLCache sharedURLCache] removeAllCachedResponses];

	//[self initFloatingIcon];
	//[self.uiViewCtrl.view insertSubview:myButton aboveSubview:mWebView];
	//[tabBarView setHidden:YES];
}

- (void) buttonFbBackDown:(id)sender
{
	UIButton *btn = sender;
	btn.backgroundColor = [UIColor colorWithRed:0.0 green:128.0/255.0f blue:255.0f alpha:0.5f];
}

- (void) buttonFbBack:(id)sender
{
	UIButton *btn = sender;
	[btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	btn.backgroundColor = [UIColor colorWithRed:0.0 green:128.0/255.0f blue:255.0f alpha:1.0f];
	[btnFbBack setHidden:YES];
	//[mWebView goBack];
	[self loadProgress];
	NSMutableString* profileUrl = [[NSMutableString alloc] initWithFormat:@"%@", msProfileUrl];
	//dp(@"profileUrl2: %@ msUserId: %@", profileUrl, msUserId);
	profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
	profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
	//dp(@"profileUrl3: %@", profileUrl);
	[self loadUrlRequest:YES url:profileUrl postParam:nil];
}

-(void)initButtonFbBack
{
	if(btnFbBack != nil) return;
	int roundWidthHeight = 20;
	btnFbBack = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, roundWidthHeight, roundWidthHeight)];
	//btnFbBack.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	btnFbBack.contentEdgeInsets = UIEdgeInsetsMake(-2, 0, 0, 0);
	[btnFbBack setTitle:@"<" forState:UIControlStateNormal];
	[btnFbBack addTarget:self action:@selector(buttonFbBack:) forControlEvents:UIControlEventTouchUpInside];
	[btnFbBack addTarget:self action:@selector(buttonFbBackDown:) forControlEvents:UIControlEventTouchDown];

	[btnFbBack.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnFbBack.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
	//btnFbBack.clipsToBounds = YES;
	btnFbBack.layer.cornerRadius = roundWidthHeight/2.0f;
	btnFbBack.backgroundColor = [UIColor colorWithRed:0.0 green:128.0/255.0f blue:255.0f alpha:1.0f];
	[btnFbBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	//btnFbBack.layer.borderColor = [UIColor blackColor].CGColor;
	//btnFbBack.layer.borderColor = [UIColor colorWithRed:0.000 green:0.000 blue:1.000 alpha:0.750].CGColor;
	//btnFbBack.tintColor = [UIColor colorWithRed:0.764 green:1.000 blue:0.000 alpha:1.000];
	//btnFbBack.layer.borderWidth=1.5f;

	[self.mWebView addSubview:btnFbBack];
	[btnFbBack setHidden:YES];
}

- (void) loadUrlWithBrowser:(NSString*) surl
{
	NSURL* url = [NSURL URLWithString:surl];
	if(![[UIApplication sharedApplication] openURL:url]) {
		NSLog(@"%@%@",@"Failed to open url:",[url description]);
	}
}

- (void) loadUrlRequest:(BOOL)withCustomHeader url:(NSString*) url postParam:(NSString*) postParam
{
	dp(@"loadUrlRequest:url: %@ customHeader:%@", url, withCustomHeader ? @"Yes": @"No");
	/*if(mWebView == nil) {
		[self initTabBarView];
		[self initWebView];
		[[NSURLCache sharedURLCache] removeAllCachedResponses];
	}*/
	NSURL* nsurl = nil;
	if(url == nil || [url isEqualToString:@"(null)"]) {
		nsurl= [NSURL URLWithString:@"about:blank"];
	} else {
		nsurl= [NSURL URLWithString:url];
	}
	NSURLRequest* nsrequest = nil;
	NSMutableURLRequest* nsmutablereq = nil;
	if(withCustomHeader) {
		NSString *scursystm = [self getUnixTimeStamp];
		NSMutableString* str = [[NSMutableString alloc] init];
		[str appendString:msSecretKey];
		[str appendString:scursystm];
		nsmutablereq = [NSMutableURLRequest requestWithURL:nsurl];
		[nsmutablereq setValue:scursystm forHTTPHeaderField:@"timestamp"];
		[nsmutablereq setValue:[self getMd5:str] forHTTPHeaderField:@"token"];
		//nsrequest = [nsmutablereq mutableCopy];
	} else {
		//nsrequest = [NSURLRequest requestWithURL:nsurl];
		// this will ensure that facebook js read the cookies
		nsmutablereq = [NSMutableURLRequest requestWithURL:nsurl];
		[nsmutablereq setHTTPShouldHandleCookies:YES];
	}
	if(postParam != nil) {
		//NSURL *url = [NSURL URLWithString: @"http://your_url.com"];
		//NSString *body = [NSString stringWithFormat: @"arg1=%@&arg2=%@", @"val1",@"val2"];
		//NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL: url];
		[nsmutablereq setHTTPMethod: @"POST"];
		[nsmutablereq setHTTPBody: [postParam dataUsingEncoding: NSUTF8StringEncoding]];
		//[webView loadRequest: request];
	}
	nsrequest = [nsmutablereq mutableCopy];
	[mWebView loadRequest:nsrequest];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	//dp(@"webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	NSString *s = webView.request.URL.absoluteString;
	dp(@"webViewDidFinishLoad: %@", s);

	if([s rangeOfString:@"gamespark.net"].location == NSNotFound){}else {
	  if([s rangeOfString:@"user/index"].location == NSNotFound){}else {
		if(mbLoggedIn) {
			if(mActivityIndicView == nil) {
				mActivityIndicView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
				mActivityIndicView.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width / 2, self.uiViewCtrl.view.bounds.size.height / 2);
			}
			if(mActivityIndicView.superview == nil) {
				[mActivityIndicView startAnimating];
				[self.uiViewCtrl.view addSubview:self.mActivityIndicView];
			}
			return;
		}
	}}

	if([s rangeOfString:@"gamespark.net"].location == NSNotFound){}else {
	  if([s rangeOfString:@"user/profile"].location == NSNotFound){}else {
		if(mWebView != nil) {
			[mWebView setAlpha:1.0];
		}
	}}

	if([s rangeOfString:@"facebook.com"].location == NSNotFound){}else {
	  if([s rangeOfString:@"login"].location == NSNotFound){}else {
		[btnFbBack setHidden:NO];
	}}

	[self fitContentForWebviewResize];
	[self closeProgress];
}

// JAVASCRIPT interface goes here...
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	//dp(@"shouldStartLoadWithRequest. scheme:%@ host:%@ fragment:%@", request.URL.scheme, request.URL.host, request.URL.fragment);
	dp(@"shouldStartLoadWithRequest: %@", request.URL.absoluteString);
	NSError* error = nil;
	NSString *s=nil,*t=nil,*u=nil;
	//NSDictionary *dictb = nil;
	//amesparkDemo[19699:495572] shouldStartLoadWithRequest. scheme:https host:m.facebook.com fragment:_=_
	
	if(![request.URL.scheme isEqualToString:@"igamespark"]) {
		if([request.URL.scheme isEqualToString:@"https"] && [request.URL.host rangeOfString:@"facebook.com"].location != NSNotFound
		   && request.URL.fragment != nil && [request.URL.fragment rangeOfString:@"_=_"].location != NSNotFound) {
			 dp(@"facebook login successful");
			[btnFbBack setHidden:YES];
			// [mWebView goBack];
			NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
			NSString *encspref0 = (__bridge NSString*)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)spref0, NULL,
				(CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 );
			NSString *indexUrl = [NSString stringWithFormat:@"%@?product_id=%@&select=%@",msIndexUrl,msProductId,encspref0];
			//msIndexUrl + "?" + String.format("product_id=%s&select=%s", msProductId, URLEncoder.encode(email))
			[self loadUrlRequest:YES url:indexUrl postParam:nil];
			if(mWebView != nil) {
				[mWebView setAlpha:0.0];
			}
		 }
		return YES;
	}
	
	//if([request.URL.host isEqualToString:@"ZZZ"]) {
	//	return YES;
	//}
	
	NSString *reqParams = [request.URL.fragment stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	NSData* reqDatas = [reqParams dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
	if(!dict) {
		dp(@"shouldStartLoadWithRequest:error: %@", error);
		return NO;
	}
	
	//dp(@"reqParams: %@", reqParams);
	NSString *funcName = request.URL.host;
	
	//////////////////////////////////////////////////////////////////////
	// log, cancel, load url, ...
	if([funcName isEqualToString:@"log"]) {
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"out"]];
		dp(@"javascriptLog: %@", s);
	} else if([funcName isEqualToString:@"cancel"]) {
		//[self destroy];
		if(tabBarView != nil) {
			tabBarView.hidden = YES;
			tabBarViewShown = NO;
			//[self displayToastWithMessage:@"User Canceled"];
		    mGpOnReturn(GPRET_LOGIN_CANCELED, @"User Canceled");
		}
	} else if([funcName isEqualToString:@"goback"]) {
		if(mWebView != nil) {
			[mWebView goBack];
		}
	} else if([funcName isEqualToString:@"loadUrl"]) {
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"url"]];
		[self loadUrlRequest:NO url:s postParam:nil];

	} else if([funcName isEqualToString:@"loadUrlWithBrowser"]) {
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"url"]];
		[self loadUrlWithBrowser:s];
		// @JavascriptInterface public void loadUrlWithBrowser(String url) {
		// 	dp("IGamespark.loadUrlWithBrowser: " + url);
		// 	new LoadUrlWithBrowserInUiModeTask().execute(new String[]{url});
		// }
	} else if([funcName isEqualToString:@"postQuickStart"]) {//?
		dp(@"js:postQuickStart");
		[self doQuickStart:msQuickStartUrl];
	} else if([funcName isEqualToString:@"postAutoLogin"]) {
		dp(@"js:postAutoLogin");
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"email"]];
		if([s isEqualToString:@"Guest"]) {
			dp(@"do quick login");
			[self doQuickStart:msQuickLoginUrl];
		} else {
			NSString *semail = [self getUserPreference:@"gamesparkEmail"];
			NSString *sidx = [self getIndexInJsonArray:semail stringToCheck:s];
		    NSString *spassword = [self getUserPreference:@"gamesparkPassword"];
			NSString *spwd = [self getStringFromJsonArray:spassword indexString:sidx];
			if([s isEqualToString:@"Facebook_Enabled"]) {
				dp(@"do facebook login using existing facebook account");
				[self doLoginWithFacebook:[self getDecryptedPassword:spwd] bind:NO];
			} else {
				dp(@"do normal login using gamespark account");
				[self doLogin:s encryptedPassword:[self getDecryptedPassword:spwd]];
			}
		}
	} else if([funcName isEqualToString:@"loadUrlCustomHeader"]) {
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"url"]];
		[self loadUrlRequest:YES url:s postParam:nil];
	} else if([funcName isEqualToString:@"logout"]) {
		[self doLogout];
	//////////////////////////////////////////////////////////////////////
	// post login & register
	} else if([funcName isEqualToString:@"postLogin"]) {
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"email"]];
		t = [[NSString alloc] initWithFormat:@"%@",dict[@"password"]];
		u = [self getEncryptedPassword:t];
		[self doLogin:s encryptedPassword:u];
	} else if([funcName isEqualToString:@"postRegister"]) {
		[self doRegistration:dict bind:NO];
	} else if([funcName isEqualToString:@"postSupport"]) {
		s = [[NSString alloc] initWithFormat:@"%@",dict[@"param"]];
		[self doSupport:s];
	//////////////////////////////////////////////////////////////////////
	// facebook stuff
	} else if([funcName isEqualToString:@"goingToFbLogin"]) {
		dp(@"facebook: goingToFbLogin");
		//mbGoingToFbLogin = YES;
	} else if([funcName isEqualToString:@"fbloginConnected"]) {
		dp(@"facebook: fbloginConnected");
		sConnResp = [[NSString alloc] initWithFormat:@"%@",reqParams];
	} else if([funcName isEqualToString:@"fbloginNotConnected"]) {
		dp(@"facebook: fbloginNotConnected");
		return [self doFacebookWork:nil bind:NO];
	} else if([funcName isEqualToString:@"fbloginUserData"]) {
		if(mbLoggedIn) {
			dp(@"facebook: fbBindUserData");
			[self doFacebookWork:reqParams bind:YES];
			NSMutableString* profileUrl = [[NSMutableString alloc] initWithFormat:@"%@", msProfileUrl];
			profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
			profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
			[self loadUrlRequest:YES url:profileUrl postParam:nil];
			return NO;
		} else {		
			dp(@"facebook: fbloginUserData");
			return [self doFacebookWork:reqParams bind:NO];
		}
	} else if([funcName isEqualToString:@"fbBindUserData"]) {
		dp(@"facebook: fbBindUserData");
		return [self doFacebookWork:reqParams bind:YES];
	//////////////////////////////////////////////////////////////////////
	// catatan:
	// selasa 7 juli 2015
	// login facebook jalan karena fbloginConnected, fbloginUserData, dipanggil, normal nya begitu
	// bind facebook gak jalan karena 2 tsb diatas gak dipanggil, code javascript server ada masalah!

	//////////////////////////////////////////////////////////////////////
	// binding email & fb
	} else if([funcName isEqualToString:@"bindEmail"]) {
		[self doRegistration:dict bind:YES];
	} else if([funcName isEqualToString:@"bindFb"]) {
	}
	return NO;
}

-(NSString *)getDeviceId
{
	//String[] spref = loadLoginPref();
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	//String uuid, lidx = getIndexInJsonArray(spref[0], "Guest");
	NSString *uuid, *lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];
	if([lidx isEqualToString:@"-1"]) {
		//uuid = UUID.randomUUID().toString();
		uuid = [[NSUUID UUID] UUIDString];
		spref0 = [self addStringToJsonArray:spref0 stringToSet:@"Guest"];
		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:uuid]];
		spref2 = [self addStringToJsonArray:spref2 stringToSet:@"guestUserIdNotYetSet"];
		lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];
	} else {
		// NSString *spwd = [self getStringFromJsonArray:spref1 indexString:lidx];
		// uuid = [self decodeEncryptedPassword:spwd];
		uuid = [self getDecryptedPassword:[self getStringFromJsonArray:spref1 indexString:lidx]];
	}
	dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	return uuid;
}

-(NSString *)getUniqueDeviceIdentifierAsString
{	
	NSString *bundleId = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
	dp(@"bundleId: %@",bundleId);

	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:bundleId accessGroup:nil];
	NSString *strApplicationUUID = [keychainItem objectForKey:(__bridge id)(kSecValueData)];
	
	if(strApplicationUUID == nil || [strApplicationUUID isEqualToString:@""]) {
		strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
		[keychainItem setObject:strApplicationUUID forKey:(__bridge id)(kSecValueData)];
	}

	return strApplicationUUID;
}

//2015-03-31 11:42:35.666 GamesparkDemo[824:73152] userSubmission: {
//	"deviceSystemVersion" : "8.2",
//	"deviceKeychainIdentifierForVendor" : "677066BD-18EA-4BCF-BD65-46E6D8167FB5",
//	"device_model" : "iPhone",
//	"carrierName" : "XL",
//	"mobileCountryCode" : "510",
//	"deviceLocalizedModel" : "iPhone",
//	"mobileNetworkCode" : "11",
//	"deviceName" : "iJimbas",
//	"deviceSystemName" : "iPhone OS",
//	"deviceIdentifierForVendor" : "677066BD-18EA-4BCF-BD65-46E6D8167FB5"
//}

- (NSString*) getUserSubmission
{
	CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *carrier = [networkInfo subscriberCellularProvider];
	
	NSError *error;
	UIDevice *device = [UIDevice currentDevice];
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
						  [device name],@"deviceName",
						  [device systemName],@"deviceSystemName",
						  [device systemVersion],@"deviceSystemVersion",
						  [device localizedModel],@"deviceLocalizedModel",
						  [self getUniqueDeviceIdentifierAsString],@"deviceKeychainIdentifierForVendor",
						  [device model],@"device_model",
						  [[device identifierForVendor]UUIDString],@"deviceIdentifierForVendor",
						  [carrier carrierName],@"carrierName",
						  [carrier mobileCountryCode],@"mobileCountryCode",
						  [carrier mobileNetworkCode],@"mobileNetworkCode",
						  nil];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
	return [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
}

- (void) doRegistration:(NSDictionary*)dict bind:(BOOL)bind
{
	NSError *error;
	NSMutableString* sUrl = nil;
	NSString *s = [[NSString alloc] initWithFormat:@"%@",dict[@"password"]];
	NSString *t = [[NSString alloc] initWithFormat:@"%@",dict[@"password_confirmation"]];
	NSString *sBindType = [[NSString alloc] initWithFormat:@"Registration"];
	
	if([s isEqualToString:t]) {
		NSString *u = [self getEncryptedPassword:s];
		NSString *smdn = [NSString stringWithFormat:@"%@",dict[@"mdn"]];
		if([smdn isEqualToString:@"1100000011"]) {
			sBindType = [[NSString alloc] initWithFormat:@"Login"];
		}
		msDict = [[NSDictionary alloc] initWithObjectsAndKeys:
			msProductId,@"product_id",
			dict[@"email"],@"email",
			smdn,@"mdn",
			u,@"password",
			u,@"password_confirmation",
			msChannelId,@"channel_id",
			[self getUserSubmission],@"extra",
		nil];
		NSData *jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:&error];
		NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
		dp(@"doRegistration:jsonstr: %@", jsonstr);

		if(bind) {
			sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msBindEmail];
			sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
		} else {
			sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msRegisterUrl];
		}
		//[self connectServer:msRegisterUrl postParam:jsonstr
		[self connectServer:sUrl postParam:jsonstr
		  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
		  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
			  //[mConnectionTimer invalidate];
			  NSString *respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
			  dp(@"doRegistration:respText: %@",respText);
			  if(error == nil) {
				  /////////////////
				  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
				  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
				  if(!dictb) {
					  dp(@"doRegistration:error: %@", error);
					  respText = [NSString stringWithFormat:@"%@",error];
					  if(respText == nil || [respText isEqualToString:@"(null)"]) {
						respText = [NSString stringWithFormat:@"%@ has failed", sBindType];
					  }
					  [self doAlert:[NSString stringWithFormat:@"Gamespark %@ Failed", sBindType] message:respText alertid:ALID_NONE];
					  mGpOnReturn(bind ? GPRET_BINDING_ERROR : GPRET_REGISTRATION_ERROR, respText);
					  return;
				  }
				  NSNumber *stat = [dictb valueForKey:@"success"];
				  if([stat isEqual: @YES]) {
					  if(bind) {
						  [self replaceGuestForBinding:NO];
						  //[self replaceGuestForBinding:@"Facebook_Enabled"];
						  //NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('bindingSuccess', '')"];
						  //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
						  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__binding_success" repeats:NO];
						  mGpOnReturn(GPRET_BINDING_SUCCESS, respText);
						  [tabBarView setHidden:YES];
						  tabBarViewShown = NO;
						  return;
					  }
					  msUserId = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					  NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
					  NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
					  NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
					  NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
					  spref0 = [self addStringToJsonArray:spref0 stringToSet:msDict[@"email"]];
					  spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:msDict[@"password"]]];
					  spref2 = [self addStringToJsonArray:spref2 stringToSet:[self getEncryptedPassword:msUserId]];
					  NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:msDict[@"email"]];
					  spref3 = lidx;
					  dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
					  [self setUserPreference:spref0 forKey:@"gamesparkEmail"];
					  [self setUserPreference:spref1 forKey:@"gamesparkPassword"];
					  [self setUserPreference:spref2 forKey:@"gamesparkUserId"];
					  [self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
					  mbLoggedIn = YES;
					  [tabBarView setHidden:YES];
					  tabBarViewShown = NO;
					  [myButton setHidden:NO];
					  [myButton moveButton:YES];
					  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__registration_success" repeats:NO];
					  mGpOnReturn(GPRET_REGISTRATION_SUCCESS, respText);
				  } else {
					  NSString *msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"message"]];
					  if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
						if(msg == nil || [msg isEqualToString:@"(null)"]) {
							msg = [NSString stringWithFormat:@"%@ has failed", sBindType];
						}
					  }
					  dp(@"registration failed: %@", msg);
					  [self doAlert:[NSString stringWithFormat:@"Gamespark %@ Failed", sBindType] message:msg alertid:ALID_NONE];
					  if(bind) {
						mGpOnReturn(GPRET_BINDING_ERROR, respText);
						return;
					  }
					  mGpOnReturn(GPRET_REGISTRATION_ERROR, respText);
				  }
			  } else {
				  [self connectError:error];
			  }
		  }
		 ];
	} else {
		//NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('registrationFailed', 'password mismatch')"];
		//[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
		[self displayToastWithMessage:@"Password Mismatch"];
	}
}

- (void) doSupport:(NSString*)sparam
{
	//NSError *error;

	dp(@"doSupport:sparam: %@", sparam);
	[self connectServer:msSupportUrl postParam:sparam
	  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		  //[mConnectionTimer invalidate];
		  NSString *respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		  dp(@"doSupport:respText: %@",respText);
		  if(error == nil) {
			  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
			  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
			  if(!dictb) {
				  dp(@"doSupport:completion:error: %@", error);
				  respText = [NSString stringWithFormat:@"%@",error];
				  [self doAlert:@"Support Message Failed" message:respText alertid:ALID_NONE];
				  mGpOnReturn(GPRET_SUPPORTMSG_ERROR, respText);
				  return;
			  }
			  NSNumber *stat = [dictb valueForKey:@"success"];
			  //mGpOnReturn([stat isEqual: @YES] ? GPRET_SUPPORTMSG_SUCCESS : GPRET_SUPPORTMSG_ERROR, respText);
			  if([stat isEqual: @YES]) {
				  // NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('supportSuccess', '')"];
				  // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
				  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__support_success" repeats:NO];
				  mGpOnReturn(GPRET_SUPPORTMSG_SUCCESS, respText);
				  if(mWebView != nil) {
				  	[mWebView goBack];
				  }
			  } else {
				  NSString *msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"message"]];
				  if(msg == nil || [msg isEqualToString:@"(null)"]) {
					msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"Message failed to send"];
					}
				  }
				  [self doAlert:@"Support Message Failed" message:msg alertid:ALID_NONE];
				  // NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('supportFailed', '%@')",respText];
				  // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
				  mGpOnReturn(GPRET_SUPPORTMSG_ERROR, respText);
			  }
		  } else {
			  [self connectError:error];
		  }
	  }
	 ];
}

- (BOOL) doFacebookWork:(NSString*)reqParams bind:(BOOL)bind
{
	NSError* error = nil;
	NSString *s=nil,*t=nil,*u=nil;
	NSData *reqDatas=nil;
	NSDictionary *dictb = nil;
	
	if(sConnResp == nil) {
		dp(@"doFacebookWork: sConnResp nil");
		return NO;
	} else {
		dp(@"doFacebookWork: sConnResp available!");
	}
	
	////// extract value dari key:json
	reqDatas = [sConnResp dataUsingEncoding:NSUTF8StringEncoding];
	dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
	if(!dictb) {
		dp(@"doFacebookWork:error: %@", error);
		return NO;
	}
	s = [[NSString alloc] initWithFormat:@"%@",dictb[@"json"]];
	////// extract value dari key:authResponse dan key:status
	reqDatas = [s dataUsingEncoding:NSUTF8StringEncoding];
	dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
	if(!dictb) {
		dp(@"doFacebookWork:errorb: %@", error);
		return NO;
	}
	t = [[NSString alloc] initWithFormat:@"%@",dictb[@"authResponse"]];
	u = [[NSString alloc] initWithFormat:@"%@",dictb[@"status"]];
	////// extract value dari key:json dari reqParams
	if(reqParams != nil) {
		reqDatas = [reqParams dataUsingEncoding:NSUTF8StringEncoding];
		dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
		if(!dictb) {
			dp(@"doFacebookWork:errorc: %@", error);
			return NO;
		}
		s = [[NSString alloc] initWithFormat:@"%@",dictb[@"json"]];
	} else {
		s = nil;
	}
	////// contruct lagi json-nya. userData, authResponse, status
	dictb = @{
		//@"userData":[[NSString alloc] initWithFormat:@"%@",s],
		@"product_id":msProductId,
		@"userData":reqParams != nil ? [[NSString alloc] initWithFormat:@"%@",s] : [NSNumber numberWithInt:0],
		@"authResponse":[[NSString alloc] initWithFormat:@"%@",t],
		@"status":[[NSString alloc] initWithFormat:@"%@",u],
		@"channel_id":[[NSString alloc] initWithFormat:@"%@", msChannelId]
	};
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dictb options:NSJSONWritingPrettyPrinted error:&error];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	msDict = [[NSDictionary alloc] initWithDictionary:dictb];
	[self doLoginWithFacebook:jsonstr bind:bind];
	[self clearFacebookCookies];
	sConnResp = nil;
	return NO;
}

- (void) doLoginWithFacebook:(NSString*)jsonstr bind:(BOOL)bind
{
	NSMutableString* sUrl = nil;

	////////////////////////////////////////////////////////////////////////////////////
	// binding with facebook account
	if(bind) {
		sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msBindFb];
		sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
		dp(@"doLoginWithFacebook:bind:jsonstr:%@,sUrl:%@", jsonstr, sUrl);
		// kirim ke server
		[self connectServer:sUrl postParam:jsonstr
		  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
		  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
			  //[mConnectionTimer invalidate];
			  if(mbJimbasOrg) {
				  dp(@"bind success test - jimbas.org");
				  NSString *sresult = [NSString stringWithFormat:@"%@",@"{\"success\":true,\"user_id\":\"54db4205891e40032d00000a\"}"];
				  data = [sresult dataUsingEncoding:NSUTF8StringEncoding];
				  error = nil;
			  }		  		  
			  NSString *respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
			  //if(!mbJimbasOrg) {
			  dp(@"doLoginWithFacebook:respText: = %@",respText);
			  //}
			  if(error == nil) {
				  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
				  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
				  if(!dictb) {
					  dp(@"doLoginWithFacebook:errore: %@", error);
					  respText = [NSString stringWithFormat:@"%@",error];
					  //[self doAlert:@"Gamespark Binding Failed" message:respText alertid:ALID_NONE];
					  if(respText == nil || [respText isEqualToString:@"(null)"]) {
						respText = [NSString stringWithFormat:@"Facebook login has failed"];
					  }
					  [self doAlert:@"Gamespark Binding Failed" message:respText alertid:ALID_NONE];
					  mGpOnReturn(GPRET_BINDING_ERROR, respText);
					  return;
				  }
				  NSNumber *stat = [dictb valueForKey:@"success"];
				  if([stat isEqual: @YES] && error == nil) {
					  //[self replaceGuestForBinding:@"Facebook_Enabled"];
					  [self replaceGuestForBinding:YES];					  
					  //msUserId = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					  //[self setUserPreference:msUserId forKey:@"gamesparkUserId"];
					  // NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('bindingSuccess', '')"];
					  // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
					  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__binding_success" repeats:NO];
					  mGpOnReturn(GPRET_BINDING_SUCCESS, respText);
					  [tabBarView setHidden:YES];
					  tabBarViewShown = NO;
				  } else {
					  // NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('bindingFailed', '')"];
					  // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
					  NSString *msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"message"]];
					  if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
						if(msg == nil || [msg isEqualToString:@"(null)"]) {
							msg = [NSString stringWithFormat:@"Account Binding has failed"];
						}
					  }
					  [self doAlert:@"Gamespark Binding Failed" message:msg alertid:ALID_NONE];
					  mGpOnReturn(GPRET_BINDING_ERROR, respText);
				  }
			  } else {
				  [self connectError:error];
			  }
		  }
		 ];
		return;
	}

	////////////////////////////////////////////////////////////////////////////////////
	// normal facebook login

	[self setUserPreference:jsonstr forKey:@"gamesparkFbStuff"];
	sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msFbUrl];
	sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
	dp(@"doLoginWithFacebook:login:jsonstr: %@ sUrl: %@", jsonstr, sUrl);
	////// kirim ke server
	[self connectServer:sUrl postParam:jsonstr
	  //completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		  //[mConnectionTimer invalidate];
		  if(mbJimbasOrg) {
			  dp(@"fblogin success test - jimbas.org");
			  NSString *sresult = [NSString stringWithFormat:@"%@",@"{\"success\":true,\"user_id\":\"54db4205891e40032d00000a\"}"];
			  data = [sresult dataUsingEncoding:NSUTF8StringEncoding];
			  error = nil;
		  }		  		  
		  NSString *respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		  //if(!mbJimbasOrg) {
		  dp(@"doLoginWithFacebook:respText: = %@",respText);
		  //}
		  if(error == nil) {
			  NSData *reqDatas = [respText dataUsingEncoding:NSUTF8StringEncoding];
			  NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:reqDatas options:NSJSONReadingMutableContainers error:&error];
			  if(!dictb) {
				  dp(@"doLoginWithFacebook:errore: %@", error);
				  respText = [NSString stringWithFormat:@"%@",error];
				  [self doAlert:@"Facebook Login Failed" message:respText alertid:ALID_NONE];
				  mGpOnReturn(GPRET_FBLOGIN_ERROR, respText);
				  return;
			  }
			  NSNumber *stat = [dictb valueForKey:@"success"];
			  if([stat isEqual: @YES] && error == nil) {
				  msUserId = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
				  //[self setUserPreference:msUserId forKey:@"gamesparkUserId"];
				  // untuk facebook gak simpan apa2 di pref
				  [tabBarView setHidden:YES];
				  tabBarViewShown = NO;
				  [myButton setHidden:NO];
				  [myButton moveButton:YES];
				  mbLoggedIn = YES;
				  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__fblogin_success" repeats:NO];
				  mGpOnReturn(GPRET_FBLOGIN_SUCCESS, respText);
			  } else {
				  // NSString *jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('fbLoginFailed', '')"];
				  // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:jsCallBack repeats:NO];
				  [self doAlert:@"Facebook Login Failed" message:respText alertid:ALID_NONE];
				  mGpOnReturn(GPRET_FBLOGIN_ERROR, respText);
				  [self setUserPreference:[NSString stringWithFormat:@""] forKey:@"gamesparkFbStuff"];
			  }
		  } else {
			  [self connectError:error];
		  }
	  }
	 ];
}

//- (void) replaceGuestForBinding:(NSString*) account
- (void) replaceGuestForBinding:(BOOL) bfacebook
{
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];

	NSString *sjsondata = nil;
	NSData *jsondata = nil;

	if([lidx rangeOfString:@"-1"].location == NSNotFound) {
		if(bfacebook) {
			dp(@"lidx -1 NSNotFound");
			spref0 = [self setStringInJsonArray:spref0 indexString:lidx stringToSet:@"Facebook_Enabled"];
			jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
			sjsondata = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
		} else {
			spref0 = [self setStringInJsonArray:spref0 indexString:lidx stringToSet:msDict[@"email"]];
			sjsondata = [[NSString alloc] initWithFormat:@"%@",msDict[@"password"]];
		}
		spref1 = [self setStringInJsonArray:spref1 indexString:lidx stringToSet:[self getEncryptedPassword:sjsondata]];
	} else {
		if(bfacebook) {
			dp(@"lidx > 0 Found");
			spref0 = [self addStringToJsonArray:spref0 stringToSet:@"Facebook_Enabled"];
			jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
			sjsondata = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
		} else {
			spref0 = [self addStringToJsonArray:spref0 stringToSet:msDict[@"email"]];
			sjsondata = [[NSString alloc] initWithFormat:@"%@", msDict[@"password"]];
		}
		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:sjsondata]];
	}

	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
}

- (void) clearFacebookCookies
{
	NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	for(NSHTTPCookie *cookie in [storage cookies]) {
		//dp(@"cookie before: %@", cookie);
		if([[cookie domain] isEqualToString:@".facebook.com"]) {
			if(  [[cookie name] isEqualToString:@"datr"]
			  || [[cookie name] isEqualToString:@"s"]
			  || [[cookie name] isEqualToString:@"csm"]
			  || [[cookie name] isEqualToString:@"fr"]
			  || [[cookie name] isEqualToString:@"lu"]
			  || [[cookie name] isEqualToString:@"c_user"]
			  || [[cookie name] isEqualToString:@"xs"] ) {
			  	//dp(@"FOUND!");
		        NSMutableDictionary *propscook = [[NSMutableDictionary alloc] initWithDictionary: [cookie properties]];
		        [propscook setObject:@"" forKey:NSHTTPCookieValue];
		        NSHTTPCookie *newcookie = [NSHTTPCookie cookieWithProperties:propscook];
		        [storage setCookie:newcookie];
			}
		}
	}
	// for(NSHTTPCookie *cookieafter in [storage cookies]) {
	// 	if([[cookieafter domain] isEqualToString:@".facebook.com"]) {
	// 		dp(@"cookie after: %@", cookieafter);
	// 	}
	// }
	[[NSUserDefaults standardUserDefaults] synchronize];
	dp(@"Fb cookies clear");
}

- (void) clearPreferences
{
	[self setUserPreference:@"-1" forKey:@"gamesparkLoggedIn"];
	// NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
	// NSDictionary *dict = [defs dictionaryRepresentation];
	// for(id key in dict) {
	// 	[defs removeObjectForKey:key];
	// }
	// [defs synchronize];
	dp(@"preferences cleared!");
}

// -(void) tmevIGamesparkCallback:(NSTimer*)tmr
// {
// 	NSString *jsCallBack;
// 	NSString *tag = tmr.userInfo;
// 	dp(@"tmevIGamesparkCallback:tag: %@", tag);
// 	if([tag isEqualToString:@"__logout_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('logoutSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self destroy];
// 	} else if([tag isEqualToString:@"__login_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('loginSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page
// 	} else if([tag isEqualToString:@"__fblogin_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('fbLoginSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page
// 	} else if([tag isEqualToString:@"__registration_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('registrationSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page
// 	} else {
// 		[mWebView stringByEvaluatingJavaScriptFromString:tag];
// 	}
// }

-(void) tmevIGamesparkCallback:(NSTimer*)tmr
{
	NSString *tag = tmr.userInfo;
	dp(@"tmevIGamesparkCallback:tag: %@", tag);
	if([tag isEqualToString:@"__logout_success"]) {
		[self displayToastWithMessage:@"Logout Success"];
		[self destroy];

	} else if([tag isEqualToString:@"__login_success"]) {
		[self displayToastWithMessage:@"Login Success"];
		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page

	} else if([tag isEqualToString:@"__fblogin_success"]) {
		[self displayToastWithMessage:@"Facebook Login Success"];
		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page

	} else if([tag isEqualToString:@"__registration_success"]) {
		[self displayToastWithMessage:@"Registration Success"];
		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page

	} else if([tag isEqualToString:@"__support_success"]) {
		[self displayToastWithMessage:@"Message Sent"];

	} else if([tag isEqualToString:@"__binding_success"]) {
		[self displayToastWithMessage:@"Binding Success"];

	} else {
		dp(@"call js: %@", tag);
	}
}

- (void) fitContentForWebviewResize {
	[mWebView stringByEvaluatingJavaScriptFromString:
		[NSString stringWithFormat:
		 @"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ",
		 (int)mWebView.bounds.size.width]];
}

-(void)roundButtonDidTap:(UIButton*)tappedButton
{
	dp(@"roundButtonDidTap Method Called");
	[tappedButton removeFromSuperview];
	[self destroy];
}

-(void)displayToastWithMessageWithSize:(NSString *)toastMessage width:(NSInteger)width height:(NSInteger)height
{
}

-(void)displayToastWithMessage:(NSString *)toastMessage
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        float fontHeight = 12.0;
		float fa = self.uiViewCtrl.view.bounds.size.width;
		float fb = self.uiViewCtrl.view.bounds.size.height;

        UILabel *toastView = [[UILabel alloc] init];
        toastView.text = toastMessage;
        toastView.font = [UIFont fontWithName:@"System" size:fontHeight];
        toastView.textColor = [UIColor whiteColor];

        toastView.backgroundColor = [UIColor blackColor];
		toastView.layer.borderColor = [UIColor lightGrayColor].CGColor;
		toastView.layer.borderWidth = 2.0;
		toastView.clipsToBounds = YES;

		toastView.lineBreakMode = NSLineBreakByWordWrapping;
		toastView.numberOfLines = 0;

		float fwlim = fa - 40.0;
		CGSize s = [toastMessage sizeWithFont:[UIFont systemFontOfSize:fontHeight] constrainedToSize:CGSizeMake(fwlim, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
		float fh = s.height * 2;
        float fw = s.width * 2;
        if(fw > fwlim) fw = fwlim;

        toastView.textAlignment = NSTextAlignmentCenter;
        toastView.frame = CGRectMake(0.0, 0.0, fw, fh);
        toastView.layer.cornerRadius = 8;
        toastView.layer.masksToBounds = YES;
		toastView.center = CGPointMake(fa/2, 3*fb/4);

		// float widthIs =  [toastView.text
		// 					boundingRectWithSize:toastView.frame.size                                           
		// 					options:NSStringDrawingUsesLineFragmentOrigin
		// 					attributes:@{ NSFontAttributeName:toastView.font }
		// 					context:nil].size.width;
		// toastView.frame = CGRectMake(0.0, 0.0, fa/2.0, 42.0);
		//NSLog(@"the width of yourLabel is %f", widthIs);

        [self.uiViewCtrl.view addSubview:toastView];
        [UIView animateWithDuration:4.0f delay:0.0 options: UIViewAnimationOptionCurveEaseOut
			animations: ^{
				toastView.alpha = 0.0;
			} completion: ^(BOOL finished) {
				[toastView removeFromSuperview];
			}
         ];
    }];
}

- (void)floatIconClicked:(FloatingIcon*)btn
{
	if(!btn.MoveEnabled) {
		if(!tabBarViewShown){
			if(mbLoggedIn == YES && msUserId != nil) {
				[self loadProgress];
				NSMutableString* profileUrl = [[NSMutableString alloc] initWithFormat:@"%@", msProfileUrl];
				//dp(@"profileUrl2: %@ msUserId: %@", profileUrl, msUserId);
				profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
				profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
				//dp(@"profileUrl3: %@", profileUrl);
				[self loadUrlRequest:YES url:profileUrl postParam:nil];
			} else {
			}
			tabBarView.hidden = NO;
			tabBarViewShown = YES;
		} else {
			tabBarView.hidden = YES;
			tabBarViewShown = NO;
		}
	}
}

//- (void) connectServerTimeout:(NSTimer*)tmr
//{
//	if(tmr == nil) return;
//	[tmr invalidate];
//	tmr = nil;
//	//if(mConnectionTimer == nil) return;
//	NSURLSessionDataTask* dataTask = tmr.userInfo;
//	if(dataTask != nil) {
//		dp(@"checkjar timeout, cancelling connection");
//		[dataTask cancel];
//		[self closeProgress];
//		mGpOnReturn(GPRET_CONNECT_ERROR, @"Error connection time-out");
//		[self doAlert:@"Gamespark Connection" message:@"Error connection time-out. Press OK to retry." alertid:ALERTID_CHECKJAR];
//	}
//	//mConnectionTimer = nil;
//}

- (void) connectServer:(NSString*)urlToPost postParam:(NSString*)postParam
  //completionHandler:(void (^)(NSData* data, NSURLResponse* response, NSError* error))completionHandler
  completionHandler:(void (^)(NSURLResponse* response, NSData* data, NSError* error))completionHandler
{
	//NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
	//NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
	NSURL * url = [NSURL URLWithString:urlToPost];
	NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
	
	//urlRequest.timeoutInterval = 241;
	urlRequest.timeoutInterval = 60;
	[urlRequest setValue:@"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401" forHTTPHeaderField:@"User-Agent"];
	[urlRequest setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[urlRequest setValue:@"en-US" forHTTPHeaderField:@"Accept-Language"];
	[urlRequest setValue:@"UTF-8" forHTTPHeaderField:@"Accept-Charset"];
	[urlRequest setValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];
	NSString *scursystm = [self getUnixTimeStamp];
	[urlRequest setValue:scursystm forHTTPHeaderField:@"timestamp"];
	NSMutableString* str = [[NSMutableString alloc] init];
	[str appendString:msSecretKey];
	[str appendString:scursystm];
	[urlRequest setValue:[self getMd5:str] forHTTPHeaderField:@"token"];
	if(postParam != nil && postParam.length) {
		[urlRequest setHTTPMethod:@"POST"];
		NSString *urlParameters = postParam;
		[urlRequest setHTTPBody:[urlParameters dataUsingEncoding:NSUTF8StringEncoding]];
	}
	//NSURLRequest *urlreq = [urlRequest mutableCopy];
	//dp(@"urlreq: %@", [urlreq allHTTPHeaderFields]);
	//NSURLSessionDataTask * dataTask = [delegateFreeSession dataTaskWithRequest:urlRequest completionHandler:completionHandler];

	dp(@"urlreq: %@", [[urlRequest mutableCopy] allHTTPHeaderFields]);
	[NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue mainQueue] completionHandler:completionHandler];
	//mConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:12 target:self selector:@selector(connectServerTimeout:) userInfo:dataTask repeats:NO];
	//[dataTask resume];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch(buttonIndex) {	// cancel button
	case 0:
		switch (nAlertId) {
		case ALERTID_CHECKJAR:
			dp(@"checkjar retry");
			[self getCheckJar];
			break;
		}
		break;
	case 1:
		break;
	}
	[alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

- (NSData*) base64EncodingInit:(NSString*)str
{
	NSData *data = nil;
	if([NSData instancesRespondToSelector:@selector(initWithBase64EncodedString:options:)]) {
		data = [[NSData alloc] initWithBase64EncodedString:str options:0];	// iOS 7+
	} else {
		data = [[NSData alloc] initWithBase64Encoding:str];	// pre iOS7
	}
	return data;
}

- (NSString*) base64Encoding:(NSData*)data
{
	NSString *str = nil;
	if([data respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
		str = [[NSString alloc] initWithFormat:@"%@", [data base64EncodedStringWithOptions:0]];	// iOS 7+
	} else {
		str = [[NSString alloc] initWithFormat:@"%@", [data base64Encoding]];	// pre iOS7
	}
	return str;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// encryption decryption
///////////////////////////////////////////////////////////////////////////////////////////////

- (NSString*) getMd5:(NSString*) input
{
	const char *cStr = [input UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, (CC_LONG) strlen(cStr), result ); // This is the md5 call
	return [NSString stringWithFormat:
			@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

- (NSString*) getUnixTimeStamp
{
	NSDate* past = [NSDate date];
	NSTimeInterval oldTime = [past timeIntervalSince1970];
	return [NSString stringWithFormat:@"%0.0f", oldTime];
}

- (NSString*) getEncryptedPassword:(NSString*) realpaswd
{
	NSMutableString* sres = [[NSMutableString alloc] init];
	NSString *skey = [self getMd5:msSecretKey];
	NSData* iv = [self generateIv];
	NSString *sciphertext = [self cbcEncrypt:skey sdata:realpaswd ivps:iv];
	//NSString *siv = [self base64encode:[[NSString alloc] initWithData:iv encoding:NSASCIIStringEncoding]];
	//NSString *siv = [iv base64EncodedStringWithOptions:0];
	NSString *siv = [self base64Encoding:iv];
	[sres appendFormat:@"%@",siv];
	[sres appendFormat:@"%@",sciphertext];
	//dp(@"realpaswd: %@ siv: %@ sciphertext: %@", realpaswd, siv, sciphertext);
	return [NSString stringWithString:sres];
}

- (NSString*) getDecryptedPassword:(NSString*) encivpaswd
{
	NSString *siv = [[NSString alloc] initWithFormat:@"%@",[encivpaswd substringToIndex:24]];
	NSString *encpaswd = [[NSString alloc] initWithFormat:@"%@",[encivpaswd substringFromIndex:24]];
	//NSData* iv = [[NSData alloc] initWithBase64EncodedString:siv options:0];
	NSData* iv = [self base64EncodingInit:siv];
	NSString *skey = [self getMd5:msSecretKey];
	NSString *srealpaswd = [self cbcDecrypt:skey sdata:encpaswd ivps:iv];
	//dp(@"srealpaswd: %@ siv: %@ encpaswd: %@", srealpaswd, siv, encpaswd);
	return srealpaswd;
}

- (NSData*) secureRandom:(NSInteger)len
{
	uint8_t randomBytes[len];
	int result = SecRandomCopyBytes(kSecRandomDefault, len, (uint8_t*)&randomBytes);
	if(result == 0) {
		//return [[NSData dataWithBytes:randomBytes length:len] base64EncodedStringWithOptions:0];
		return [NSData dataWithBytes:randomBytes length:len];
	}
	return nil;
}

- (NSData*) generateIv
{
	//uint8_t biv[16] = { 1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6 };
	//return [NSData dataWithBytes:biv length:16];
	return [self secureRandom:16];
}

- (NSString*) cbcEncrypt:(NSString*)key sdata:(NSString*)sdata ivps:(NSData*)ivps
{
	//dp(@"key: %@ sdata: %@ ivps: %@", key, sdata, [ivps base64EncodedStringWithOptions:0]);
	NSData* data = [sdata dataUsingEncoding:NSUTF8StringEncoding];
	NSData* dkey = [key dataUsingEncoding:NSUTF8StringEncoding];
	NSMutableData *cipherData = [NSMutableData dataWithLength:data.length + kCCBlockSizeAES128];
	size_t numBytesEncrypted = 0;
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
		[dkey bytes], [dkey length],
		[ivps bytes],
		[data bytes], [data length],
		[cipherData mutableBytes], [cipherData length],
		&numBytesEncrypted
	);
 
	NSString *returnString = nil;
	if(cryptStatus == kCCSuccess) {
		cipherData.length = numBytesEncrypted;
		//returnString = [cipherData base64EncodedStringWithOptions:0];
		returnString = [self base64Encoding:cipherData];
	}
	
	return returnString;
}

- (NSString*) cbcDecrypt:(NSString*)key sdata:(NSString*)sdata ivps:(NSData*)ivps
{
	//dp(@"key: %@ sdata: %@ ivps: %@", key, sdata, [ivps base64EncodedStringWithOptions:0]);
	// NSData* data = [[NSData alloc] initWithBase64EncodedString:sdata options:0];
	NSData* data = [self base64EncodingInit:sdata];
	NSData* dkey = [key dataUsingEncoding:NSUTF8StringEncoding];
	
	NSMutableData *cipherData = [NSMutableData dataWithLength:data.length + kCCBlockSizeAES128];
	size_t numBytesEncrypted = 0;
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
		[dkey bytes], [dkey length],
		[ivps bytes],
		[data bytes], [data length],
		[cipherData mutableBytes], [cipherData length],
		&numBytesEncrypted
	);

	NSString *returnString = nil;
	if(cryptStatus == kCCSuccess) {
		cipherData.length = numBytesEncrypted;
		returnString = [[NSString alloc] initWithData:cipherData encoding:NSASCIIStringEncoding];
	}
	
	return returnString;
}


@end
