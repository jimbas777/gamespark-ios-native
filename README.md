# README #

iOS version of Gamespark

CHANGES NOTE:
1.0
	- Initial release
1.1,1.2,1.3
	- Skipped to standardize all versioning number for SDK & the PDF documentation
1.4
	- page support is available now
	- bug minor fixed, to close logout busy indicator
	- add getSdkVersion function
	- add enableGateway function
1.4.1
	- bug fix when register new user.
1.4.2
	- remove classes that refer to iOS7 and later. This will allow running on iOS 6 down to 5
	- bug fix rotate handling in iOS7 and below
1.4.3
	- fix back button on register
1.4.4
	- implement quick start
	- implement multiple user account
1.4.5
	- fixed several problems in quick start & its multiple user.
1.4.6
 	- bug fixed, return message (respText) null onGamesparkReturn, null'ed by ARC
 	- bug fixed, when logout
1.4.6.1
 	- bug on V146 still exist. duplicate respText to other var for onGamesparkReturn
1.4.7
	- now we activate custom header, standardize iOS & Android in how to access index page
2.0
	- this is the native version of gamespark iOS SDK
	- all are similar
