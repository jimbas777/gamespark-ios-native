#!/bin/bash

###############################################
if [[ ${1:0:1} == "-" ]]; then
###############################################

	## build
	if [[ "$1" == *b* ]]; then
		"$0" build
		if [ $? -gt 0 ]; then
			echo "ERROR! when try to build"
			exit 1
		fi
	fi

###############################################
elif [ "$1" = "getlaravelerror" ]; then
###############################################

rm laravelerror.html && adb pull /storage/sdcard0/gamespark/error.log laravelerror.html && open -a Safari laravelerror.html

###############################################
elif [ "$1" = "webview" ]; then
###############################################

rm Gamespark && ln -s webview Gamespark

###############################################
elif [ "$1" = "native" ]; then
###############################################

rm Gamespark && ln -s native Gamespark

###############################################
elif [ "$1" = "build" ]; then
###############################################

	export XCODEPROJECT=Gamespark

	function makeLibAll
	{
		BUILDDIR=build
		BUILDROOT=${BUILDDIR}
		OUTDIR=${BUILDDIR}/"$1"-all
		TMPDIR=${BUILDDIR}/tmp

		mkdir -p "${TMPDIR}"
		if [ -f "${TMPDIR}/lib${XCODEPROJECT}.a" ]; then
		    rm "${TMPDIR}/lib${XCODEPROJECT}.a"
		fi
		mkdir -p "${OUTDIR}"
		if [ -f "${OUTDIR}/lib${XCODEPROJECT}.a" ]; then
		    rm -r "${OUTDIR}/lib${XCODEPROJECT}.a"
		fi
		echo "=======> Building devices..."
		xcodebuild -target  ${XCODEPROJECT} ONLY_ACTIVE_ARCH=NO -configuration "$1" -sdk iphoneos  BUILD_DIR="${BUILDDIR}" BUILD_ROOT="${BUILDROOT}"
		echo "=======> Building i386..."
		xcodebuild -target ${XCODEPROJECT} -configuration "$1" -sdk iphonesimulator -arch i386 BUILD_DIR="${BUILDDIR}" BUILD_ROOT="${BUILDROOT}"
		cp "${BUILDDIR}/"$1"-iphonesimulator/lib${XCODEPROJECT}.a" "${TMPDIR}"
		echo "=======> Building x86_64..."
		xcodebuild -target ${XCODEPROJECT} -configuration "$1" -sdk iphonesimulator -arch x86_64 BUILD_DIR="${BUILDDIR}" BUILD_ROOT="${BUILDROOT}"
		echo "=======> Building all..."
		lipo -create -output "${OUTDIR}/lib${XCODEPROJECT}.a" "${BUILDDIR}/"$1"-iphoneos/lib${XCODEPROJECT}.a" "${TMPDIR}/lib${XCODEPROJECT}.a" "${BUILDDIR}/"$1"-iphonesimulator/lib${XCODEPROJECT}.a"
		echo "=======> Copying headers..."
		cp -R "${BUILDDIR}/"$1"-iphoneos/include" "${OUTDIR}/"
		echo "=======> Done"
	}

	if [ -d build ]; then
		rm -r build
		echo "Remove previous build"
	fi

	echo "Building Debug..."
	xcodebuild -project ${XCODEPROJECT}.xcodeproj -target ${XCODEPROJECT} -configuration Debug
	makeLibAll Debug

	echo "Building Release..."
	xcodebuild -project ${XCODEPROJECT}.xcodeproj -target ${XCODEPROJECT} -configuration Release
	makeLibAll Release

###############################################
elif [ "$1" = "rmuserdat" ]; then
###############################################

	adb shell "rm /storage/sdcard0/gamespark/user.dat"

else
	echo "use param please"
fi

