//
//  Gamespark.m
//  Gamespark
//
//  Created by Jimmy Basselo on 3/5/15.
//  Copyright (c) 2015 mainpark. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h> // Need to import for CC_MD5 access
#import <CommonCrypto/CommonCryptor.h>

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

//#import <QuartzCore/QuartzCore.h>
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>

#import "Gamespark.h"
#import "FloatingIcon.h"
#import "KeychainItemWrapper.h"

enum {
	GPBTN_QUICKSTART=1,
	GPBTN_LOGIN,
	GPBTN_FACEBOOK,
	GPBTN_REGISTER,
	GPBTN_BACK,
	GPBTN_BINDEMAIL,
	GPBTN_BINDFACEBOOK,
	GPBTN_MORE,
	GPBTN_FBFANPAGE,
	GPBTN_SUPPORT,
	GPBTN_LOGOUT,
	GPBTN_REG_REGISTER,
	GPBTN_REG_BACK,
	GPBTN_SUPP_SUBMIT,
	GPBTN_SUPP_BACK,
	GPBTN_NONE
};

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// ini dari header, utk sementara jgn dulu
//- (void) enableTouchToClose:(BOOL)touchToClose;

@interface GPUIView : UIView {
	id<GPUIViewDelegate> _delegate;
}
@property (nonatomic,strong) id delegate;
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event;
@end

@implementation GPUIView
@synthesize delegate;
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if( point.x > 0 && point.x < self.frame.size.width && point.y > 0 && point.y < self.frame.size.height ) {
        [delegate uiViewTouched:YES];
        return YES;
    }
    [delegate uiViewTouched:NO];
    return NO;
}
@end

enum {
	ALERTID_NONE,
	ALERTID_CHECKJAR,
	ALERTID_LOGOUT
};

enum {
	PAGE_LODING=1,
	PAGE_LOGIN,
	PAGE_REGISTER,
	PAGE_OTHERACC,
	PAGE_FACEBOOK,
	PAGE_PROFILE,
	PAGE_SUPPORT
};

enum {
	COMBO_ACCOUNTS=1,
	COMBO_SUPPCATEGS
};

BOOL gbProduction = YES;
NSMutableArray *mAls = nil;

@interface Gamespark () {
	BOOL tabBarViewShown;
	GPUIView *tabBarView;
	FloatingIcon *myButton;
	//UIWebView *mWebView;
	UIScrollView *mScrollView;
	//UIButton *btnFbBack;
	//UIActivityIndicatorView* mActivityIndicView;
	NSString *msSecretKey;
	GamesparkOnReturn mGpOnReturn;
	NSString *msProductId;
	NSString *mProductName;
	NSString *msChannelId;
	//BOOL mbJimbasOrg;
	NSString *msCheckJar;
	NSString *msUserId;
	NSString *msIndexUrl;
	NSString *msLoginUrl;
	NSString *msRegisterUrl;
	NSString *msFbUrl;
	NSString *msProfileUrl;
	NSString *msLogoutUrl;
	NSString *msBindEmail;
	NSString *msBindFb;
	NSString *msSupportUrl;
	NSString *msQuickStartUrl;
	NSString *msQuickLoginUrl;
	NSString *msFbFansPageUrl;
	BOOL mbLoggedIn;
	//NSString *msPage;
	NSUInteger nTag;
	//NSDictionary *msDict;
	NSString *msTempStrings0,*msTempStrings1;
	NSUInteger nAlertId;
	//NSTimer *mConnectionTimer;
	NSTimer *mTimerTouchToClose;
	NSUInteger nCheckJarErrorRet;
	BOOL mbTouchToClose;
	// components
	UITextField *mTFEmail; 
	UITextField *mTFPassword; 
	UITextField *mTFPhoneNum; 
	UITextField *mTFPasswordConf; 
	UITextField *mTFSupportMsg; 
	UIButton *mBtnCombo;
	UIButton *mBtnLogin;
	UIButton *mBtnRegister;
	UIButton *mBtnSubmit;
	// new
	int mPageId;
	int mLastPageId;
	NSMutableArray *mLoginPageParams;
	BOOL mbBindMode;
	int mnComboType;
	float fWindowHeight;
}
@property(nonatomic,strong)UIViewController *uiViewCtrl;
@property(nonatomic,strong)GPUIView *tabBarView;
//@property(nonatomic,strong)UIWebView *mWebView;
@property(nonatomic,strong)UIScrollView *mScrollView;
//@property(nonatomic,strong)UIActivityIndicatorView *mActivityIndicView;
@end

@implementation Gamespark

@synthesize tabBarView;
//@synthesize mWebView;
@synthesize mScrollView;
//@synthesize mActivityIndicView;
@synthesize uiViewCtrl;

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Versioning
	//
	// 1.0
	// 	- Initial release
	// 1.1,1.2,1.3
	// 	- Skipped to standardize all versioning number for SDK & the PDF documentation
	// 1.4
	// 	- page support is available now
	// 	- bug minor fixed, to close logout busy indicator
	// 	- add getSdkVersion function
	// 	- add enableGateway function
	// 1.4.1
	// 	- bug fix when register new user.
	// 1.4.2
	// 	- remove classes that refer to iOS7 and later. This will allow running on iOS 6 down to 5
	// 	- bug fix rotate handling in iOS7 and below
	// 1.4.3
	// 	- fix back button on register
	// 1.4.4
	// 	- implement quick start
	//  - implement multiple user account
	// 1.4.5
	// 	- fixed several problems in quick start & its multiple user.
	// 1.4.6
	// 	- bug fixed, return message (respText) null onGamesparkReturn, null'ed by ARC
	// 	- bug fixed, when logout
	// 1.4.6.1
	// 	- bug on V146 still exist. duplicate respText to other var for onGamesparkReturn
	// 1.4.7
	// 	- now we activate custom header, standardize iOS & Android in how to access index page
	// 1.4.8
	// 	- it is now running good in iOS 6
	//	- bug fixed, floating icon animated tends to 'sunk' at lower left corner or at upper right corner
	//	- remember the last floating icon position.
	//	- add small circle button at upper left corner as a back button when it shows facebook login page
	// 1.4.9
	// 	- add channel parameter to distinguish mobile store
	// 	- file logging
	//
	// 1.5.0
	// 	- minor bug fixed on channel_id implementation
	// 1.5.1
	// 	- fixed all binding, facebook and email
	//	- implement new toast message for all success messages.
	//
	// 2.0
	// 	- native version
	//
	////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString*) getSdkVersion
{
	return [NSString stringWithFormat:@"v2.0"];
}

void dp(NSString *format, ...)
{
	if(gbProduction) return;
	va_list args;
	va_start(args, format);
	NSLogv([NSString stringWithFormat:@"Gamespark:%@", format], args);
	va_end(args);
	//#if TARGET_IPHONE_SIMULATOR == 0
	NSString *s = [[NSString alloc] initWithFormat:format arguments:args];
	if(mAls == nil) mAls = [[NSMutableArray alloc] init];
	[mAls addObject:s];
	if([mAls count] < 16) return;
	[mAls removeObjectAtIndex:0]; 
	//#endif
}

- (void) flushLog
{
	if(mAls == nil) return;
	NSString *sals = [mAls componentsJoinedByString:@"\n"];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docdir = [paths objectAtIndex:0];
	NSString *filePath = [NSString stringWithFormat:@"%@/%@", docdir, @"gplog.txt"];
	if([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
		NSError *error;
		[sals writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
		if(error == nil) {
			dp(@"written to %@", filePath);
		} else {
			dp(@"write file error: %@", error);
		}
	} else {
		dp(@"path not writeable");
	}
}

- (void) enableGateway:(BOOL)productionMode
{
	if(productionMode) {
		//msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"https://api.gamespark.net/api/v1/check/jar"];
		msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"https://api.gamespark.net/api/v2/get/params/{product_id}"];
		gbProduction = YES;
		return;
	}
	//msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://staging.gamespark.net/api/v1/check/jar"];
	msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://staging.gamespark.net/api/v2/get/params/{product_id}"];
	gbProduction = NO;
}

// - (void) enableGateway:(BOOL)productionMode
// {
// 	if(productionMode) {
// 		msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"https://api.gamespark.net/api/v2/get/params/{product_id}"];
// 		gbProduction = YES;
// 		return;
// 	}
// 	msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://staging.gamespark.net/api/v2/get/params/{product_id}"];
// 	gbProduction = NO;
// }

- (void)create:(UIViewController*)uivc gamesparkOnReturn:(GamesparkOnReturn)gamesparkOnReturn
	 productId:(NSString*)productId productName:(NSString*)productName channelId:(NSString*)channelId
{	
	msSecretKey = [[NSString alloc] initWithFormat:@"%@",@"XJf8o3OJ4qtaOYaBYO4XOtwMMJiBD6qG"];
	msProductId = [[NSString alloc] initWithFormat:@"%@",productId];
	mProductName = [[NSString alloc] initWithFormat:@"%@",productName];
	msChannelId = [[NSString alloc] initWithFormat:@"%@",channelId];
	self.uiViewCtrl = uivc;
	mGpOnReturn = gamesparkOnReturn;
	[self clearVars];
}

- (void) clearVars
{
	//mbJimbasOrg = NO;
	mbLoggedIn = NO;	
	msUserId = nil;
	msIndexUrl = nil;
	msLoginUrl = nil;
	msRegisterUrl = nil;
	msFbUrl = nil;
	msProfileUrl = nil;
	msLogoutUrl = nil;
	msBindEmail = nil;
	msBindFb = nil;
	msSupportUrl = nil;
	msQuickStartUrl = nil;
	msQuickLoginUrl = nil;
	msFbFansPageUrl = nil;
	//msPage = nil;
	nTag = 0;
	//msDict = nil;
	msTempStrings0 = msTempStrings1 = nil;
	nAlertId = 0;
	//mConnectionTimer = nil;
	mTimerTouchToClose = nil;
	nCheckJarErrorRet = 999;
	mbTouchToClose = NO;
	//btnFbBack = nil;
	gbProduction = NO;
	[self enableGateway: NO];
	msCheckJar = [[NSString alloc] initWithFormat:@"%@",@"http://staging.gamespark.net/api/v2/get/params/{product_id}"];
	mTFEmail = nil;
	mTFPassword = nil;
	mTFPhoneNum = nil;
	mTFPasswordConf = nil;
	mTFSupportMsg = nil;
	mBtnCombo = nil;
	mBtnLogin = nil;
	mBtnRegister = nil;
	mBtnSubmit = nil;
	mPageId = 0;
	mLastPageId = 0;
	mLoginPageParams = nil;
	mbBindMode = NO;
	mnComboType = 0;
	fWindowHeight = 0.0f;
}

-(void) enableTouchToClose:(BOOL)touchToClose;
{
	mbTouchToClose = touchToClose;
}

-(void) touchToClose:(NSTimer*)tmr
{
	mTimerTouchToClose = nil;
}

- (void) uiViewTouched:(BOOL)wasInside
{
	if(!mbTouchToClose) return;
	if(!wasInside) {
		if(mTimerTouchToClose == nil) {
			mTimerTouchToClose = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(touchToClose:) userInfo:nil repeats:NO];
		} else {
			// close the thing here...
			[mTimerTouchToClose invalidate];
			mTimerTouchToClose = nil;
			if(tabBarView != nil) {
				[tabBarView setHidden:YES];
				tabBarViewShown = NO;
				NSString *errmsg = [NSString stringWithFormat:@"User Canceled"];
				[self displayToastWithMessage:errmsg];
			    mGpOnReturn(GPRET_LOGIN_CANCELED, errmsg);
			}
		}
	}
}

- (void)execute:(NSUInteger)tag
{
	BOOL bexec = NO;
	NSString *errmsg = nil;
	switch(tag) {
	case GPEXEC_LOGIN:
		if(mbLoggedIn) {
			errmsg = [NSString stringWithFormat:@"Logged in already"];
			[self displayToastWithMessage:errmsg];
			mGpOnReturn(GPRET_LOGIN_ALREADY, errmsg);
			return;
		}
		nCheckJarErrorRet = GPRET_LOGIN_ERROR;
		[self initFloatingIcon];
		[self initTabBarView];
		[self showLoading];
		bexec = YES;
		break;
	case GPEXEC_LOGOUT:
		if(!mbLoggedIn) {
			errmsg = [NSString stringWithFormat:@"Logged out already or never login"];
			[self displayToastWithMessage:errmsg];
			mGpOnReturn(GPRET_NOT_LOGIN_YET, errmsg);
			return;
		}
		nCheckJarErrorRet = GPRET_LOGOUT_ERROR;
		bexec = YES;
		if(tabBarView == nil) {
			dp(@"Try logging out but view not available");
			return;
		}
		[self showLoading];
		[tabBarView setHidden:NO];
		tabBarViewShown = YES;
		break;
	default:
		errmsg = [NSString stringWithFormat:@"Unknown Tag"];
		[self displayToastWithMessage:errmsg];
		mGpOnReturn(GPRET_UNKNOWN_TAG, errmsg);
		return;
	}

	if(!bexec) return;
	nTag = tag;
	// [self initTabBarView];
	// [self initFloatingIcon];
	// [self showLoading];
	//[self loadProgress];
	[self getCheckJar];
}

- (NSString*) getIndexInJsonArray:(NSString*)jarrstr stringToCheck:(NSString*)str
{
    int i;
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    for(i=0;i<[values count];i++) {
        if([[values objectAtIndex:i] isEqualToString:str]) {
            return [NSString stringWithFormat:@"%d",i];
        }
    }
    return [NSString stringWithFormat:@"%@",@"-1"];
}

- (NSString*) getStringFromJsonArray:(NSString*)jarrstr indexString:(NSString*)indexString
{
    int idx = [indexString intValue];
    if(idx == -1) return nil;
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSString* str = [[NSString alloc] initWithFormat:@"%@",[values objectAtIndex:idx]];
    return str;
}

- (NSString*) addStringToJsonArray:(NSString*)jarrstr stringToSet:(NSString*)str
{
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    [values addObject:str];
    data = [NSJSONSerialization dataWithJSONObject:values options:0 error:nil];
    NSString *jsonstr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return jsonstr;
}

- (NSString*) setStringInJsonArray:(NSString*)jarrstr indexString:(NSString*)indexString stringToSet:(NSString*)stringToSet
{
    int idx = [indexString intValue];
    NSData* data = [jarrstr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
	[values setObject:stringToSet atIndexedSubscript:idx];
    data = [NSJSONSerialization dataWithJSONObject:values options:0 error:nil];
    NSString *jsonstr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return jsonstr;
}

- (void) normalizaUserAcountInPreference
{
	NSString *semail = [self getUserPreference:@"gamesparkEmail"];
	if(semail == nil || [semail isEqualToString:@"(null)"]) {
		semail = [NSString stringWithFormat:@"%@", @"[]"];
		[self setUserPreference:semail forKey:@"gamesparkEmail"];
	}
	NSString *password = [self getUserPreference:@"gamesparkPassword"];
	if(password == nil || [password isEqualToString:@"(null)"]) [self setUserPreference:@"[]" forKey:@"gamesparkPassword"];
	NSString *userid = [self getUserPreference:@"gamesparkUserId"];
	if(userid == nil || [userid isEqualToString:@"(null)"]) [self setUserPreference:@"[]" forKey:@"gamesparkUserId"];
	NSString *loggedin = [self getUserPreference:@"gamesparkLoggedIn"];
	if(loggedin == nil || [loggedin isEqualToString:@"(null)"]) [self setUserPreference:@"-1" forKey:@"gamesparkLoggedIn"];
	NSString *sbindlink = [self getUserPreference:@"gamesparkBindLink"];
	if(sbindlink == nil || [sbindlink isEqualToString:@"(null)"]) {
		sbindlink = [NSString stringWithFormat:@"%@", @"[]"];
		[self setUserPreference:sbindlink forKey:@"gamesparkBindLink"];
	}

	// testing
    //semail = [[NSString alloc] initWithFormat:@"[\"jim\",\"bas\",\"teng\"]"];
    //sbindlink = [[NSString alloc] initWithFormat:@"[\"\",\"\"]"];

    NSData* demail = [semail dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *maemail = [NSJSONSerialization JSONObjectWithData:demail options:NSJSONReadingMutableContainers error:nil];
    NSData* dbindlink = [sbindlink dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *mabindlink = [NSJSONSerialization JSONObjectWithData:dbindlink options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"email&bindlink: %@ %@ %d %d", semail, sbindlink, (int)[maemail count], (int)[mabindlink count]);
    unsigned long i,j = [maemail count] - [mabindlink count];
    if(j > 0) {
    	for(i=0;i<j;i++) {
		   	NSString *s = [NSString stringWithFormat:@"%@", @""];
		   	[mabindlink addObject:s];
		   	NSLog(@"addObject: %@", s);
		}
		dbindlink = [NSJSONSerialization dataWithJSONObject:mabindlink options:0 error:nil];
	    sbindlink = [[NSString alloc] initWithData:dbindlink encoding:NSUTF8StringEncoding];
	    [self setUserPreference:sbindlink forKey:@"gamesparkBindLink"];
    }
    //NSLog(@"email&bindlink: %@ %@", semail, sbindlink);
}

- (void) executeResume
{	
	[self normalizaUserAcountInPreference];
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];
	dp(@"executeResume:\n\temail=%@\n\tpassword:%@\n\tuserid:%@\n\tloggedin:%@\n\tbindlink:%@\n", spref0, spref1, spref2, spref3, spref6);
	
	BOOL userdetexist = [spref3 isEqualToString:@"-1"] ? NO : YES;//?

	if(userdetexist) {	
		msUserId = [self getDecryptedPassword:[self getStringFromJsonArray:spref2 indexString:spref3]];
	}

	switch(nTag) {
	case GPEXEC_LOGIN: {
		BOOL blandingpage = YES;
		if(userdetexist) {
			dp(@"user details exist. try to logging in...");
			NSString *semail = [self getStringFromJsonArray:spref0 indexString:spref3];
			NSString *spassword = [self getStringFromJsonArray:spref1 indexString:spref3];
			if([semail isEqualToString:@"Guest"]) {
				[self doQuickStart:msQuickLoginUrl];
			} else if([semail isEqualToString:@"Facebook_Enabled"]) {
				dp(@"Login using facebook account");// on Facebook Binding instead");
				[self doFbLogin:[self getDecryptedPassword:spassword] bind:NO];
			} else {
				dp(@"Existing user details available. Do normal login");
				[self doLogin:semail encryptedPassword:[self getDecryptedPassword:spassword] bind:NO];
			}
			blandingpage = NO;
		}
		if(blandingpage) {
		    NSData* demail = [spref0 dataUsingEncoding:NSUTF8StringEncoding];
		    mLoginPageParams = [NSJSONSerialization JSONObjectWithData:demail options:NSJSONReadingMutableContainers error:nil];
			[self showLoginPage:NO];
		}
	} break;
	case GPEXEC_LOGOUT: {
			[self doLogout];
		}
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (UIColor*) invertColor:(UIColor*) color
{
	CGFloat *rgba = (CGFloat*) CGColorGetComponents(color.CGColor);
	return [[UIColor alloc] initWithRed:(1.0 - rgba[0]) green:(1.0 - rgba[1]) blue:(1.0 - rgba[2]) alpha:rgba[3]];
}

- (void) btnInvertColor:(UIButton*)sender
{
	NSArray *subviews = [sender subviews];
	if([subviews count] > 1) {
		int i=0;
		for(UIView *subview in subviews) {
			if(i++ > 0) subview.backgroundColor = [self invertColor:subview.backgroundColor];
		}
	}
	sender.backgroundColor = [self invertColor:sender.backgroundColor];
}

- (void) btnTouchUp:(UIButton*)sender
{
	[self btnInvertColor:sender];
	// if(mTFEmail != nil && mTFPassword != nil) {
	//     dp(@"Good jorb, you pressed a button: %@ # %@", mTFEmail.text, mTFPassword.text);	//sender.titleLabel.text);
	// }
	switch(sender.tag)
	{
	// login menu
	case GPBTN_QUICKSTART: {
			//dp(@"button quick start");
			[self doQuickStart:msQuickStartUrl];
		} break;
	case GPBTN_LOGIN: {
			//dp(@"button login");
			//if(isGamesparkNull()) return;
			NSString *semail = nil;
			if(mBtnCombo != nil) {
				semail = [NSString stringWithFormat:@"%@", mBtnCombo.currentTitle];
			} else {
				semail = [NSString stringWithFormat:@"%@", @""];
			}
			NSString *spassword = [NSString stringWithFormat:@"%@",@""];
			if(mTFEmail != nil && mTFPassword != nil) {
				semail = [NSString stringWithFormat:@"%@",mTFEmail.text];
				spassword = [NSString stringWithFormat:@"%@",mTFPassword.text];
			}
			NSRange r1 = [semail rangeOfString:@"fb:"];
			NSRange r2 = [semail rangeOfString:@"@"];
			NSRange r3 = [semail rangeOfString:@"."];
			if(mTFEmail != nil && r1.location == NSNotFound && r2.location == NSNotFound && r3.location == NSNotFound) {
				dp(@"Email is not in correct format");
				[self displayToastWithMessage:[NSString stringWithFormat:@"%@",@"Email is not in correct format!"]];
				return;
			}
			// do login account of bind
			[self showLoading];
			r1 = [semail rangeOfString:@"fb:"];
			if(mTFEmail != nil && r1.location == NSNotFound && mTFPassword != nil) {
				[self doLogin:semail encryptedPassword:[self getEncryptedPassword:spassword] bind:mbBindMode];
				mbBindMode = NO;
			} else if([semail isEqualToString:@"Guest"]) {
				[self doQuickStart:msQuickLoginUrl];
			} else {
				NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
				NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
				if([semail rangeOfString:@"fb:"].location != NSNotFound) {
					semail = [NSString stringWithFormat:@"%@",@"Facebook_Enabled"];
				}
				NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:semail];
			    NSString *spwd = [self getStringFromJsonArray:spref1 indexString:lidx];
				if([semail isEqualToString:@"Facebook_Enabled"] && spwd != nil && [semail isEqualToString:@""] == NO) {
					[self doFbLogin:[self getDecryptedPassword:spwd] bind:mbBindMode];
				} else {
					[self doLogin:semail encryptedPassword:[self getDecryptedPassword:spwd] bind:mbBindMode];
				}
				mbBindMode = NO;
			}
		} break;
	case GPBTN_BACK: {
			//dp(@"button back");
			if(mbLoggedIn) {
				[self showProfilePage];
			} else {
				//[self showLoginPage:mbBindMode?YES:NO];
				[self showLoginPage:NO];
			}
		} break;
	case GPBTN_FACEBOOK: {
			//dp(@"button facebook");
			[self doFbWork];
		} break;
	case GPBTN_REGISTER: {
			[self showRegisterPage];
		} break;

	// register menu
	case GPBTN_REG_REGISTER: {
			//dp(@"go register");
			NSString *semail = [NSString stringWithFormat:@"%@",mTFEmail.text];
			NSString *smdn = [NSString stringWithFormat:@"%@",mTFPhoneNum.text];
			NSString *spasswd = [NSString stringWithFormat:@"%@",mTFPassword.text];
			NSString *sconfpass = [NSString stringWithFormat:@"%@",mTFPasswordConf.text];
			[self doRegistration:semail phonenum:smdn password:spasswd confPassword:sconfpass bind:mbBindMode];
		} break;
	case GPBTN_REG_BACK: {
			if(mbLoggedIn) {
				[self showLoginPage:YES];
			} else {
				[self showLoginPage:YES];
			}
		} break;

	// profile menu
	case GPBTN_BINDEMAIL: {
			mbBindMode = YES;
			[self showLoginPage:YES];
		} break;
	case GPBTN_BINDFACEBOOK: {
			mbBindMode = YES;
			//lagi nyelidik mbBindMode nya yes atau no
			[self doFbWork];
		} break;
	case GPBTN_MORE: {
			NSString *s = [NSString stringWithFormat:@"%@",@"http://www.google.com"];
			[self loadUrlWithBrowser:s];
		} break;
	case GPBTN_FBFANPAGE: {
			NSString *s = [NSString stringWithFormat:@"%@",msFbFansPageUrl];
			[self loadUrlWithBrowser:s];
		} break;
	case GPBTN_SUPPORT: {
			[self showSupportPage];
		} break;
	case GPBTN_LOGOUT: {
			[self doYesNoAlert:@"Logout" message:@"Are you sure?" alertid:ALERTID_LOGOUT];
		} break;

	// support page
	case GPBTN_SUPP_SUBMIT: {
			dp(@"button support submit");
			if(mTFEmail == nil || mBtnCombo == nil || mTFSupportMsg == nil) return;
			NSString *semail = [NSString stringWithFormat:@"%@",mTFEmail.text];
			NSString *scateg = [NSString stringWithFormat:@"%@", mBtnCombo.currentTitle];
			NSString *ssuppmsg = [NSString stringWithFormat:@"%@",mTFSupportMsg.text];
			dp(@"submiting support message. email: %@, category: %@, message: %@", semail, scateg, ssuppmsg);
			NSRange r1 = [semail rangeOfString:@"fb:"];
			NSRange r2 = [semail rangeOfString:@"@"];
			NSRange r3 = [semail rangeOfString:@"."];
			if(mTFEmail != nil && r1.location == NSNotFound && r2.location == NSNotFound && r3.location == NSNotFound) {
				dp(@"Email is not in correct format");
				[self displayToastWithMessage:[NSString stringWithFormat:@"%@",@"Email is not in correct format!"]];
				return;
			}
			[self showLoading];
			[self doSupport:semail category:scateg message:ssuppmsg];
		} break;
	case GPBTN_SUPP_BACK: {
		[self showProfilePage];
		} break;
	}
}


- (void) btnTouchCancel:(UIButton*)sender
{
	[self btnInvertColor:sender];
}

- (void) btnTouchDown:(UIButton*)sender
{
	[self btnInvertColor:sender];
}

- (void) showLoading
{
	[mScrollView setContentOffset:CGPointZero animated:NO];
	[mScrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
	UIActivityIndicatorView *actvIndicView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	actvIndicView.transform = CGAffineTransformMakeScale(0.9, 0.9);
	actvIndicView.center = CGPointMake(self.tabBarView.bounds.size.width / 2, self.tabBarView.bounds.size.height / 2);
	[mScrollView addSubview:actvIndicView];
	mScrollView.contentSize = CGSizeMake(tabBarView.bounds.size.width, tabBarView.bounds.size.height);
	[actvIndicView startAnimating];
}

- (void)textFieldDidBeginEditing:(UITextField *)sender
{
	CGPoint scrollPoint = CGPointMake(0, sender.frame.origin.y - 10);
	[mScrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)sender
{
	[mScrollView setContentOffset:CGPointZero animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField*)sender
{
	[mScrollView setContentOffset:CGPointZero animated:NO];
    [sender resignFirstResponder];
    //dp(@"close keyboard");
    return NO;
}

- (void)textFieldLoginPageDidChange:(UITextField*)sender
{
	if(mBtnLogin == nil) return;
	NSString *s = [NSString stringWithFormat:@"%@", mTFEmail.text];
	NSString *t = [NSString stringWithFormat:@"%@", mTFPassword.text];
	int a = (int) [s length];
	int b = (int) [t length];
	unsigned int col = 0;
	if(a > 0 && b > 0) {
		[mBtnLogin setEnabled:YES];
		col = 0xFFFFAB00;
	} else {
		[mBtnLogin setEnabled:NO];
		col = 0xFF9E9E9E;
	}
	NSArray *subviews = [mBtnLogin subviews];
	if([subviews count] > 1) {
		int i=0;
		for(UIView *subview in subviews) {
			if(i++ > 0) subview.backgroundColor = UIColorFromRGB(col);
		}
	}
	mBtnLogin.backgroundColor = UIColorFromRGB(col);
}

- (void)textFieldRegisterPageDidChange:(UITextField*)sender
{
	if(mBtnRegister == nil) return;
	NSString *s = [NSString stringWithFormat:@"%@", mTFEmail.text];
	NSString *t = [NSString stringWithFormat:@"%@", mTFPassword.text];
	NSString *u = [NSString stringWithFormat:@"%@", mTFPhoneNum.text];
	NSString *v = [NSString stringWithFormat:@"%@", mTFPasswordConf.text];
	int a = (int) [s length];
	int b = (int) [t length];
	int c = (int) [u length];
	int d = (int) [v length];
	unsigned int col = 0;
	if(a > 0 && b > 0 && c > 0 && d > 0) {
		[mBtnRegister setEnabled:YES];
		col = 0xFFFFAB00;
	} else {
		[mBtnRegister setEnabled:NO];
		col = 0xFF9E9E9E;
	}
	NSArray *subviews = [mBtnRegister subviews];
	if([subviews count] > 1) {
		int i=0;
		for(UIView *subview in subviews) {
			if(i++ > 0) subview.backgroundColor = UIColorFromRGB(col);
		}
	}
	mBtnRegister.backgroundColor = UIColorFromRGB(col);
}

- (void)textFieldSupportPageDidChange:(UITextField*)sender
{
	if(mBtnSubmit == nil) return;
	NSString *s = [NSString stringWithFormat:@"%@", mTFEmail.text];
	NSString *t = [NSString stringWithFormat:@"%@", mTFSupportMsg.text];
	int a = (int) [s length];
	int b = (int) [t length];
	unsigned int col = 0;
	if(a > 0 && b > 0) {
		[mBtnSubmit setEnabled:YES];
		col = 0xFFFFAB00;
	} else {
		[mBtnSubmit setEnabled:NO];
		col = 0xFF9E9E9E;
	}
	NSArray *subviews = [mBtnSubmit subviews];
	if([subviews count] > 1) {
		int i=0;
		for(UIView *subview in subviews) {
			if(i++ > 0) subview.backgroundColor = UIColorFromRGB(col);
		}
	}
	mBtnSubmit.backgroundColor = UIColorFromRGB(col);
}

- (void) showLoginPage:(BOOL) onOtherAccount
{
	mLastPageId = mPageId;
	mPageId = onOtherAccount ? PAGE_OTHERACC : PAGE_LOGIN;

	// remove all views
	[mScrollView setContentOffset:CGPointZero animated:NO];
	[mScrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

	int offs = 10;
	int frw = mScrollView.bounds.size.width;
	int w = 106; int h = 70;
	int x = (frw - w) / 2;
	int y = offs;

	UIImageView *gplogo =[[UIImageView alloc] initWithFrame:CGRectMake(x,y,w,h)];
	gplogo.image=[UIImage imageNamed:@"gamesparklogo.png"];
	[mScrollView addSubview:gplogo];

    // if([mLoginPageParams count] <= 0) {
	if(![self isGuestAvail] && !mbLoggedIn) {
		// button quick start
		x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
		UIButton *btnQuickStart = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
		[btnQuickStart setTitle:@"Quick Start" forState:UIControlStateNormal];
		[btnQuickStart addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
		[btnQuickStart addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
		[btnQuickStart addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
		[btnQuickStart.titleLabel setTextAlignment: NSTextAlignmentCenter];
		[btnQuickStart.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		btnQuickStart.layer.cornerRadius = 6;
		btnQuickStart.backgroundColor = UIColorFromRGB(0xFFFFAB00);
		[btnQuickStart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		btnQuickStart.tag = GPBTN_QUICKSTART;
		[mScrollView addSubview:btnQuickStart];
	}

	BOOL btnLoginEnabled = YES;
	unsigned int btnLoginCol = 0xFFFFAB00;
	NSString *s = nil;
	if(mLoginPageParams != nil) {
		NSData *d = [NSJSONSerialization dataWithJSONObject:mLoginPageParams options:0 error:nil];
		s = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
	}

	if((s != nil && [s isEqualToString:@"[]"]) || onOtherAccount) {
		/////////////////////////////////////////////////////////////////////////////////
		// email editbox
		x = offs; y += (h + offs + offs); w = frw - offs - offs; h = 36;
		mTFEmail = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
	    [mTFEmail setBorderStyle:UITextBorderStyleRoundedRect];
		[mTFEmail setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		mTFEmail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		mTFEmail.textAlignment = NSTextAlignmentLeft;
		mTFEmail.keyboardType = UIKeyboardTypeEmailAddress;
		mTFEmail.leftViewMode = UITextFieldViewModeAlways;
		mTFEmail.placeholder = [NSString stringWithFormat:@"Email..."];
		mTFEmail.clearButtonMode = UITextFieldViewModeAlways;
		mTFEmail.returnKeyType = UIReturnKeyDone;
		[mTFEmail addTarget:self action:@selector(textFieldLoginPageDidChange:) forControlEvents:UIControlEventEditingChanged];
		mTFEmail.delegate = self;
		[mScrollView addSubview:mTFEmail];
		// Add a "textFieldDidChange" notification method to the text field control.
		/////////////////////////////////////////////////////////////////////////////////
		// password editbox
		x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
		mTFPassword = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
	    [mTFPassword setBorderStyle:UITextBorderStyleRoundedRect];
		[mTFPassword setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		mTFPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		mTFPassword.textAlignment = NSTextAlignmentLeft;
		mTFPassword.keyboardType = UIKeyboardTypeEmailAddress;
		mTFPassword.leftViewMode = UITextFieldViewModeAlways;
		mTFPassword.placeholder = [NSString stringWithFormat:@"Password..."];
		mTFPassword.clearButtonMode = UITextFieldViewModeAlways;
		mTFPassword.returnKeyType = UIReturnKeyDone;
		[mTFPassword addTarget:self action:@selector(textFieldLoginPageDidChange:) forControlEvents:UIControlEventEditingChanged];
		mTFPassword.secureTextEntry = YES;
		mTFPassword.delegate = self;
		[mScrollView addSubview:mTFPassword];
		btnLoginCol = 0xFF9E9E9E;
		btnLoginEnabled = NO;
	} else {
		/////////////////////////////////////////////////////////////////////////////////
		// combo box atau spinner di android
		mTFEmail = mTFPassword = nil;
		x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
		mBtnCombo = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
		NSString *s = [NSString stringWithFormat:@"%@", [mLoginPageParams objectAtIndex:0]];
		if([s isEqualToString:@"Facebook_Enabled"]) {
			s = [NSString stringWithFormat:@"fb: %@",[self getFacebookName:0]];
		}
		//[mBtnCombo setTitle:[mLoginPageParams objectAtIndex:0] forState:UIControlStateNormal];
		[mBtnCombo setTitle:s forState:UIControlStateNormal];
		[mBtnCombo addTarget:self action:@selector(BtnAccountsClick:) forControlEvents:UIControlEventTouchUpInside];
		//[mBtnCombo addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
		[mBtnCombo.titleLabel setTextAlignment: NSTextAlignmentCenter];
		[mBtnCombo.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		//mBtnCombo.layer.cornerRadius = 6;
		//mBtnCombo.backgroundColor = UIColorFromRGB(0xFFFFAB00);
		[mBtnCombo setTitleColor:self.uiViewCtrl.view.tintColor forState:UIControlStateNormal];
		UIView *vwAccountsLine = [[UIView alloc] initWithFrame:CGRectMake(offs, mBtnCombo.bounds.size.height-1, mBtnCombo.bounds.size.width - offs - offs, 1)];
		vwAccountsLine.backgroundColor = self.uiViewCtrl.view.tintColor;
		[mBtnCombo addSubview:vwAccountsLine];
		UIView *vwAccountsLine2 = [[UIView alloc] initWithFrame:CGRectMake(mBtnCombo.bounds.size.width-offs-8, mBtnCombo.bounds.size.height-8, 8, 8)];
		vwAccountsLine2.backgroundColor = self.uiViewCtrl.view.tintColor;
		[mBtnCombo addSubview:vwAccountsLine2];
		[mScrollView addSubview:mBtnCombo];
	}

	/////////////////////////////////////////////////////////////////////////////////
    // button login
	x = offs; y += (h + offs + offs); w = (frw / 2) - offs; h = 36;
	mBtnLogin = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[mBtnLogin setTitle:mbBindMode ? @"Bind" : @"Login" forState:UIControlStateNormal];
	[mBtnLogin addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[mBtnLogin addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[mBtnLogin addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[mBtnLogin.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[mBtnLogin.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mBtnLogin.layer.cornerRadius = 6;
	mBtnLogin.backgroundColor = UIColorFromRGB(btnLoginCol);
	[mBtnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	UIView *vwLoginLeftSolid = [[UIView alloc] initWithFrame:CGRectMake(w-4, 0, 4, h)];
	vwLoginLeftSolid.backgroundColor = UIColorFromRGB(btnLoginCol);
	[mBtnLogin addSubview:vwLoginLeftSolid];
	mBtnLogin.tag = GPBTN_LOGIN;
	[mScrollView addSubview:mBtnLogin];
	[mBtnLogin setEnabled:btnLoginEnabled];

	if(onOtherAccount) {
		w = (2 * (frw / 3)) - offs;
		mBtnLogin.frame = CGRectMake(x, y, w, h);
		vwLoginLeftSolid.frame = CGRectMake(w-4, 0, 4, h);
		// button back login
		x = offs + w + 1; //y += (h + offs); w = frw / 2; h = 36;
		w = (1 * (frw / 3)) - offs;
		UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
		[btnBack setTitle:@"Back" forState:UIControlStateNormal];
		[btnBack addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
		[btnBack addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
		[btnBack addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
		[btnBack.titleLabel setTextAlignment: NSTextAlignmentCenter];
		[btnBack.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		btnBack.layer.cornerRadius = 6;
		btnBack.backgroundColor = UIColorFromRGB(0xFFFF9100);
		[btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		UIView *vwBackRightSolid = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, btnBack.bounds.size.height)];
		vwBackRightSolid.backgroundColor = UIColorFromRGB(0xFFFF9100);
		[btnBack addSubview:vwBackRightSolid];
		btnBack.tag = GPBTN_BACK;
		[mScrollView addSubview:btnBack];
	} else {
		// button facebook login
		x = offs + w + 1; //y += (h + offs); w = frw / 2; h = 36;
		UIButton *btnFacebook = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
		[btnFacebook setTitle:@"Facebook" forState:UIControlStateNormal];
		[btnFacebook addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
		[btnFacebook addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
		[btnFacebook addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
		[btnFacebook.titleLabel setTextAlignment: NSTextAlignmentCenter];
		[btnFacebook.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		btnFacebook.layer.cornerRadius = 6;
		btnFacebook.backgroundColor = UIColorFromRGB(0xFF0D47A1);
		[btnFacebook setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		UIView *vwFbRightSolid = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, btnFacebook.bounds.size.height)];
		vwFbRightSolid.backgroundColor = UIColorFromRGB(0xFF0D47A1);
		[btnFacebook addSubview:vwFbRightSolid];
		btnFacebook.tag = GPBTN_FACEBOOK;
		[mScrollView addSubview:btnFacebook];
	}

	// button register
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	UIButton *btnRegister = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnRegister setTitle:@"Register" forState:UIControlStateNormal];
	[btnRegister addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnRegister addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnRegister addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnRegister.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnRegister.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnRegister.layer.cornerRadius = 6;
	btnRegister.backgroundColor = UIColorFromRGB(0xFFFFAB00);
	[btnRegister setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	btnRegister.tag = GPBTN_REGISTER;
	[mScrollView addSubview:btnRegister];

	y += (h + offs + offs); 
	mScrollView.contentSize = CGSizeMake(tabBarView.bounds.size.width-10, y);
	[self normalizeWindowHeight:y+offs];
}

-(void)normalizeWindowHeight:(float)height
{
	CGRect frm = self.tabBarView.frame;
	frm.size.height = height < fWindowHeight ? height : fWindowHeight;
	[self.tabBarView setFrame:frm];
}

-(void) showRegisterPage
{
	mLastPageId = mPageId;
	mPageId = PAGE_REGISTER;

	// remove all views
	[mScrollView setContentOffset:CGPointZero animated:NO];
	[mScrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

	int offs = 10;
	int frw = mScrollView.bounds.size.width;
	int w = 106; int h = 70;
	int x = (frw - w) / 2;
	int y = offs;

	UIImageView *gplogo =[[UIImageView alloc] initWithFrame:CGRectMake(x,y,w,h)];
	gplogo.image=[UIImage imageNamed:@"gamesparklogo.png"];
	[mScrollView addSubview:gplogo];

	// text field email
	// x = offs; y += (h + offs + offs); w = frw - offs - offs; h = 36;
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	mTFEmail = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
    [mTFEmail setBorderStyle:UITextBorderStyleRoundedRect];
	[mTFEmail setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mTFEmail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	mTFEmail.textAlignment = NSTextAlignmentLeft;
	mTFEmail.keyboardType = UIKeyboardTypeEmailAddress;
	mTFEmail.leftViewMode = UITextFieldViewModeAlways;
	mTFEmail.placeholder = [NSString stringWithFormat:@"Email ..."];
	mTFEmail.clearButtonMode = UITextFieldViewModeAlways;
	mTFEmail.returnKeyType = UIReturnKeyDone;
	[mTFEmail addTarget:self action:@selector(textFieldRegisterPageDidChange:) forControlEvents:UIControlEventEditingChanged];
	mTFEmail.delegate = self;
	[mScrollView addSubview:mTFEmail];

	// text field phone number
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	mTFPhoneNum = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
    [mTFPhoneNum setBorderStyle:UITextBorderStyleRoundedRect];
	[mTFPhoneNum setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mTFPhoneNum.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	mTFPhoneNum.textAlignment = NSTextAlignmentLeft;
	mTFPhoneNum.keyboardType = UIKeyboardTypeDecimalPad;
	mTFPhoneNum.leftViewMode = UITextFieldViewModeAlways;
	mTFPhoneNum.placeholder = [NSString stringWithFormat:@"Phone number ..."];
	mTFPhoneNum.clearButtonMode = UITextFieldViewModeAlways;
	mTFPhoneNum.returnKeyType = UIReturnKeyDone;
	[mTFPhoneNum addTarget:self action:@selector(textFieldRegisterPageDidChange:) forControlEvents:UIControlEventEditingChanged];
	mTFPhoneNum.delegate = self;
	[mScrollView addSubview:mTFPhoneNum];

	// text field password
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	mTFPassword = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
    [mTFPassword setBorderStyle:UITextBorderStyleRoundedRect];
	[mTFPassword setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mTFPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	mTFPassword.textAlignment = NSTextAlignmentLeft;
	mTFPassword.keyboardType = UIKeyboardTypeEmailAddress;
	mTFPassword.leftViewMode = UITextFieldViewModeAlways;
	mTFPassword.placeholder = [NSString stringWithFormat:@"Password ..."];
	mTFPassword.clearButtonMode = UITextFieldViewModeAlways;
	mTFPassword.returnKeyType = UIReturnKeyDone;
	[mTFPassword addTarget:self action:@selector(textFieldRegisterPageDidChange:) forControlEvents:UIControlEventEditingChanged];
	mTFPassword.delegate = self;
	mTFPassword.secureTextEntry = YES;
	[mScrollView addSubview:mTFPassword];

	// text field password confirmation
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	mTFPasswordConf = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
    [mTFPasswordConf setBorderStyle:UITextBorderStyleRoundedRect];
	[mTFPasswordConf setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mTFPasswordConf.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	mTFPasswordConf.textAlignment = NSTextAlignmentLeft;
	mTFPasswordConf.keyboardType = UIKeyboardTypeEmailAddress;
	mTFPasswordConf.leftViewMode = UITextFieldViewModeAlways;
	mTFPasswordConf.placeholder = [NSString stringWithFormat:@"Confrm Password ..."];
	mTFPasswordConf.clearButtonMode = UITextFieldViewModeAlways;
	mTFPasswordConf.returnKeyType = UIReturnKeyDone;
	[mTFPasswordConf addTarget:self action:@selector(textFieldRegisterPageDidChange:) forControlEvents:UIControlEventEditingChanged];
	mTFPasswordConf.delegate = self;
	mTFPasswordConf.secureTextEntry = YES;
	[mScrollView addSubview:mTFPasswordConf];

	// button register and back
	x = offs; y += (h + offs); w = ((2*frw) / 3) - offs; h = 36;
	mBtnRegister = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[mBtnRegister setTitle:mbBindMode ? @"Bind New" : @"Register" forState:UIControlStateNormal];
	[mBtnRegister addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[mBtnRegister addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[mBtnRegister addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[mBtnRegister.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[mBtnRegister.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mBtnRegister.layer.cornerRadius = 6;
	mBtnRegister.backgroundColor = UIColorFromRGB(0xFF9E9E9E);
	[mBtnRegister setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	UIView *vwLoginLeftSolid = [[UIView alloc] initWithFrame:CGRectMake(w-4, 0, 4, h)];
	vwLoginLeftSolid.backgroundColor = UIColorFromRGB(0xFF9E9E9E);
	[mBtnRegister addSubview:vwLoginLeftSolid];
	mBtnRegister.tag = GPBTN_REG_REGISTER;
	[mScrollView addSubview:mBtnRegister];
	[mBtnRegister setEnabled:NO];

	// button facebook login
	x = offs + w + 1; //y += (h + offs); w = frw / 2; h = 36;
	w = (frw / 3) - offs;
	UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnBack setTitle:@"Back" forState:UIControlStateNormal];
	[btnBack addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnBack addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnBack addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnBack.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnBack.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnBack.layer.cornerRadius = 6;
	btnBack.backgroundColor = UIColorFromRGB(0xFFFF9100);
	[btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	UIView *vwFbRightSolid = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, btnBack.bounds.size.height)];
	vwFbRightSolid.backgroundColor = UIColorFromRGB(0xFFFF9100);
	[btnBack addSubview:vwFbRightSolid];
	btnBack.tag = GPBTN_REG_BACK;
	[mScrollView addSubview:btnBack];

	y += (h + offs + offs); 
	mScrollView.contentSize = CGSizeMake(tabBarView.bounds.size.width-10, y);
	[self normalizeWindowHeight:y+offs];
}

-(void) showSupportPage
{
	mLastPageId = mPageId;
	mPageId = PAGE_REGISTER;

	// remove all views
	[mScrollView setContentOffset:CGPointZero animated:NO];
	[mScrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

	int offs = 10;
	int frw = mScrollView.bounds.size.width;
	int w = 106; int h = 70;
	int x = (frw - w) / 2;
	int y = offs;

	UIImageView *gplogo =[[UIImageView alloc] initWithFrame:CGRectMake(x,y,w,h)];
	gplogo.image=[UIImage imageNamed:@"gamesparklogo.png"];
	[mScrollView addSubview:gplogo];

	// support page message
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	UILabel *lbAboutBind = [[UILabel alloc] initWithFrame:CGRectMake(x, y, w, h)];
	lbAboutBind.text = @"If you had any problem, suggestions, feedback, send us message!";
	//lbAboutBind.font = [UIFont fontWithName:@"System" size:fontHeight];
	[lbAboutBind setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]*0.8f]];
	lbAboutBind.textColor = [UIColor blackColor];
	lbAboutBind.backgroundColor = [UIColor clearColor];
	lbAboutBind.clipsToBounds = YES;
	lbAboutBind.lineBreakMode = NSLineBreakByWordWrapping;
	lbAboutBind.numberOfLines = 0;
	lbAboutBind.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
	lbAboutBind.textAlignment = NSTextAlignmentLeft;
	[mScrollView addSubview:lbAboutBind];

	// text field email
	// x = offs; y += (h + offs + offs); w = frw - offs - offs; h = 36;
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	mTFEmail = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
    [mTFEmail setBorderStyle:UITextBorderStyleRoundedRect];
	[mTFEmail setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mTFEmail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	mTFEmail.textAlignment = NSTextAlignmentLeft;
	mTFEmail.keyboardType = UIKeyboardTypeEmailAddress;
	mTFEmail.leftViewMode = UITextFieldViewModeAlways;
	mTFEmail.placeholder = [NSString stringWithFormat:@"Email ..."];
	mTFEmail.clearButtonMode = UITextFieldViewModeAlways;
	mTFEmail.returnKeyType = UIReturnKeyDone;
	[mTFEmail addTarget:self action:@selector(textFieldSupportPageDidChange:) forControlEvents:UIControlEventEditingChanged];
	mTFEmail.delegate = self;
	[mScrollView addSubview:mTFEmail];

	//UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	// button.frame = CGRectMake(20, 20, 200, 72);
	// UIImage *image = [UIImage imageNamed:@"rob.png"];
	// [button setImage:image forState:UIControlStateNormal];
	// button.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
	// [button setTitle:@"Hello" forState:UIControlStateNormal];
	// button.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
	// button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	// [self.view addSubview:button];
	// combo box atau spinner di android
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	mBtnCombo = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[mBtnCombo setTitle:@"General" forState:UIControlStateNormal];
	[mBtnCombo addTarget:self action:@selector(BtnSupportCategoryClick:) forControlEvents:UIControlEventTouchUpInside];
	//[mBtnCombo addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	mBtnCombo.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
	mBtnCombo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	//[mBtnCombo.titleLabel setTextAlignment: NSTextAlignmentLeft];
	[mBtnCombo.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	//mBtnCombo.layer.cornerRadius = 6;
	//mBtnCombo.backgroundColor = UIColorFromRGB(0xFFFFAB00);
	[mBtnCombo setTitleColor:self.uiViewCtrl.view.tintColor forState:UIControlStateNormal];
	UIView *vwAccountsLine = [[UIView alloc] initWithFrame:CGRectMake(offs, mBtnCombo.bounds.size.height-1, mBtnCombo.bounds.size.width - offs - offs, 1)];
	vwAccountsLine.backgroundColor = self.uiViewCtrl.view.tintColor;
	[mBtnCombo addSubview:vwAccountsLine];
	UIView *vwAccountsLine2 = [[UIView alloc] initWithFrame:CGRectMake(mBtnCombo.bounds.size.width-offs-8, mBtnCombo.bounds.size.height-8, 8, 8)];
	vwAccountsLine2.backgroundColor = self.uiViewCtrl.view.tintColor;
	[mBtnCombo addSubview:vwAccountsLine2];
	[mScrollView addSubview:mBtnCombo];

	// text field password
	x = offs; y += (h + offs); w = frw - offs - offs; h = 72;
	mTFSupportMsg = [[UITextField alloc] initWithFrame:CGRectMake(x, y, w, h)]; 
    [mTFSupportMsg setBorderStyle:UITextBorderStyleRoundedRect];
	[mTFSupportMsg setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mTFSupportMsg.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
	mTFSupportMsg.textAlignment = NSTextAlignmentLeft;
	mTFSupportMsg.keyboardType = UIKeyboardTypeEmailAddress;
	mTFSupportMsg.leftViewMode = UITextFieldViewModeAlways;
	mTFSupportMsg.placeholder = [NSString stringWithFormat:@"Messages ..."];
	mTFSupportMsg.clearButtonMode = UITextFieldViewModeAlways;
	mTFSupportMsg.returnKeyType = UIReturnKeyDone;
	[mTFSupportMsg addTarget:self action:@selector(textFieldSupportPageDidChange:) forControlEvents:UIControlEventEditingChanged];
	mTFSupportMsg.delegate = self;
	[mScrollView addSubview:mTFSupportMsg];

	// button submit and back
	x = offs; y += (h + offs); w = ((2*frw) / 3) - offs; h = 36;
	mBtnSubmit = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[mBtnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
	[mBtnSubmit addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[mBtnSubmit addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[mBtnSubmit addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[mBtnSubmit.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[mBtnSubmit.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	mBtnSubmit.layer.cornerRadius = 6;
	mBtnSubmit.backgroundColor = UIColorFromRGB(0xFF9E9E9E);
	[mBtnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	UIView *vwLoginLeftSolid = [[UIView alloc] initWithFrame:CGRectMake(w-4, 0, 4, h)];
	vwLoginLeftSolid.backgroundColor = UIColorFromRGB(0xFF9E9E9E);
	[mBtnSubmit addSubview:vwLoginLeftSolid];
	mBtnSubmit.tag = GPBTN_SUPP_SUBMIT;
	[mScrollView addSubview:mBtnSubmit];
	[mBtnSubmit setEnabled:NO];

	// button facebook login
	x = offs + w + 1; //y += (h + offs); w = frw / 2; h = 36;
	w = (frw / 3) - offs;
	UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnBack setTitle:@"Back" forState:UIControlStateNormal];
	[btnBack addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnBack addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnBack addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnBack.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnBack.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnBack.layer.cornerRadius = 6;
	btnBack.backgroundColor = UIColorFromRGB(0xFFFF9100);
	[btnBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	UIView *vwFbRightSolid = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, btnBack.bounds.size.height)];
	vwFbRightSolid.backgroundColor = UIColorFromRGB(0xFFFF9100);
	[btnBack addSubview:vwFbRightSolid];
	btnBack.tag = GPBTN_SUPP_BACK;
	[mScrollView addSubview:btnBack];

	y += (h + offs + offs); 
	mScrollView.contentSize = CGSizeMake(tabBarView.bounds.size.width-10, y);
	[self normalizeWindowHeight:y+offs];
}

-(BOOL) isLoggedInByEmail
{
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *semail = [self getStringFromJsonArray:spref0 indexString:spref3];
	if([semail isEqualToString:@"Guest"]) {
		return NO;
	}
	if([semail isEqualToString:@"Facebook_Enabled"]) {
		return NO;
	}
	return YES;
}

-(BOOL) isLoggedInByFb
{
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *semail = [self getStringFromJsonArray:spref0 indexString:spref3];
	if([semail isEqualToString:@"Guest"]) {
		return NO;
	}
	if([semail isEqualToString:@"Facebook_Enabled"]) {
		return YES;
	}
	return NO;
}

-(NSString*) getFacebookName:(int)idx
{
	// String[] spref = loadLoginPref();
	// String pwds = spref[1];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *pwds = spref1;
	//String loggedin = Integer.toString(idx);
	NSString *loggedin = [NSString stringWithFormat:@"%d",idx];

	//String sname = "FB";
	NSString *sname = [NSString stringWithFormat:@"%@",@"FB"];
    //String spwd = getStringFromJsonArray(pwds, loggedin);
	NSString *spwd = [self getStringFromJsonArray:pwds indexString:loggedin];
	//String sfb = decodeEncryptedPassword(spwd);
	NSString *sfb = [NSString stringWithFormat:@"%@",[self getDecryptedPassword:spwd]];

	NSData *da = [sfb dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *di = [NSJSONSerialization JSONObjectWithData:da options:NSJSONReadingMutableContainers error:nil];
	if(di != nil) {
		sname = [NSString stringWithFormat:@"%@", [di valueForKey:@"name"]];
	}
	// try {
	// 	JSONObject jobj = new JSONObject(sfb);
	// 	// String userdata = jobj.getString("userData");
	// 	// JSONObject jobjud = new JSONObject(userdata);
	// 	sname = jobj.getString("name");
	// } catch(Exception e) {
	// 	dp("<getFacebookName> e: " + e.toString());
	// }
	return sname;
}

-(BOOL) isBound
{
	NSString *emails = [self getUserPreference:@"gamesparkEmail"];
	NSString *loggedin = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *semail = [self getStringFromJsonArray:emails indexString:loggedin];
	if([semail isEqualToString:@"Guest"]) {
		// jadi klo sampe ini sama dgn 'Guest' maka field bindlink selalu kosong.
		// karena setiap bind, baik email atau fb, Guest selalu di overwrite
		return NO;
	}
	// sampai sini berarti user telah login dengan email atau facebook
	// cek apakan sudah pernah bind atau belum
	NSString *bindlinks = [self getUserPreference:@"gamesparkBindLink"];
	NSString *bindlink = [self getStringFromJsonArray:bindlinks indexString:loggedin];
	if([bindlink isEqualToString:@""]) {
		return NO;
	}
	// karena tidak kosong, baik user login-nya dgn email maupun facebook, user sudah pernah bind
	return YES;
}

-(BOOL) isGuestAvail
{
	NSString *emails = [self getUserPreference:@"gamesparkEmail"];
	if([emails rangeOfString:@"\"Guest\""].location != NSNotFound) {
		return YES;
	}
	return NO;
}

-(void) showProfilePage
{
	// mLastPageId = mPageId;
	// mPageId = PAGE_PROFILE;
	mLastPageId = mPageId;
	mPageId = PAGE_PROFILE;

	// remove all views
	[mScrollView setContentOffset:CGPointZero animated:NO];
	[mScrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

	int offs = 10;
	int frw = mScrollView.bounds.size.width;
	int w = 106; int h = 70;
	int x = (frw - w) / 2;
	int y = offs;

	UIImageView *gplogo =[[UIImageView alloc] initWithFrame:CGRectMake(x,y,w,h)];
	gplogo.image=[UIImage imageNamed:@"gamesparklogo.png"];
	[mScrollView addSubview:gplogo];

	BOOL loginByEmail = [self isLoggedInByEmail];
	BOOL loginByFb = [self isLoggedInByFb];
	BOOL bound = [self isBound];

	//dp(@"loginByEmail=%@ loginByFb=%@ bound=%@", loginByEmail?@"Yes":@"No", loginByFb?@"Yes":@"No", bound?@"Yes":@"No");

	// button bind email
	if(!loginByEmail && !bound) {
		x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
		UIButton *btnBindEmail = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
		[btnBindEmail setTitle:@"Bind Email" forState:UIControlStateNormal];
		[btnBindEmail addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
		[btnBindEmail addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
		[btnBindEmail addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
		[btnBindEmail.titleLabel setTextAlignment: NSTextAlignmentCenter];
		[btnBindEmail.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		btnBindEmail.layer.cornerRadius = 6;
		btnBindEmail.backgroundColor = UIColorFromRGB(0xFFFFAB00);
		[btnBindEmail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		btnBindEmail.tag = GPBTN_BINDEMAIL;
		[mScrollView addSubview:btnBindEmail];
	}

	// button bind facebook
	if(!loginByFb && !bound) {
		x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
		UIButton *btnBindFacebook = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
		[btnBindFacebook setTitle:@"Bind Facebook" forState:UIControlStateNormal];
		[btnBindFacebook addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
		[btnBindFacebook addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
		[btnBindFacebook addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
		[btnBindFacebook.titleLabel setTextAlignment: NSTextAlignmentCenter];
		[btnBindFacebook.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
		btnBindFacebook.layer.cornerRadius = 6;
		btnBindFacebook.backgroundColor = UIColorFromRGB(0xFF0D47A1);
		[btnBindFacebook setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		btnBindFacebook.tag = GPBTN_BINDFACEBOOK;
		[mScrollView addSubview:btnBindFacebook];
	}

	// button more
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	UIButton *btnMore = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnMore setTitle:@"More..." forState:UIControlStateNormal];
	[btnMore addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnMore addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnMore addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnMore.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnMore.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnMore.layer.cornerRadius = 6;
	btnMore.backgroundColor = UIColorFromRGB(0xFF01579B);
	[btnMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	btnMore.tag = GPBTN_MORE;
	[mScrollView addSubview:btnMore];

	// label about binding
	x = offs; y += (h + offs); w = frw - offs - offs; h = 48;
	UILabel *lbAboutBind = [[UILabel alloc] initWithFrame:CGRectMake(x, y, w, h)];
	lbAboutBind.text = @"Binding your facebook or email secures your game data, and you can play with the same game data with multiple devices";
	//lbAboutBind.font = [UIFont fontWithName:@"System" size:fontHeight];
	[lbAboutBind setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]*0.8f]];
	lbAboutBind.textColor = [UIColor blackColor];
	lbAboutBind.backgroundColor = [UIColor clearColor];
	lbAboutBind.clipsToBounds = YES;
	lbAboutBind.lineBreakMode = NSLineBreakByWordWrapping;
	lbAboutBind.numberOfLines = 0;
	lbAboutBind.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;

	// float fwlim = fa - 40.0;
	// CGSize s = [toastMessage sizeWithFont:[UIFont systemFontOfSize:fontHeight] constrainedToSize:CGSizeMake(fwlim, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
	// float fh = s.height * 2;
	// float fw = s.width * 2;
	// if(fw > fwlim) fw = fwlim;

	lbAboutBind.textAlignment = NSTextAlignmentLeft;
	// lbAboutBind.frame = CGRectMake(0.0, 0.0, fw, fh);
	// lbAboutBind.layer.cornerRadius = 8;
	// lbAboutBind.layer.masksToBounds = YES;
	// lbAboutBind.center = CGPointMake(fa/2, 3*fb/4);
	[mScrollView addSubview:lbAboutBind];

	///////////////
	// button facebook fan page
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	UIButton *btnFbFanPage = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnFbFanPage setTitle:@"Facebook Fan Page" forState:UIControlStateNormal];
	[btnFbFanPage addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnFbFanPage addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnFbFanPage addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnFbFanPage.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnFbFanPage.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnFbFanPage.layer.cornerRadius = 6;
	btnFbFanPage.backgroundColor = UIColorFromRGB(0xFF0277BD);
	[btnFbFanPage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	btnFbFanPage.tag = GPBTN_FBFANPAGE;
	[mScrollView addSubview:btnFbFanPage];

	// button support
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	UIButton *btnSupport = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnSupport setTitle:@"Support" forState:UIControlStateNormal];
	[btnSupport addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnSupport addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnSupport addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnSupport.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnSupport.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnSupport.layer.cornerRadius = 6;
	btnSupport.backgroundColor = UIColorFromRGB(0xFFFFAB00);
	[btnSupport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	btnSupport.tag = GPBTN_SUPPORT;
	[mScrollView addSubview:btnSupport];

	// button logout
	x = offs; y += (h + offs); w = frw - offs - offs; h = 36;
	UIButton *btnLogout = [[UIButton alloc] initWithFrame:CGRectMake(x, y, w, h)];
	[btnLogout setTitle:@"Logout" forState:UIControlStateNormal];
	[btnLogout addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
	[btnLogout addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
	[btnLogout addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
	[btnLogout.titleLabel setTextAlignment: NSTextAlignmentCenter];
	[btnLogout.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:[UIFont systemFontSize]]];
	btnLogout.layer.cornerRadius = 6;
	btnLogout.backgroundColor = UIColorFromRGB(0xFF40C4FF);
	[btnLogout setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	btnLogout.tag = GPBTN_LOGOUT;
	[mScrollView addSubview:btnLogout];

	y += (h + offs + offs); 
	mScrollView.contentSize = CGSizeMake(tabBarView.bounds.size.width-10, y);
	[self normalizeWindowHeight:y+offs];
}

- (void)BtnAccountsClick:(UIButton*)sender
{
    //dp(@"BtnAccountsClick clicked! %@", [FBSDKAccessToken currentAccessToken] ? @"Log out" : @"Log in");
	//UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
	//destructiveButtonTitle:nil otherButtonTitles:@"jimbas777@gmail.com", @"anu@anu.com", @"jimbas@jimbas.com", nil];
	UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
		cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
	NSString *s;
	for(int i=0;i<[mLoginPageParams count];i++) {
		//[sheet addButtonWithTitle:[mLoginPageParams objectAtIndex:i]];
		s = [NSString stringWithFormat:@"%@", [mLoginPageParams objectAtIndex:i]];
		if([s isEqualToString:@"Facebook_Enabled"]) {
			s = [NSString stringWithFormat:@"fb: %@",[self getFacebookName:i]];
		}
		[sheet addButtonWithTitle:s];
	}
	[sheet addButtonWithTitle:@"Other account"];
	[sheet addButtonWithTitle:@"Cancel"];
	[sheet setCancelButtonIndex:[sheet numberOfButtons] - 1];
	//sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [sheet showInView:self.tabBarView];
	mnComboType = COMBO_ACCOUNTS;
}

- (void)BtnSupportCategoryClick:(UIButton*)sender
{
	UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
		cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
	[sheet addButtonWithTitle:@"General"];
	[sheet addButtonWithTitle:@"Billing"];
	[sheet addButtonWithTitle:@"Game"];
	[sheet addButtonWithTitle:@"Bug"];
	[sheet addButtonWithTitle:@"Cancel"];
	[sheet setCancelButtonIndex:[sheet numberOfButtons] - 1];
	//sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [sheet showInView:self.tabBarView];
	mnComboType = COMBO_SUPPCATEGS;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	//dp(@"actionSheet: %d %d %@", (int)[actionSheet numberOfButtons], (int)buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
	if(([actionSheet numberOfButtons]-1) == buttonIndex) {
		//dp(@"cancel choosen");
		return;
	}
	if(([actionSheet numberOfButtons]-2) == buttonIndex && mnComboType == COMBO_ACCOUNTS) {
		//dp(@"other account choosen");
		[self showLoginPage:YES];
		return;
	}
	//[mBtnCombo setTitle:[mLoginPageParams objectAtIndex:buttonIndex] forState:UIControlStateNormal];
	[mBtnCombo setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
    // switch (buttonIndex) {
    //     case 0: {
    //         break;
    //     } case 1: {
    //         break;
    //     } case 2: {
    //         break;
    //     }
    // }
}

- (void) destroy
{
	if(tabBarView != nil && myButton != nil) {
		//[self->mWebView removeFromSuperview];
		[self->tabBarView removeFromSuperview];
		[self->myButton removeFromSuperview];
		tabBarView = nil;
		//mWebView = nil;
		myButton = nil;
		//btnFbBack = nil;
		[self clearVars];
	}
}

// - (void) loadProgress
// {
// 	if(mActivityIndicView == nil) {
// 		mActivityIndicView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
// 		mActivityIndicView.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width / 2, self.uiViewCtrl.view.bounds.size.height / 2);
// 	}
// 	if(mActivityIndicView.superview == nil) {
// 		[mActivityIndicView startAnimating];
// 		[self.uiViewCtrl.view addSubview:self.mActivityIndicView];
// 		if(tabBarView != nil) {
// 			[tabBarView setAlpha:0.5];
// 		}
// 	}
// }

// - (void) closeProgress
// {
// 	if(mActivityIndicView != nil) {
// 		[mActivityIndicView removeFromSuperview];
// 		mActivityIndicView = nil;
// 		if(tabBarView != nil) {
// 			[tabBarView setAlpha:1.0];
// 		}
// 	}
// }

- (void) connectError:(NSError*)error
{
	dp(@"connectError: %@", error);
	//[self closeProgress];
	//[self doAlert:@"Gamespark Connection Problem" message:@"Unable to connect to server. Press OK to retry." alertid:ALERTID_CHECKJAR];
	[self doAlert:@"Gamespark Connection Problem" message:@"Unable to connect to server. Press OK to retry." alertid:ALERTID_NONE];
	mGpOnReturn(GPRET_CONNECT_ERROR, @"Unable to connect to server");
}

- (NSString*) decodeFromPercentEscapeString:(NSString*)string
{
	return (__bridge NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
		(__bridge CFStringRef) string, CFSTR(""), kCFStringEncodingUTF8);
} 

- (void) getCheckJar
{
	// if([msCheckJar rangeOfString:@"jimbas.org"].location == NSNotFound) {
	// } else {
	// 	mbJimbasOrg = true;
	// 	dp(@"using jimbas.org detected");
	// }

	NSMutableString *mchkjar = [[NSMutableString alloc] initWithFormat:@"%@", msCheckJar];
	mchkjar = [[mchkjar stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];

	NSString *realchkjar = [self decodeFromPercentEscapeString:mchkjar];

	[self connectServer:realchkjar postParam:nil
	//completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
	  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		//[mConnectionTimer invalidate];
		NSString * respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
		dp(@"getCheckJar:respText: %@",respText);
		if(error == nil) {
			// parse check jar json
			NSData* respData = [respText dataUsingEncoding:NSUTF8StringEncoding];
			NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:respData options:NSJSONReadingMutableContainers error:&error];
			if(!dict) {
				dp(@"getCheckJar:jsonParseError: %@", error);
				mGpOnReturn(nCheckJarErrorRet, respText);
				return;
			}
			msIndexUrl = [[NSString alloc] initWithFormat:@"%@",[self decodeFromPercentEscapeString:dict[@"indexURL"]]];
			msLoginUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"loginURL"]]];
			msRegisterUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"registerURL"]]];
			msFbUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"fbURL"]]];
			msProfileUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"profileURL"]]];
			msLogoutUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"logoutURL"]]];
			msBindEmail = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"bindEmail"]]];
			msBindFb = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"bindFB"]]];
			msSupportUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"supportURL"]]];
			msQuickStartUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"quickstartURL"]]];
			msQuickLoginUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"quickLoginURL"]]];
			msFbFansPageUrl = [[NSString alloc] initWithFormat:@"%@", [self decodeFromPercentEscapeString:dict[@"fbFansPage"]]];

			dp(@"\nmsIndexUrl=%@\nmsLoginUrl=%@\nmsRegisterUrl=%@\nmsFbUrl=%@\nmsProfileUrl=%@\nmsLogoutUrl=%@\nmsBindEmail=%@\nmsBindFb=%@\nmsSupportUrl=%@\nmsQuickStartUrl=%@\nmsQuickLoginUrl=%@\nmsFbFansPageUrl=%@",
			msIndexUrl, msLoginUrl, msRegisterUrl, msFbUrl, msProfileUrl, msLogoutUrl,
			msBindEmail, msBindFb, msSupportUrl, msQuickStartUrl, msQuickLoginUrl, msFbFansPageUrl);
			[self executeResume];
		} else {
			[self connectError:error];
		}
	}];
}

- (id) getUserPreference:(NSString*)forKey
{
	NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
	return [defaults valueForKey:forKey];
}

- (void) setUserPreference:(id)value forKey:(NSString*)key
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:value forKey:key];
	[defaults synchronize];
}

// - (NSString*) decodeEncryptedPassword:(NSString*) scodedencryptedpassword
// {
// 	return scodedencryptedpassword;
// }

- (void) doLogout
{
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		msUserId,@"user_id",
	nil];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doLogout:jsonString=%@", jsonstr);
	NSMutableString* sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msLogoutUrl];
	sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];

	[self connectServer:sUrl postParam:jsonstr completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		// normal logout
		[self onReturnLogout:response withData:data withError:error];
	}];
}

- (void) doQuickStart:(NSString*)quickUrl
{
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		[self getDeviceId],@"uuid",
		msChannelId,@"channel_id",
	nil];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doQuickStart:jsonString: %@", jsonstr);
	[self connectServer:quickUrl postParam:jsonstr completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		// normal quickstart
		[self onReturnQuickStart:response withData:data withError:error];
	}];
}

- (void) doLogin:(NSString*)email encryptedPassword:(NSString*)encryptedPassword bind:(BOOL)bind
{
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		email,@"email",
		encryptedPassword,@"password",
		msChannelId,@"channel_id",
	nil];
	msTempStrings0 = [[NSString alloc] initWithFormat:@"%@",email];
	msTempStrings1 = [[NSString alloc] initWithFormat:@"%@",encryptedPassword];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doLogin:jsonString: %@", jsonstr);
	NSMutableString* sUrl = nil;
	if(bind) {
		sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msBindEmail];
		sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
	} else {
		sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msLoginUrl];
	}
	[self connectServer:sUrl postParam:jsonstr completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		if(bind) { // binding
			[self onReturnBinding:response withData:data withError:error];
			return;
		}
		// normal login
		[self onReturnLogin:response withData:data withError:error];
	}];
}

- (void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	dp(@"alertView:title=%@ buttonIndex=%d", alertView.title, buttonIndex);
	switch(nAlertId)
	{
	case ALERTID_CHECKJAR: {
			[self getCheckJar];
		} break;
	case ALERTID_LOGOUT: {
			// no=0, yes=1
			switch(buttonIndex) {
				case 0: {} break;
				case 1: {
					[self showLoading];
					[tabBarView setHidden:NO];
					tabBarViewShown = YES;
					[self doLogout];
				} break;
			}
		} break;
	}
	// switch(buttonIndex) {	// cancel button
	// case 0:
	// 	switch(nAlertId) {
	// 	case ALERTID_CHECKJAR:
	// 		dp(@"checkjar retry");
	// 		[self getCheckJar];
	// 		break;
	// 	}
	// 	break;
	// case 1:
	// 	break;
	// }
	[alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

- (void) doAlert:(NSString*)title message:(NSString*)message alertid:(NSUInteger)alertid
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
		message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	nAlertId = alertid;
}

- (void) doYesNoAlert:(NSString*)title message:(NSString*)message alertid:(NSUInteger)alertid
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
		message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"No", @"Yes", nil];
	[alert show];
	nAlertId = alertid;
}

- (void) initFloatingIcon
{
	if(myButton == nil) {
		myButton = [FloatingIcon buttonWithType:UIButtonTypeCustom];
		myButton.MoveEnable = YES;
		myButton.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width/2, self.uiViewCtrl.view.bounds.size.height/2);
		myButton.frame = CGRectMake(myButton.center.x, myButton.center.y, 40, 40);
		[myButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"asstvlogo.png"]] forState:UIControlStateNormal];		
		myButton.layer.cornerRadius = 40/2;//5;
		//myButton.layer.backgroundColor = [[UIColor blackColor] CGColor];
		myButton.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
		myButton.layer.borderWidth = 2.0; // set as you want.
		myButton.clipsToBounds = YES;
		
		[myButton setTag:10];
		[myButton addTarget:self action:@selector(floatIconClicked:) forControlEvents:UIControlEventTouchUpInside];
		[self.uiViewCtrl.view addSubview:myButton];
		[myButton setHidden:YES];
	} else {
		myButton.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width/2, self.uiViewCtrl.view.bounds.size.height/2);
		myButton.frame = CGRectMake(myButton.center.x, myButton.center.y, 40, 40);
	}

	//tabBarViewShown = NO;
	//[self initTabBarView];
	//[self initWebView];
}

- (void) updateView
{
	[self initFloatingIcon];
	[self initTabBarView];
	[myButton moveButton:YES];
}

-(void) initTabBarView
{
	float fa = self.uiViewCtrl.view.bounds.size.width;
	float fb = self.uiViewCtrl.view.bounds.size.height;
	dp(@"width:%.2f height:%.2f", fa, fb);
	float fc = fa < fb ? fa : fb;
	float fd = fc;
	fc *= 0.80f;
	fd *= 0.95f;
	if(tabBarView == nil) {
		tabBarView = [[GPUIView alloc] initWithFrame:CGRectMake((fa/2)-(fc/2), (fb/2)-(fd/2), fc , fd)] ;
		tabBarView.layer.cornerRadius = 10;
		tabBarView.layer.backgroundColor = [[UIColor whiteColor] CGColor];
		//tabBarView.alpha = 0.5f;
		tabBarView.layer.masksToBounds = YES;
		tabBarView.layer.borderColor = [UIColor lightGrayColor].CGColor; // set color as you want.
		tabBarView.layer.borderWidth = 3.0; // set as you want.
		//tabBarView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		tabBarView.delegate = self;
		[self.uiViewCtrl.view addSubview:tabBarView];
		//[tabBarView setHidden:YES];
		tabBarViewShown = YES;
		mScrollView = [[UIScrollView alloc] initWithFrame:self.tabBarView.bounds];
		//mScrollView.contentSize = CGSizeMake(tabBarView.bounds.size.width-10, tabBarView.bounds.size.height-10);
		//scrollview.showsHorizontalScrollIndicator = YES;
		//scrollview.showsVerticalScrollIndicator=YES;
		mScrollView.scrollEnabled = YES;
		mScrollView.userInteractionEnabled = YES;
		//mScrollView.layer.backgroundColor = [[UIColor lightGrayColor] CGColor];
		mScrollView.delegate = self;
		[self.tabBarView addSubview:mScrollView];
	} else {
		tabBarView.frame = CGRectMake((fa/2)-(fc/2), (fb/2)-(fd/2), fc , fd);
	}
	fWindowHeight = tabBarView.bounds.size.height;
}

// -(void) initWebView
// {
// 	if(mWebView != nil) return;
// 	mWebView = [[UIWebView alloc] initWithFrame:self.tabBarView.bounds];
// 	// mWebView.delegate = self;
// 	//[self.tabBarView addSubview:mWebView];
// 	[[NSURLCache sharedURLCache] removeAllCachedResponses];

// 	//[self initFloatingIcon];
// 	//[self.uiViewCtrl.view insertSubview:myButton aboveSubview:mWebView];
// 	//[tabBarView setHidden:YES];
// }

// -(void)initContents
// {
// }

// - (void) buttonFbBackDown:(id)sender
// {
// 	UIButton *btn = sender;
// 	btn.backgroundColor = [UIColor colorWithRed:0.0 green:128.0/255.0f blue:255.0f alpha:0.5f];
// }

// - (void) buttonFbBack:(id)sender
// {
// 	UIButton *btn = sender;
// 	[btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
// 	btn.backgroundColor = [UIColor colorWithRed:0.0 green:128.0/255.0f blue:255.0f alpha:1.0f];
// 	[btnFbBack setHidden:YES];
// 	//[mWebView goBack];
// 	[self loadProgress];
// 	NSMutableString* profileUrl = [[NSMutableString alloc] initWithFormat:@"%@", msProfileUrl];
// 	//dp(@"profileUrl2: %@ msUserId: %@", profileUrl, msUserId);
// 	profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
// 	profileUrl = [[profileUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
// 	//dp(@"profileUrl3: %@", profileUrl);
// 	[self loadUrlRequest:YES url:profileUrl postParam:nil];
// }

// -(void)initButtonFbBack
// {
// 	if(btnFbBack != nil) return;
// 	int roundWidthHeight = 20;
// 	btnFbBack = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, roundWidthHeight, roundWidthHeight)];
// 	//btnFbBack.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
// 	btnFbBack.contentEdgeInsets = UIEdgeInsetsMake(-2, 0, 0, 0);
// 	[btnFbBack setTitle:@"<" forState:UIControlStateNormal];
// 	[btnFbBack addTarget:self action:@selector(buttonFbBack:) forControlEvents:UIControlEventTouchUpInside];
// 	[btnFbBack addTarget:self action:@selector(buttonFbBackDown:) forControlEvents:UIControlEventTouchDown];

// 	[btnFbBack.titleLabel setTextAlignment: NSTextAlignmentCenter];
// 	[btnFbBack.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
// 	//btnFbBack.clipsToBounds = YES;
// 	btnFbBack.layer.cornerRadius = roundWidthHeight/2.0f;
// 	btnFbBack.backgroundColor = [UIColor colorWithRed:0.0 green:128.0/255.0f blue:255.0f alpha:1.0f];
// 	[btnFbBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
// 	//btnFbBack.layer.borderColor = [UIColor blackColor].CGColor;
// 	//btnFbBack.layer.borderColor = [UIColor colorWithRed:0.000 green:0.000 blue:1.000 alpha:0.750].CGColor;
// 	//btnFbBack.tintColor = [UIColor colorWithRed:0.764 green:1.000 blue:0.000 alpha:1.000];
// 	//btnFbBack.layer.borderWidth=1.5f;

// 	[self.mWebView addSubview:btnFbBack];
// 	[btnFbBack setHidden:YES];
// }

- (void) loadUrlWithBrowser:(NSString*) surl
{
	NSURL* url = [NSURL URLWithString:surl];
	if(![[UIApplication sharedApplication] openURL:url]) {
		dp(@"%@%@",@"Failed to open url:",[url description]);
	}
}

// - (void) loadUrlRequest:(BOOL)withCustomHeader url:(NSString*) url postParam:(NSString*) postParam
// {
// 	dp(@"loadUrlRequest:url: %@ customHeader:%@", url, withCustomHeader ? @"Yes": @"No");
// 	/*if(mWebView == nil) {
// 		[self initTabBarView];
// 		[self initWebView];
// 		[[NSURLCache sharedURLCache] removeAllCachedResponses];
// 	}*/
// 	NSURL* nsurl = nil;
// 	if(url == nil || [url isEqualToString:@"(null)"]) {
// 		nsurl= [NSURL URLWithString:@"about:blank"];
// 	} else {
// 		nsurl= [NSURL URLWithString:url];
// 	}
// 	NSURLRequest* nsrequest = nil;
// 	NSMutableURLRequest* nsmutablereq = nil;
// 	if(withCustomHeader) {
// 		NSString *scursystm = [self getUnixTimeStamp];
// 		NSMutableString* str = [[NSMutableString alloc] init];
// 		[str appendString:msSecretKey];
// 		[str appendString:scursystm];
// 		nsmutablereq = [NSMutableURLRequest requestWithURL:nsurl];
// 		[nsmutablereq setValue:scursystm forHTTPHeaderField:@"timestamp"];
// 		[nsmutablereq setValue:[self getMd5:str] forHTTPHeaderField:@"token"];
// 		//nsrequest = [nsmutablereq mutableCopy];
// 	} else {
// 		//nsrequest = [NSURLRequest requestWithURL:nsurl];
// 		// this will ensure that facebook js read the cookies
// 		nsmutablereq = [NSMutableURLRequest requestWithURL:nsurl];
// 		[nsmutablereq setHTTPShouldHandleCookies:YES];
// 	}
// 	if(postParam != nil) {
// 		//NSURL *url = [NSURL URLWithString: @"http://your_url.com"];
// 		//NSString *body = [NSString stringWithFormat: @"arg1=%@&arg2=%@", @"val1",@"val2"];
// 		//NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL: url];
// 		[nsmutablereq setHTTPMethod: @"POST"];
// 		[nsmutablereq setHTTPBody: [postParam dataUsingEncoding: NSUTF8StringEncoding]];
// 		//[webView loadRequest: request];
// 	}
// 	nsrequest = [nsmutablereq mutableCopy];
// 	[mWebView loadRequest:nsrequest];
// }

// - (void)webViewDidStartLoad:(UIWebView *)webView {
// 	//dp(@"webViewDidStartLoad");
// }

// - (void)webViewDidFinishLoad:(UIWebView *)webView
// {
// 	NSString *s = webView.request.URL.absoluteString;
// 	dp(@"webViewDidFinishLoad: %@", s);

// 	if([s rangeOfString:@"gamespark.net"].location == NSNotFound){}else {
// 	  if([s rangeOfString:@"user/index"].location == NSNotFound){}else {
// 		if(mbLoggedIn) {
// 			if(mActivityIndicView == nil) {
// 				mActivityIndicView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
// 				mActivityIndicView.center = CGPointMake(self.uiViewCtrl.view.bounds.size.width / 2, self.uiViewCtrl.view.bounds.size.height / 2);
// 			}
// 			if(mActivityIndicView.superview == nil) {
// 				[mActivityIndicView startAnimating];
// 				[self.uiViewCtrl.view addSubview:self.mActivityIndicView];
// 			}
// 			return;
// 		}
// 	}}

// 	if([s rangeOfString:@"gamespark.net"].location == NSNotFound){}else {
// 	  if([s rangeOfString:@"user/profile"].location == NSNotFound){}else {
// 		if(mWebView != nil) {
// 			[mWebView setAlpha:1.0];
// 		}
// 	}}

// 	if([s rangeOfString:@"facebook.com"].location == NSNotFound){}else {
// 	  if([s rangeOfString:@"login"].location == NSNotFound){}else {
// 		[btnFbBack setHidden:NO];
// 	}}

// 	[self fitContentForWebviewResize];
// 	[self closeProgress];
// }

-(NSString *)getDeviceId
{
	//String[] spref = loadLoginPref();
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];
	//String uuid, lidx = getIndexInJsonArray(spref[0], "Guest");
	NSString *uuid, *lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];
	if([lidx isEqualToString:@"-1"]) {
		//uuid = UUID.randomUUID().toString();
		uuid = [[NSUUID UUID] UUIDString];
		spref0 = [self addStringToJsonArray:spref0 stringToSet:@"Guest"];
		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:uuid]];
		spref2 = [self addStringToJsonArray:spref2 stringToSet:@"guestUserIdNotYetSet"];
		spref6 = [self addStringToJsonArray:spref6 stringToSet:@""];	// bound field
		lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];
	} else {
		// NSString *spwd = [self getStringFromJsonArray:spref1 indexString:lidx];
		// uuid = [self decodeEncryptedPassword:spwd];
		uuid = [self getDecryptedPassword:[self getStringFromJsonArray:spref1 indexString:lidx]];
	}
	dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	[self setUserPreference:spref6 forKey:@"gamesparkBindLink"];
	return uuid;
}

-(NSString *)getUniqueDeviceIdentifierAsString
{	
	NSString *bundleId = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
	dp(@"bundleId: %@",bundleId);

	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:bundleId accessGroup:nil];
	NSString *strApplicationUUID = [keychainItem objectForKey:(__bridge id)(kSecValueData)];
	
	if(strApplicationUUID == nil || [strApplicationUUID isEqualToString:@""]) {
		strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
		[keychainItem setObject:strApplicationUUID forKey:(__bridge id)(kSecValueData)];
	}

	return strApplicationUUID;
}

//2015-03-31 11:42:35.666 GamesparkDemo[824:73152] userSubmission: {
//	"deviceSystemVersion" : "8.2",
//	"deviceKeychainIdentifierForVendor" : "677066BD-18EA-4BCF-BD65-46E6D8167FB5",
//	"device_model" : "iPhone",
//	"carrierName" : "XL",
//	"mobileCountryCode" : "510",
//	"deviceLocalizedModel" : "iPhone",
//	"mobileNetworkCode" : "11",
//	"deviceName" : "iJimbas",
//	"deviceSystemName" : "iPhone OS",
//	"deviceIdentifierForVendor" : "677066BD-18EA-4BCF-BD65-46E6D8167FB5"
//}

- (NSString*) getUserSubmission
{
	CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *carrier = [networkInfo subscriberCellularProvider];
	
	NSError *error;
	UIDevice *device = [UIDevice currentDevice];
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
						  [device name],@"deviceName",
						  [device systemName],@"deviceSystemName",
						  [device systemVersion],@"deviceSystemVersion",
						  [device localizedModel],@"deviceLocalizedModel",
						  [self getUniqueDeviceIdentifierAsString],@"deviceKeychainIdentifierForVendor",
						  [device model],@"device_model",
						  [[device identifierForVendor]UUIDString],@"deviceIdentifierForVendor",
						  [carrier carrierName],@"carrierName",
						  [carrier mobileCountryCode],@"mobileCountryCode",
						  [carrier mobileNetworkCode],@"mobileNetworkCode",
						  nil];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
	return [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
}

- (void) doRegistration:(NSString*)email phonenum:(NSString*)phonenum
	password:(NSString*)password confPassword:(NSString*)confPassword bind:(BOOL)bind
{
	NSString *encryptedPassword = [self getEncryptedPassword:password];
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		//dict[@"email"],@"email",
		email,@"email",
		phonenum,@"mdn",
		encryptedPassword,@"password",
		encryptedPassword,@"password_confirmation",
		msChannelId,@"channel_id",
		[self getUserSubmission],@"extra",
	nil];
	msTempStrings0 = [[NSString alloc] initWithFormat:@"%@",email];
	msTempStrings1 = [[NSString alloc] initWithFormat:@"%@",encryptedPassword];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonstr = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doLogin:jsonString: %@", jsonstr);
	NSMutableString* sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msRegisterUrl];
	[self connectServer:sUrl postParam:jsonstr completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		if(bind) { // binding
			[self onReturnBinding:response withData:data withError:error];
			return;
		}
		// normal register
		[self onReturnRegister:response withData:data withError:error];
	}];
}

- (void) doSupport:(NSString*)email category:(NSString*)categ message:(NSString*)msg
{
	// {"success":false,"message":{"email":["The email must be a valid email address."]}}
	// {
	// success: false
	// message: "Someone already has that email. Try another?"
	// }	
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
		email,@"email",
		categ,@"category",
		msg,@"message",
		msUserId,@"user_id",
		msProductId,@"product_id",
		msChannelId,@"channel_id",
	nil];
	NSError *error = nil;
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
	NSString *sparam = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	dp(@"doSupport:sparam: %@", sparam);
	[self connectServer:msSupportUrl postParam:sparam completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		NSString *respText = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
		dp(@"doSupport:respText: %@",respText);
		// check laravel error
		// Whoops, looks like something went wrong
		if(error == nil) {
			NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
			if(!dictb) {
				dp(@"doSupport:completion:error: %@", error);
				respText = [NSString stringWithFormat:@"%@",error];
				[self doAlert:@"Support Message Failed" message:respText alertid:ALERTID_NONE];
				mGpOnReturn(GPRET_SUPPORTMSG_ERROR, respText);
				return;
			}
			NSNumber *stat = [dictb valueForKey:@"success"];
			if([stat isEqual: @YES]) {
				[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__support_success" repeats:NO];
				mGpOnReturn(GPRET_SUPPORTMSG_SUCCESS, respText);
				[self showProfilePage];
			} else {
				NSString *msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"message"]];
				if(msg == nil || [msg isEqualToString:@"(null)"]) {
					msg = [NSString stringWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
					if(msg == nil || [msg isEqualToString:@"(null)"]) {
						msg = [NSString stringWithFormat:@"Message failed to send"];
					}
				}
				[self doAlert:@"Support Message Failed" message:msg alertid:ALERTID_NONE];
				mGpOnReturn(GPRET_SUPPORTMSG_ERROR, respText);
			}
		} else {
			[self connectError:error];
		}
	}];
}

-(void)onReturnBinding:(NSURLResponse*)response withData:(NSData*)data withError:(NSError*)error
{
	if(error != nil) {
		[self connectError:error];
		return;
	}
	NSString* respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
	dp(@"onReturnBinding:respText=%@",respText);
	NSDictionary *dictb = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	if(dictb == nil) {
		dp(@"onReturnBinding:dictb:jsonerror=%@", error);
		respText = [NSString stringWithFormat:@"%@",error];
		[self doAlert:@"Gamespark Login Failed" message:respText alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_BINDING_ERROR, respText);
		return;
	}

	NSNumber *stat = [dictb valueForKey:@"success"];
	if([stat isEqual: @NO]) {
		dp(@"onReturnBinding:svrerror=%@", respText);
		NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"message"]];
		if(msg == nil || [msg isEqualToString:@"(null)"]) {
			msg = [[NSString alloc] initWithFormat:@"%@",[dictb valueForKey:@"user_id"]];
			if(msg == nil || [msg isEqualToString:@"(null)"]) {
				msg = [NSString stringWithFormat:@"Login Failed. Please make sure correct email & password"];
			}
		}
		[self doAlert:@"Gamespark Login" message:msg alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_BINDING_ERROR, respText);
		[self showLoginPage:YES];
		return;
	}

	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];

	// sekarang ini user login dgn email atau facebook, jadi update aja spref[6]					
	NSString *aidx = spref3;
	NSString *semail = [self getStringFromJsonArray:spref0 indexString:aidx];
	if([semail isEqualToString:@"Guest"]) {
		spref0 = [self setStringInJsonArray:spref0 indexString:aidx stringToSet:msTempStrings0];
		spref1 = [self setStringInJsonArray:spref1 indexString:aidx stringToSet:[self getEncryptedPassword:msTempStrings1]];
	} else {
		NSString *slink = [NSString stringWithFormat:@"%@", @""];
		if([msTempStrings0 isEqualToString:@"Facebook_Enabled"]) {
			NSString *sfb = msTempStrings1;
			NSData *da = [sfb dataUsingEncoding:NSUTF8StringEncoding];
			NSDictionary *di = [NSJSONSerialization JSONObjectWithData:da options:NSJSONReadingMutableContainers error:nil];
			if(di != nil) {
				slink = [NSString stringWithFormat:@"%@ @FB", [di valueForKey:@"name"]];
				spref6 = [self setStringInJsonArray:spref6 indexString:aidx stringToSet:slink];
			} else {
				dp(@"onReturnBinding:e: getting json key name failed");
			}
		} else {
			slink = [NSString stringWithFormat:@"%@",msTempStrings0];
		}
		spref6 = [self setStringInJsonArray:spref6 indexString:aidx stringToSet:slink];
	}
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	[self setUserPreference:spref6 forKey:@"gamesparkBindLink"];
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__binding_success" repeats:NO];
	mGpOnReturn(GPRET_BINDING_SUCCESS, respText);
	[self showProfilePage];
}

-(void)onReturnLogin:(NSURLResponse*)response withData:(NSData*)data withError:(NSError*)error
{
	if(error != nil) {//`
		[self connectError:error];//`
		return;
	}//`
	NSString* respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
	dp(@"onReturnLogin:respText=%@",respText);
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];//`
	if(dict == nil) {//`
		dp(@"onReturnLogin:dict:jsonerror=%@", error);
		respText = [NSString stringWithFormat:@"%@",error];//`
		[self doAlert:@"Gamespark Login Failed" message:respText alertid:ALERTID_NONE];//`
		mGpOnReturn(GPRET_LOGIN_ERROR, respText);//`
		return;
	}//`

	NSNumber *stat = [dict valueForKey:@"success"];//`
	if([stat isEqual: @NO]) {
		dp(@"onReturnLogin:svrerror=%@", respText);
		NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"message"]];//`
		if(msg == nil || [msg isEqualToString:@"(null)"]) {
			msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
			if(msg == nil || [msg isEqualToString:@"(null)"]) {
				msg = [NSString stringWithFormat:@"Login Failed. Please make sure correct email & password"];
			}
		}
		[self doAlert:@"Gamespark Login" message:msg alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_LOGIN_ERROR, respText);
		[self showLoginPage:YES];//`
		return;
	}//`

	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];//`

	msUserId = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];//`
	NSString *fbbound = [dict valueForKey:@"fb"];//`
	if(fbbound == nil) {
		fbbound = [self getStringFromJsonArray:spref6 indexString:spref3];//`
		if(fbbound == nil) fbbound = [NSString stringWithFormat:@"%@",@""];
	}//`

	NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:msTempStrings0];//`
	if([lidx isEqualToString:@"-1"]) {//`
		spref0 = [self addStringToJsonArray:spref0 stringToSet:msTempStrings0];//`
		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:msTempStrings1]];//`
		spref6 = [self addStringToJsonArray:spref6 stringToSet:fbbound];//`
		lidx = [self getIndexInJsonArray:spref0 stringToCheck:msTempStrings0];//`
	}//`
	spref2 = [self setStringInJsonArray:spref2 indexString:lidx stringToSet:[self getEncryptedPassword:msUserId]];//`
	spref3 = lidx;//`
	spref6 = [self setStringInJsonArray:spref6 indexString:lidx stringToSet:fbbound];

	dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	[self setUserPreference:spref6 forKey:@"gamesparkBindLink"];//`
	[tabBarView setHidden:YES];
	tabBarViewShown = NO;
	[myButton setHidden:NO];
	[myButton moveButton:YES];
	mbLoggedIn = YES;//`
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__login_success" repeats:NO];
	mGpOnReturn(GPRET_LOGIN_SUCCESS, respText);//`
}


-(void)onReturnRegister:(NSURLResponse*)response withData:(NSData*)data withError:(NSError*)error
{
	if(error != nil) {
		[self connectError:error];
		return;
	}
	NSString* respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
	dp(@"onReturnRegister:respText=%@",respText);
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	if(dict == nil) {
		dp(@"onReturnRegister:dict:jsonerror=%@", error);
		respText = [NSString stringWithFormat:@"%@",error];
		[self doAlert:@"Gamespark Register Failed" message:respText alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_REGISTRATION_ERROR, respText);
		return;
	}

	NSNumber *stat = [dict valueForKey:@"success"];
	if([stat isEqual: @NO]) {
		dp(@"onReturnRegister:svrerror=%@", respText);
		NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"message"]];
		if(msg == nil || [msg isEqualToString:@"(null)"]) {
			msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
			if(msg == nil || [msg isEqualToString:@"(null)"]) {
				msg = [NSString stringWithFormat:@"Register Failed"];
			}
		}
		[self doAlert:@"Gamespark Register" message:msg alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_REGISTRATION_ERROR, respText);
		[self showRegisterPage];
		return;
	}

	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];

	msUserId = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];

	NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:msTempStrings0];
	if([lidx isEqualToString:@"-1"]) {
		spref0 = [self addStringToJsonArray:spref0 stringToSet:msTempStrings0];
		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:msTempStrings1]];
		lidx = [self getIndexInJsonArray:spref0 stringToCheck:msTempStrings0];
	}
	spref2 = [self setStringInJsonArray:spref2 indexString:lidx stringToSet:[self getEncryptedPassword:msUserId]];
	spref3 = lidx;
	spref6 = [self setStringInJsonArray:spref6 indexString:lidx stringToSet:[NSString stringWithFormat:@"%@",@""]];
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	[self setUserPreference:spref6 forKey:@"gamesparkBindLink"];
	[tabBarView setHidden:YES];
	tabBarViewShown = NO;
	[myButton setHidden:NO];
	[myButton moveButton:YES];
	mbLoggedIn = YES;
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__registration_success" repeats:NO];
	mGpOnReturn(GPRET_REGISTRATION_SUCCESS, respText);
}

-(void) onReturnLogout:(NSURLResponse*)response withData:(NSData*)data withError:(NSError*)error
{
	if(error != nil) {
		[self connectError:error];
		return;
	}
	NSString* respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
	dp(@"onReturnLogout:respText=%@",respText);
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	if(dict == nil) {
		dp(@"onReturnLogout:dict:jsonerror=%@", error);
		// --- kita skip aja, server suka laravel error :(
		[self clearFacebook];
		[self clearPreferences];
		//[self setUserPreference:[NSString stringWithFormat:@""] forKey:@"gamesparkFbStuff"];
		[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__logout_success" repeats:NO];
		mGpOnReturn(GPRET_LOGOUT_SUCCESS, @"Logout success");
		// --- jadi logout akan selalu sukses :)
		// respText = [NSString stringWithFormat:@"%@",error];
		// mGpOnReturn(GPRET_LOGOUT_ERROR, respText);
		return;
		// dp(@"onReturnLogout:dict:jsonerror=%@", error);
		// respText = [NSString stringWithFormat:@"%@",error];
		// [self doAlert:@"Gamespark Logout Failed" message:respText alertid:ALERTID_NONE];
		// mGpOnReturn(GPRET_LOGOUT_ERROR, respText);
		// return;
	}

	NSNumber *stat = [dict valueForKey:@"success"];
	if([stat isEqual: @NO]) {
		dp(@"onReturnLogout:svrerror=%@", respText);
		NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"message"]];
		if(msg == nil || [msg isEqualToString:@"(null)"]) {
			msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
			if(msg == nil || [msg isEqualToString:@"(null)"]) {
				msg = [NSString stringWithFormat:@"Logout Failed"];
			}
		}
		[self doAlert:@"Gamespark Logout" message:msg alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_LOGOUT_ERROR, respText);
		[self showProfilePage];
		return;
	}

	[self clearFacebook];
	[self clearPreferences];

	// [tabBarView setHidden:YES];
	// tabBarViewShown = NO;
	// [myButton setHidden:NO];
	// [myButton moveButton:YES];
	// mbLoggedIn = YES;
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__logout_success" repeats:NO];
	mGpOnReturn(GPRET_LOGOUT_SUCCESS, respText);
}

-(void)onReturnQuickStart:(NSURLResponse*)response withData:(NSData*)data withError:(NSError*)error
{
	if(error != nil) {
		[self connectError:error];
		return;
	}
	NSString* respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
	dp(@"onReturnQuickStart:respText=%@",respText);
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	if(dict == nil) {
		dp(@"onReturnQuickStart:dict:jsonerror=%@", error);
		respText = [NSString stringWithFormat:@"%@",error];
		[self doAlert:@"Gamespark Quickstart Failed" message:respText alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_LOGIN_ERROR, respText);
		return;
	}

	NSNumber *stat = [dict valueForKey:@"success"];
	if([stat isEqual: @NO]) {
		dp(@"onReturnQuickStart:svrerror=%@", respText);
		NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"message"]];
		if(msg == nil || [msg isEqualToString:@"(null)"]) {
			msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
			if(msg == nil || [msg isEqualToString:@"(null)"]) {
				msg = [NSString stringWithFormat:@"Quickstart Failed"];
			}
		}
		[self doAlert:@"Gamespark Quickstart" message:msg alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_LOGIN_ERROR, respText);
		[self showLoginPage:NO];
		return;
	}

	dp(@"quickstart success");
	// sampe sini lidx seharusnya tidak sama dengan -1
	msUserId = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];
	spref2 = [self setStringInJsonArray:spref2 indexString:lidx stringToSet:[self getEncryptedPassword:msUserId]];
	spref3 = lidx;
	dp(@"email:%@ password:%@ userid:%@ lidx:%@", spref0, spref1, spref2, spref3);
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	[tabBarView setHidden:YES];
	tabBarViewShown = NO;
	[myButton setHidden:NO];
	[myButton moveButton:YES];
	mbLoggedIn = YES;
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__login_success" repeats:NO];
	mGpOnReturn(GPRET_LOGIN_SUCCESS, respText);
}

-(void)onReturnFbLogin:(NSURLResponse*)response withData:(NSData*)data withError:(NSError*)error
{
	if(error != nil) {
		[self connectError:error];
		return;
	}
	NSString* respText = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
	dp(@"onReturnFbLogin:respText=%@",respText);
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	if(dict == nil) {
		dp(@"onReturnFbLogin:dict:jsonerror=%@", error);
		respText = [NSString stringWithFormat:@"%@",error];
		[self doAlert:@"Gamespark FB Login Failed" message:respText alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_FBLOGIN_ERROR, respText);
		return;
	}

	NSNumber *stat = [dict valueForKey:@"success"];
	if([stat isEqual: @NO]) {
		dp(@"onReturnFbLogin:svrerror=%@", respText);
		NSString *msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"message"]];
		if(msg == nil || [msg isEqualToString:@"(null)"]) {
			msg = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
			if(msg == nil || [msg isEqualToString:@"(null)"]) {
				msg = [NSString stringWithFormat:@"FB Login Failed. Please make sure correct email & password"];
			}
		}
		[self doAlert:@"Gamespark FB Login" message:msg alertid:ALERTID_NONE];
		mGpOnReturn(GPRET_FBLOGIN_ERROR, respText);
		[self showLoginPage:NO];
		return;
	}

	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
	NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
	NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
	NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];

	dp(@"Facebook login success!");
	NSString *remail = [dict valueForKey:@"email"];
	if(remail == nil) {
		remail = [self getStringFromJsonArray:spref6 indexString:spref3];
		if(remail == nil) remail = [NSString stringWithFormat:@"%@",@""];
	}
	msUserId = [[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"user_id"]];
	NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:msTempStrings0];
	if([lidx isEqualToString:@"-1"]) {
		spref0 = [self addStringToJsonArray:spref0 stringToSet:msTempStrings0];
		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:msTempStrings1]];
		spref2 = [self addStringToJsonArray:spref2 stringToSet:@""];
		spref6 = [self addStringToJsonArray:spref6 stringToSet:remail];
		lidx = [self getIndexInJsonArray:spref0 stringToCheck:msTempStrings0];
	}
	spref2 = [self setStringInJsonArray:spref2 indexString:lidx stringToSet:[self getEncryptedPassword:msUserId]];
	spref3 = lidx;
	spref6 = [self setStringInJsonArray:spref6 indexString:lidx stringToSet:remail];
	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
	[self setUserPreference:spref2 forKey:@"gamesparkUserId"];
	[self setUserPreference:spref3 forKey:@"gamesparkLoggedIn"];
	[self setUserPreference:spref6 forKey:@"gamesparkBindLink"];
	[tabBarView setHidden:YES];
	tabBarViewShown = NO;
	[myButton setHidden:NO];
	[myButton moveButton:YES];
	mbLoggedIn = YES;
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tmevIGamesparkCallback:) userInfo:@"__fblogin_success" repeats:NO];
	mGpOnReturn(GPRET_FBLOGIN_SUCCESS, respText);
}

- (void) getFacebookData
{
	[[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}]
		startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
			if(!error) {
				NSError *error;
				NSData *fbdata = [NSJSONSerialization dataWithJSONObject:result options:NSJSONWritingPrettyPrinted error:&error];
				NSString *connResp = [[NSString alloc] initWithData:fbdata encoding:NSUTF8StringEncoding];
				//dp(@"getFacebookData:connResp=%@", connResp);
				//NSString *connResp = [NSString stringWithFormat:@"%@", result];
				[self doFbLogin:connResp bind:mbBindMode];
			}
		}
	];
}

- (void) doFbWork
{
	////////////////////////////////////////////////////////////////////////////
	// jangan lupa, ada tambahan di AppDelegate.m
	////////////////////////////////////////////////////////////////////////////
	// #import <FBSDKCoreKit/FBSDKApplicationDelegate.h>
	// - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// 	return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
	// }
	// - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
	// 	return [[FBSDKApplicationDelegate sharedInstance] application:application
	// 		openURL:url sourceApplication:sourceApplication annotation:annotation];
	// }
	////////////////////////////////////////////////////////////////////////////
	if([FBSDKAccessToken currentAccessToken]) {
		[self getFacebookData];
		return;
	}
	FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
	[login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]
		handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
			if(error) {
				NSLog(@"%@",error.description);
			} else if(result.isCancelled) {
				NSLog(@"Result Cancelled!");
			} else {
				[self getFacebookData];
			}
		}
	];
}

- (void) doFbLogin:(NSString*)sConnResp bind:(BOOL)bind
{
	dp(@"doFbLogin:sConnResp=%@", sConnResp);
	NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
		msProductId,@"product_id",
		sConnResp,@"userData",
		msChannelId,@"channel_id",
		@"connected",@"status",
	nil];
	msTempStrings0 = [[NSString alloc] initWithFormat:@"%@",@"Facebook_Enabled"];
	msTempStrings1 = [[NSString alloc] initWithFormat:@"%@",sConnResp];
	NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
	NSMutableString *jsonstr = [[NSMutableString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
	jsonstr = [[jsonstr stringByReplacingOccurrencesOfString:@"\\n" withString:@""] mutableCopy];
	dp(@"doFbLogin:jsonstr=%@", jsonstr);
	NSMutableString* sUrl = nil;
	if(bind) {
		sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msBindFb];
		sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{user_id}" withString:msUserId] mutableCopy];
	} else {
		sUrl = [[NSMutableString alloc] initWithFormat:@"%@", msFbUrl];
		sUrl = [[sUrl stringByReplacingOccurrencesOfString:@"{product_id}" withString:msProductId] mutableCopy];
	}
	[self connectServer:sUrl postParam:jsonstr completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		if(bind) { // binding
			[self onReturnBinding:response withData:data withError:error];
			return;
		}
		// normal facebook login
		[self onReturnFbLogin:response withData:data withError:error];
	}];
}

//- (void) replaceGuestForBinding:(NSString*) account
// - (void) replaceGuestForBinding:(BOOL) bfacebook
// {
// 	NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
// 	NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
// 	NSString *lidx = [self getIndexInJsonArray:spref0 stringToCheck:@"Guest"];

// 	NSString *sjsondata = nil;
// 	NSData *jsondata = nil;

// 	if([lidx rangeOfString:@"-1"].location == NSNotFound) {
// 		if(bfacebook) {
// 			dp(@"lidx -1 NSNotFound");
// 			spref0 = [self setStringInJsonArray:spref0 indexString:lidx stringToSet:@"Facebook_Enabled"];
// 			jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
// 			sjsondata = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
// 		} else {
// 			spref0 = [self setStringInJsonArray:spref0 indexString:lidx stringToSet:msDict[@"email"]];
// 			sjsondata = [[NSString alloc] initWithFormat:@"%@",msDict[@"password"]];
// 		}
// 		spref1 = [self setStringInJsonArray:spref1 indexString:lidx stringToSet:[self getEncryptedPassword:sjsondata]];
// 	} else {
// 		if(bfacebook) {
// 			dp(@"lidx > 0 Found");
// 			spref0 = [self addStringToJsonArray:spref0 stringToSet:@"Facebook_Enabled"];
// 			jsondata = [NSJSONSerialization dataWithJSONObject:msDict options:NSJSONWritingPrettyPrinted error:nil];
// 			sjsondata = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
// 		} else {
// 			spref0 = [self addStringToJsonArray:spref0 stringToSet:msDict[@"email"]];
// 			sjsondata = [[NSString alloc] initWithFormat:@"%@", msDict[@"password"]];
// 		}
// 		spref1 = [self addStringToJsonArray:spref1 stringToSet:[self getEncryptedPassword:sjsondata]];
// 	}

// 	[self setUserPreference:spref0 forKey:@"gamesparkEmail"];
// 	[self setUserPreference:spref1 forKey:@"gamesparkPassword"];
// }

- (void) clearFacebook
{
	// clear facebook sdk
	if([FBSDKAccessToken currentAccessToken]) {
		FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		[login logOut];
	}
	// clear facebook cookies
	NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	for(NSHTTPCookie *cookie in [storage cookies]) {
		//dp(@"cookie before: %@", cookie);
		if([[cookie domain] isEqualToString:@".facebook.com"]) {
			if(  [[cookie name] isEqualToString:@"datr"]
			  || [[cookie name] isEqualToString:@"s"]
			  || [[cookie name] isEqualToString:@"csm"]
			  || [[cookie name] isEqualToString:@"fr"]
			  || [[cookie name] isEqualToString:@"lu"]
			  || [[cookie name] isEqualToString:@"c_user"]
			  || [[cookie name] isEqualToString:@"xs"] ) {
			  	//dp(@"FOUND!");
		        NSMutableDictionary *propscook = [[NSMutableDictionary alloc] initWithDictionary: [cookie properties]];
		        [propscook setObject:@"" forKey:NSHTTPCookieValue];
		        NSHTTPCookie *newcookie = [NSHTTPCookie cookieWithProperties:propscook];
		        [storage setCookie:newcookie];
			}
		}
	}
	// for(NSHTTPCookie *cookieafter in [storage cookies]) {
	// 	if([[cookieafter domain] isEqualToString:@".facebook.com"]) {
	// 		dp(@"cookie after: %@", cookieafter);
	// 	}
	// }
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) clearPreferences
{
	[self setUserPreference:@"-1" forKey:@"gamesparkLoggedIn"];
	// NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
	// NSDictionary *dict = [defs dictionaryRepresentation];
	// for(id key in dict) {
	// 	[defs removeObjectForKey:key];
	// }
	// [defs synchronize];
	dp(@"preferences cleared!");
}

// -(void) tmevIGamesparkCallback:(NSTimer*)tmr
// {
// 	NSString *jsCallBack;
// 	NSString *tag = tmr.userInfo;
// 	dp(@"tmevIGamesparkCallback:tag: %@", tag);
// 	if([tag isEqualToString:@"__logout_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('logoutSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self destroy];
// 	} else if([tag isEqualToString:@"__login_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('loginSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page
// 	} else if([tag isEqualToString:@"__fblogin_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('fbLoginSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page
// 	} else if([tag isEqualToString:@"__registration_success"]) {
// 		jsCallBack = [NSString stringWithFormat:@"i_gamespark_callback('registrationSuccess', '')"];
// 		[mWebView stringByEvaluatingJavaScriptFromString:jsCallBack];
// 		[self loadUrlRequest:NO url:nil postParam:nil];	// clear page
// 	} else {
// 		[mWebView stringByEvaluatingJavaScriptFromString:tag];
// 	}
// }

-(void) tmevIGamesparkCallback:(NSTimer*)tmr
{
	NSString *tag = tmr.userInfo;
	//dp(@"tmevIGamesparkCallback:tag: %@", tag);
	if([tag isEqualToString:@"__logout_success"]) {
		[self displayToastWithMessage:@"Logout Success"];
		[self destroy];

	} else if([tag isEqualToString:@"__login_success"]) {
		[self displayToastWithMessage:@"Login Success"];
		//[self loadUrlRequest:NO url:nil postParam:nil];	// clear page

	} else if([tag isEqualToString:@"__fblogin_success"]) {
		[self displayToastWithMessage:@"Facebook Login Success"];
		//[self loadUrlRequest:NO url:nil postParam:nil];	// clear page

	} else if([tag isEqualToString:@"__registration_success"]) {
		[self displayToastWithMessage:@"Registration Success"];
		//[self loadUrlRequest:NO url:nil postParam:nil];	// clear page

	} else if([tag isEqualToString:@"__support_success"]) {
		[self displayToastWithMessage:@"Message Sent"];

	} else if([tag isEqualToString:@"__binding_success"]) {
		[self displayToastWithMessage:@"Binding Success"];

	} else {
		// dp(@"call js: %@", tag);
	}
}

// - (void) fitContentForWebviewResize {
// 	[mWebView stringByEvaluatingJavaScriptFromString:
// 		[NSString stringWithFormat:
// 		 @"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ",
// 		 (int)mWebView.bounds.size.width]];
// }

// -(void)roundButtonDidTap:(UIButton*)tappedButton
// {
// 	dp(@"roundButtonDidTap Method Called");
// 	[tappedButton removeFromSuperview];
// 	[self destroy];
// }

// -(void)displayToastWithMessageWithSize:(NSString *)toastMessage width:(NSInteger)width height:(NSInteger)height
// {
// }

-(void)displayToastWithMessage:(NSString*)toastMessage
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        float fontHeight = [UIFont systemFontSize] * 0.7f;
		float fa = self.uiViewCtrl.view.bounds.size.width;
		float fb = self.uiViewCtrl.view.bounds.size.height;

        UILabel *toastView = [[UILabel alloc] init];
        toastView.text = toastMessage;
        toastView.font = [UIFont fontWithName:@"System" size:fontHeight];
        toastView.textColor = [UIColor whiteColor];

        toastView.backgroundColor = [UIColor blackColor];
		toastView.layer.borderColor = [UIColor lightGrayColor].CGColor;
		toastView.layer.borderWidth = 2.0;
		toastView.clipsToBounds = YES;

		toastView.lineBreakMode = NSLineBreakByWordWrapping;
		toastView.numberOfLines = 0;

		float fwlim = fa - 40.0;
		CGSize s = [toastMessage sizeWithFont:[UIFont systemFontOfSize:fontHeight] constrainedToSize:CGSizeMake(fwlim, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
		float fh = s.height * 2;
        float fw = s.width * 2;
        if(fw > fwlim) fw = fwlim;

        toastView.textAlignment = NSTextAlignmentCenter;
        toastView.frame = CGRectMake(0.0, 0.0, fw, fh);
        toastView.layer.cornerRadius = 8;
        toastView.layer.masksToBounds = YES;
		toastView.center = CGPointMake(fa/2, 3*fb/4);

        [self.uiViewCtrl.view addSubview:toastView];
        [UIView animateWithDuration:4.0f delay:0.0 options: UIViewAnimationOptionCurveEaseOut
			animations: ^{
				toastView.alpha = 0.0;
			} completion: ^(BOOL finished) {
				[toastView removeFromSuperview];
			}
         ];
    }];
}

- (void)floatIconClicked:(FloatingIcon*)btn
{
	if(!btn.MoveEnabled) {
		if(!tabBarViewShown){
			if(mbLoggedIn == YES && msUserId != nil) {
				NSString *spref0 = [self getUserPreference:@"gamesparkEmail"];
				NSString *spref1 = [self getUserPreference:@"gamesparkPassword"];
				NSString *spref2 = [self getUserPreference:@"gamesparkUserId"];
				NSString *spref3 = [self getUserPreference:@"gamesparkLoggedIn"];
				NSString *spref6 = [self getUserPreference:@"gamesparkBindLink"];
				dp(@"floatIconClicked:\n\temail=%@\n\tpassword:%@\n\tuserid:%@\n\tloggedin:%@\n\tbindlink:%@\n", spref0, spref1, spref2, spref3, spref6);
				[self showProfilePage];
			} else {
			}
			[tabBarView setHidden:NO];
			tabBarViewShown = YES;
		} else {
			[tabBarView setHidden:YES];
			tabBarViewShown = NO;
		}
	}
}

//- (void) connectServerTimeout:(NSTimer*)tmr
//{
//	if(tmr == nil) return;
//	[tmr invalidate];
//	tmr = nil;
//	//if(mConnectionTimer == nil) return;
//	NSURLSessionDataTask* dataTask = tmr.userInfo;
//	if(dataTask != nil) {
//		dp(@"checkjar timeout, cancelling connection");
//		[dataTask cancel];
//		[self closeProgress];
//		mGpOnReturn(GPRET_CONNECT_ERROR, @"Error connection time-out");
//		[self doAlert:@"Gamespark Connection" message:@"Error connection time-out. Press OK to retry." alertid:ALERTID_CHECKJAR];
//	}
//	//mConnectionTimer = nil;
//}

- (void) connectServer:(NSString*)urlToPost postParam:(NSString*)postParam
  //completionHandler:(void (^)(NSData* data, NSURLResponse* response, NSError* error))completionHandler
  completionHandler:(void (^)(NSURLResponse* response, NSData* data, NSError* error))completionHandler
{
	//NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
	//NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
	NSURL * url = [NSURL URLWithString:urlToPost];
	NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
	
	//urlRequest.timeoutInterval = 241;
	urlRequest.timeoutInterval = 60;
	[urlRequest setValue:@"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401" forHTTPHeaderField:@"User-Agent"];
	[urlRequest setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[urlRequest setValue:@"en-US" forHTTPHeaderField:@"Accept-Language"];
	[urlRequest setValue:@"UTF-8" forHTTPHeaderField:@"Accept-Charset"];
	[urlRequest setValue:@"Keep-Alive" forHTTPHeaderField:@"Connection"];
	NSString *scursystm = [self getUnixTimeStamp];
	[urlRequest setValue:scursystm forHTTPHeaderField:@"timestamp"];
	NSMutableString* str = [[NSMutableString alloc] init];
	[str appendString:msSecretKey];
	[str appendString:scursystm];
	[urlRequest setValue:[self getMd5:str] forHTTPHeaderField:@"token"];
	if(postParam != nil && postParam.length) {
		[urlRequest setHTTPMethod:@"POST"];
		NSString *urlParameters = postParam;
		[urlRequest setHTTPBody:[urlParameters dataUsingEncoding:NSUTF8StringEncoding]];
	}
	//NSURLRequest *urlreq = [urlRequest mutableCopy];
	//dp(@"urlreq: %@", [urlreq allHTTPHeaderFields]);
	//NSURLSessionDataTask * dataTask = [delegateFreeSession dataTaskWithRequest:urlRequest completionHandler:completionHandler];

	dp(@"urlreq: %@", [[urlRequest mutableCopy] allHTTPHeaderFields]);
	[NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue mainQueue] completionHandler:completionHandler];
	//mConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:12 target:self selector:@selector(connectServerTimeout:) userInfo:dataTask repeats:NO];
	//[dataTask resume];
}

- (NSData*) base64EncodingInit:(NSString*)str
{
	NSData *data = nil;
	if([NSData instancesRespondToSelector:@selector(initWithBase64EncodedString:options:)]) {
		data = [[NSData alloc] initWithBase64EncodedString:str options:0];	// iOS 7+
	} else {
		data = [[NSData alloc] initWithBase64Encoding:str];	// pre iOS7
	}
	return data;
}

- (NSString*) base64Encoding:(NSData*)data
{
	NSString *str = nil;
	if([data respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
		str = [[NSString alloc] initWithFormat:@"%@", [data base64EncodedStringWithOptions:0]];	// iOS 7+
	} else {
		str = [[NSString alloc] initWithFormat:@"%@", [data base64Encoding]];	// pre iOS7
	}
	return str;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// encryption decryption
///////////////////////////////////////////////////////////////////////////////////////////////

- (NSString*) getMd5:(NSString*) input
{
	const char *cStr = [input UTF8String];
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, (CC_LONG) strlen(cStr), result ); // This is the md5 call
	return [NSString stringWithFormat:
			@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

- (NSString*) getUnixTimeStamp
{
	NSDate* past = [NSDate date];
	NSTimeInterval oldTime = [past timeIntervalSince1970];
	return [NSString stringWithFormat:@"%0.0f", oldTime];
}

- (NSString*) getEncryptedPassword:(NSString*) realpaswd
{
	NSMutableString* sres = [[NSMutableString alloc] init];
	NSString *skey = [self getMd5:msSecretKey];
	NSData* iv = [self generateIv];
	NSString *sciphertext = [self cbcEncrypt:skey sdata:realpaswd ivps:iv];
	//NSString *siv = [self base64encode:[[NSString alloc] initWithData:iv encoding:NSASCIIStringEncoding]];
	//NSString *siv = [iv base64EncodedStringWithOptions:0];
	NSString *siv = [self base64Encoding:iv];
	[sres appendFormat:@"%@",siv];
	[sres appendFormat:@"%@",sciphertext];
	//dp(@"realpaswd: %@ siv: %@ sciphertext: %@", realpaswd, siv, sciphertext);
	return [NSString stringWithString:sres];
}

- (NSString*) getDecryptedPassword:(NSString*) encivpaswd
{
	NSString *siv = [[NSString alloc] initWithFormat:@"%@",[encivpaswd substringToIndex:24]];
	NSString *encpaswd = [[NSString alloc] initWithFormat:@"%@",[encivpaswd substringFromIndex:24]];
	//NSData* iv = [[NSData alloc] initWithBase64EncodedString:siv options:0];
	NSData* iv = [self base64EncodingInit:siv];
	NSString *skey = [self getMd5:msSecretKey];
	NSString *srealpaswd = [self cbcDecrypt:skey sdata:encpaswd ivps:iv];
	//dp(@"srealpaswd: %@ siv: %@ encpaswd: %@", srealpaswd, siv, encpaswd);
	return srealpaswd;
}

- (NSData*) secureRandom:(NSInteger)len
{
	uint8_t randomBytes[len];
	int result = SecRandomCopyBytes(kSecRandomDefault, len, (uint8_t*)&randomBytes);
	if(result == 0) {
		//return [[NSData dataWithBytes:randomBytes length:len] base64EncodedStringWithOptions:0];
		return [NSData dataWithBytes:randomBytes length:len];
	}
	return nil;
}

- (NSData*) generateIv
{
	//uint8_t biv[16] = { 1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6 };
	//return [NSData dataWithBytes:biv length:16];
	return [self secureRandom:16];
}

- (NSString*) cbcEncrypt:(NSString*)key sdata:(NSString*)sdata ivps:(NSData*)ivps
{
	//dp(@"key: %@ sdata: %@ ivps: %@", key, sdata, [ivps base64EncodedStringWithOptions:0]);
	NSData* data = [sdata dataUsingEncoding:NSUTF8StringEncoding];
	NSData* dkey = [key dataUsingEncoding:NSUTF8StringEncoding];
	NSMutableData *cipherData = [NSMutableData dataWithLength:data.length + kCCBlockSizeAES128];
	size_t numBytesEncrypted = 0;
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
		[dkey bytes], [dkey length],
		[ivps bytes],
		[data bytes], [data length],
		[cipherData mutableBytes], [cipherData length],
		&numBytesEncrypted
	);
 
	NSString *returnString = nil;
	if(cryptStatus == kCCSuccess) {
		cipherData.length = numBytesEncrypted;
		//returnString = [cipherData base64EncodedStringWithOptions:0];
		returnString = [self base64Encoding:cipherData];
	}
	
	return returnString;
}

- (NSString*) cbcDecrypt:(NSString*)key sdata:(NSString*)sdata ivps:(NSData*)ivps
{
	//dp(@"key: %@ sdata: %@ ivps: %@", key, sdata, [ivps base64EncodedStringWithOptions:0]);
	// NSData* data = [[NSData alloc] initWithBase64EncodedString:sdata options:0];
	NSData* data = [self base64EncodingInit:sdata];
	NSData* dkey = [key dataUsingEncoding:NSUTF8StringEncoding];
	
	NSMutableData *cipherData = [NSMutableData dataWithLength:data.length + kCCBlockSizeAES128];
	size_t numBytesEncrypted = 0;
	
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
		[dkey bytes], [dkey length],
		[ivps bytes],
		[data bytes], [data length],
		[cipherData mutableBytes], [cipherData length],
		&numBytesEncrypted
	);

	NSString *returnString = nil;
	if(cryptStatus == kCCSuccess) {
		cipherData.length = numBytesEncrypted;
		returnString = [[NSString alloc] initWithData:cipherData encoding:NSASCIIStringEncoding];
	}
	
	return returnString;
}


@end
