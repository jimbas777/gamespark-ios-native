//
//  Gamespark.h
//  Gamespark
//
//  Created by Jimmy Basselo on 3/5/15.
//  Copyright (c) 2015 mainpark. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreTelephony;

typedef void (^GamesparkOnReturn)(NSUInteger status, NSString* message);
enum {
	// these are for execute
	GPEXEC_LOGIN=1,
	GPEXEC_LOGOUT,
	// these are for onReturn
	GPRET_LOGIN_SUCCESS,
	GPRET_LOGOUT_SUCCESS,
	GPRET_REGISTRATION_SUCCESS,
	GPRET_LOGIN_ERROR,
	GPRET_LOGOUT_ERROR,
	GPRET_REGISTRATION_ERROR,
	GPRET_CONNECT_ERROR,
	GPRET_LOGIN_ALREADY,
	GPRET_NOT_LOGIN_YET,
	GPRET_BINDING_SUCCESS,
	GPRET_BINDING_ERROR,
	GPRET_FBLOGIN_SUCCESS,
	GPRET_FBLOGIN_ERROR,
	GPRET_LOGIN_CANCELED,
	GPRET_UNKNOWN_TAG,
	GPRET_SUPPORTMSG_SUCCESS,
	GPRET_SUPPORTMSG_ERROR
};

@protocol GPUIViewDelegate <NSObject>
@required
- (void) uiViewTouched:(BOOL)wasInside;
@end

@interface Gamespark : NSObject <UIScrollViewDelegate, UIAlertViewDelegate, GPUIViewDelegate, UIActionSheetDelegate, UITextFieldDelegate>

- (void) create:(UIViewController*)uivc gamesparkOnReturn:(GamesparkOnReturn)gamesparkOnReturn
	 productId:(NSString*)productId productName:(NSString*)productName channelId:(NSString*)channelId;
- (void) execute:(NSUInteger)tag;
- (void) updateView;
- (NSString*) getSdkVersion;
- (void) enableGateway:(BOOL)productionMode;
- (void) flushLog;
@end
