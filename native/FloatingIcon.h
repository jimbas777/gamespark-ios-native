//
//  FloatingIcon.h
//  Gamespark
//
//  Created by Jimmy Basselo on 3/5/15.
//  Copyright (c) 2015 mainpark. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FloatingIcon : UIButton {
	BOOL MoveEnable;
	BOOL MoveEnabled;
	CGPoint beginpoint;
}
@property(nonatomic)BOOL MoveEnable;
@property(nonatomic)BOOL MoveEnabled;
- (void) moveButton:(BOOL)origin;
@end
